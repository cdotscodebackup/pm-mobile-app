import React, {useEffect, useState} from 'react';
import {
    View, Text, SafeAreaView, StatusBar,
} from 'react-native';


import SplashScreen from 'react-native-splash-screen';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import TabBar from './app/Components/TabBar';
import Colors from './app/Styles/Colors';
import {Login} from './app/Screens/Login';
import {Dashboard} from './app/Screens/Dashboard';
import {IntroSlides} from './app/Screens/IntroSlides';
import {Preference} from './app/Screens/Preference';
import {SignUp} from './app/Screens/SignUp';
import {Services} from './app/Screens/Services';
import {Search} from './app/Screens/Search';
import {Categories} from './app/Screens/Categories';
import {Brands} from './app/Screens/Brands';
import {Notifications} from './app/Screens/Notifications';
import {BrandStore} from './app/Screens/BrandStore';
import {Events} from './app/Screens/Events';
import {Offers} from './app/Screens/Offers';
import {Map} from './app/Screens/Map';
import {Parking} from './app/Screens/Parking';
import {More} from './app/Screens/More';
import {Service} from './app/Screens/Service';
import {MyWebView} from './app/Screens/MyWebView';
import {OpeningHours} from './app/Screens/OpeningHours';
import {MyImage} from './app/Components/MyImageButton';
import {GettingHere} from './app/Screens/GettingHere';
import {getObject, saveObject} from './app/Classes/auth';
import {EventDetails} from './app/Screens/EventDetails';
import {Profile} from './app/Screens/Profile';
import {PersonalInformation} from './app/Screens/PersonalInformation';
import {ForgotPassword} from './app/Screens/ForgotPassword';


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export const AuthContext = React.createContext();

const App = () => {

    const [isLogin, setIsLogin] = useState(null);
    const [isFirstTime, setIsFirstTime] = useState(null);

    useEffect(() => {
        getObject('intro').then(res => {
            if (res.status) {
                setIsFirstTime(false);
            } else {
                setIsFirstTime(true);
                saveObject('intro', 'intro');
            }
            SplashScreen.hide();

        }).catch(err => {
            SplashScreen.hide();
        });

    }, []);


    return (
        <SafeAreaView style={{flex: 1, backgroundColor: Colors.app_background_color}}>
            <StatusBar backgroundColor={Colors.app_background_color} barStyle="light-content" hidden={false}/>
            <NavigationContainer>
                {/*<Stack.Navigator headerMode={'none'}>*/}
                {/*    <Stack.Screen name="IntroSlides" component={IntroSlides}/>*/}
                {/*</Stack.Navigator>*/}
                {isFirstTime !== null &&
                <Stack.Navigator headerMode={'none'}
                                 initialRouteName={isFirstTime ? 'IntroSlides' : 'Dashboard'}
                >
                    <Stack.Screen name="IntroSlides" component={IntroSlides}/>
                    <Stack.Screen name="Dashboard" component={TabNav}/>
                    <Stack.Screen name="Login" component={Login}/>
                    <Stack.Screen name="SignUp" component={SignUp}/>
                    <Stack.Screen name="Preference" component={Preference}/>
                    <Stack.Screen name="Search" component={Search}/>
                    <Stack.Screen name="Categories" component={Categories}/>
                    <Stack.Screen name="Brands" component={Brands}/>
                    <Stack.Screen name="Notifications" component={Notifications}/>
                    <Stack.Screen name="BrandStore" component={BrandStore}/>
                    <Stack.Screen name="Events" component={Events}/>
                    <Stack.Screen name="Offers" component={Offers}/>
                    <Stack.Screen name="Service" component={Service}/>
                    <Stack.Screen name="MyWebView" component={MyWebView}/>
                    <Stack.Screen name="OpeningHours" component={OpeningHours}/>
                    <Stack.Screen name="GettingHere" component={GettingHere}/>
                    <Stack.Screen name="EventDetails" component={EventDetails}/>
                    <Stack.Screen name="Profile" component={Profile}/>
                    <Stack.Screen name="PersonalInformation" component={PersonalInformation}/>
                    <Stack.Screen name="ForgotPassword" component={ForgotPassword}/>
                </Stack.Navigator>
                }


            </NavigationContainer>
        </SafeAreaView>
    );
};

export default App;


const TabNav = () => {

    return (
        <Tab.Navigator
            tabBar={props => <TabBar {...props} />}
            backBehavior={'initialRoute'}
            lazy={false}
            keyboardHidesTabBar={true}
        >
            <Tab.Screen name="Dashboard" component={Dashboard}/>
            <Tab.Screen name="Map" component={Map}/>
            <Tab.Screen name="Parking" component={Parking}/>
            <Tab.Screen name="Services" component={Services}/>
            <Tab.Screen name="More" component={More}/>
        </Tab.Navigator>
    );
};

const TabNavOl = () => {

    return (
        <Tab.Navigator
            screenOptions={({route}) => ({
                tabBarIcon: ({focused, color, size}) => {
                    let iconName;
                    let iconColor;

                    if (route.name === 'Dashboard') {
                        iconName = focused ? require('./app/Asset/home.png') : require('./app/Asset/home.png');
                        iconColor = focused ? Colors.primary_color : Colors.gray;

                    } else if (route.name === 'Map') {
                        iconName = focused ? require('./app/Asset/map.png') : require('./app/Asset/map.png');
                        iconColor = focused ? Colors.primary_color : Colors.gray;
                    } else if (route.name === 'Parking') {
                        iconName = focused ? require('./app/Asset/parking.png') : require('./app/Asset/parking.png');
                        iconColor = focused ? Colors.primary_color : Colors.gray;
                    } else if (route.name === 'Services') {
                        iconName = focused ? require('./app/Asset/services.png') : require('./app/Asset/services.png');
                        iconColor = focused ? Colors.primary_color : Colors.gray;
                    } else if (route.name === 'More') {
                        iconName = focused ? require('./app/Asset/more.png') : require('./app/Asset/more.png');
                        iconColor = focused ? Colors.primary_color : Colors.gray;
                    }


                    return <MyImage source={iconName} tintColor={iconColor}
                                    imageContainerStyle={{width: 20, height: 20}}/>;

                },
            })}
            tabBarOptions={{
                activeTintColor: Colors.primary_color,
                inactiveTintColor: 'gray',
                activeBackgroundColor: Colors.app_background_color,
                inactiveBackgroundColor: Colors.app_background_color,
            }}
            backBehavior={'initialRoute'}
            lazy={false}
            keyboardHidesTabBar={true}
        >
            <Tab.Screen name="Dashboard" component={Dashboard}/>
            <Tab.Screen name="Map" component={Map}/>
            <Tab.Screen name="Parking" component={Parking}/>
            <Tab.Screen name="Services" component={Services}/>
            <Tab.Screen name="More" component={More}/>
        </Tab.Navigator>
    );
};

