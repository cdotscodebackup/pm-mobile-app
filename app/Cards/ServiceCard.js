import {TouchableWithoutFeedback, View} from 'react-native';
import dimen from '../Styles/Dimen';
import {MyImage} from '../Components/MyImageButton';
import AppText from '../Components/AppText';
import Colors from '../Styles/Colors';
import React from 'react';

export const ServiceItem = (props) => {
    return (
        <TouchableWithoutFeedback
            onPress={() => props.onPress({
                text: props.text,
                icon: props.icon,
                desc: props.desc,
            })}>
            <View>
                <View style={{
                    flex:1,
                    flexDirection: 'row',
                    alignItems: 'center',
                    padding: 20,
                    paddingLeft: 20,
                }}>
                    <MyImage
                        source={props.icon}
                        imageContainerStyle={{width: 23, height: 23}}
                    />
                    <AppText
                        style={{flex: 1, fontSize: 17, color: Colors.button_text_color, marginLeft: dimen.app_padding}}>
                        {props.text}
                    </AppText>
                    <View style={{flex: 1}}>
                        <MyImage
                            source={require('../Asset/right_arrow.png')}
                            imageContainerStyle={{width: 13, height: 13, position: 'absolute', right: 20}}
                        />
                    </View>
                </View>
                <View style={{
                    height: 0.5,
                    backgroundColor: Colors.border_color,
                    marginRight: dimen.app_padding,
                    marginLeft: dimen.app_padding,
                }}/>
            </View>
        </TouchableWithoutFeedback>
    );
};
