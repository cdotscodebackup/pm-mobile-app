import * as React from 'react';
import {View, Image, StyleSheet, TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import AppText from '../Components/AppText';
import Colors from '../Styles/Colors';
import {MyImage} from '../Components/MyImageButton';


export const SearchItemCard = (props) => {

    return (
        <TouchableWithoutFeedback style={{flex: 1}} onPress={() => props.onPress(props.data)}>
            <View style={{flex: 1}}>
                {props.data.isFirst === true &&
                <View style={{padding: 0, marginTop: 15}}>
                    <AppText style={{fontSize: 22, color: Colors.similar_brands_text_color}}>
                        {props.data.char.toUpperCase()}
                    </AppText>
                </View>
                }
                <View style={{
                    flex: 1,
                    // height: 70,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    // backgroundColor:'red',
                    marginTop: 10,
                    marginBottom: 10,
                }}>
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
                        <AppText style={{fontSize: 16, color: Colors.normal_text_color}}>
                            {props.data.title.rendered.toUpperCase()}
                        </AppText>
                    </View>
                    {/*<View style={{flexDirection: 'row', alignItems: 'center'}}>*/}
                    {/*    <MyImage source={require('../Asset/right_arrow.png')}*/}
                    {/*             imageContainerStyle={{width: 12, height: 12}}*/}
                    {/*    />*/}
                    {/*</View>*/}
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({});


