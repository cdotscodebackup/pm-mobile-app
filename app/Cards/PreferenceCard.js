import React, {useEffect, useState} from 'react';
import {
    View,
    Image,
    StyleSheet,
    TouchableWithoutFeedback,
    TouchableOpacity,
    TouchableNativeFeedback,
} from 'react-native';
import AppText from '../Components/AppText';
import dimen, {deviceWidth} from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import MyImageButton, {MyImage} from '../Components/MyImageButton';
import {console_log} from "../Classes/auth";


export const PreferenceCard = (props) => {

    const [image, setImage] = useState(require('../Asset/preference_image.png'))
    const addPreference = () => {
        props.addPreference(props.data);
    };
    const removePreference = () => {
        props.removePreference(props.data);
    };

    useEffect(() => {


        // console_log(props.data.name)
        if (props.data.name === 'Art and Handi Crafts') {
            setImage(require('../Asset/categories/Art_handicraft.png'))
        } else if (props.data.name === 'Banks And Insurance') {
            setImage(require('../Asset/categories/Bank.png'))
        } else if (props.data.name === 'Electronic / Home Appliances') {
            setImage(require('../Asset/categories/Home_Decor.png'))
        } else if (props.data.name === 'Fashion Children') {
            setImage(require('../Asset/categories/Clothes_For_Everyone.png'))
        } else if (props.data.name === 'Fashion Ladies') {
            setImage(require('../Asset/categories/Women_Fashionista.png'))
        } else if (props.data.name === 'Fashion Men') {
            setImage(require('../Asset/categories/Men_apparel.png'))
        } else if (props.data.name === 'Fashion Unisex') {
            setImage(require('../Asset/categories/Women_Fashionista.png'))
        } else if (props.data.name === 'Featured Shops') {
            setImage(require('../Asset/categories/Clothes_For_Everyone.png'))
        } else if (props.data.name === 'Footwear &amp; Accessories') {
            setImage(require('../Asset/categories/Footwear_Accessories.png'))
        } else if (props.data.name === 'Furniture and Home') {
            setImage(require('../Asset/categories/Home_Decor.png'))
        } else if (props.data.name === 'Jewllery and Watches') {
            setImage(require('../Asset/categories/jewellery_Watches.png'))
        } else if (props.data.name === 'Lingerie / Swimwear') {
            setImage(require('../Asset/categories/Women_Fashionista.png'))
        } else if (props.data.name === 'Luggage and leather') {
            setImage(require('../Asset/categories/Gadgets.png'))
        } else if (props.data.name === 'Our Picks') {
            setImage(require('../Asset/categories/groceries_more.png'))
        } else if (props.data.name === 'Perfumes and Cosmetics') {
            setImage(require('../Asset/categories/Clothes_For_Everyone.png'))
        } else if (props.data.name === 'Pharmacy and Health Services') {
            setImage(require('../Asset/categories/Pharmacy.png'))
        } else if (props.data.name === 'Sports and Outdoor') {
            setImage(require('../Asset/categories/Sports_Wear.png'))
        } else if (props.data.name === 'Supermart') {
            setImage(require('../Asset/categories/MG_Lounge.png'))
        } else if (props.data.name === 'TOYS / Stationery') {
            setImage(require('../Asset/categories/Sports_Wear.png'))
        }

    }, [props.data])


    return (
        <View>

            <View style={styles.mainContainer}>
                <MyImage
                    source={image}
                    imageContainerStyle={{width: (deviceWidth / 4) - 15, height: (deviceWidth / 4) - 15}}
                />
                <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                    <View style={{flex: 1}}>
                        <AppText style={styles.storeText}>
                            {/*LUGGAGE / LEATHER PRODUCTS*/}
                            {props.data.name}
                        </AppText>
                        <AppText style={styles.storeCountText}>
                            {props.data.count} STORES
                        </AppText>
                    </View>
                    {!props.status &&
                    <TouchableWithoutFeedback onPress={addPreference}>
                        <View>
                            <MyImage
                                source={require('../Asset/add_preference.png')}
                                imageContainerStyle={{width: 25, height: 25}}
                            />
                        </View>
                    </TouchableWithoutFeedback>
                    }
                    {props.status &&
                    <TouchableWithoutFeedback onPress={removePreference}>
                        <View>
                            <MyImage
                                source={require('../Asset/remove_preference.png')}
                                imageContainerStyle={{width: 25, height: 25}}
                            />
                        </View>
                    </TouchableWithoutFeedback>
                    }
                </View>

            </View>
            {/*<View style={styles.bottomLine}/>*/}
        </View>
    );
};

const styles = StyleSheet.create({

    mainContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: dimen.app_padding,
        paddingTop: dimen.small_padding,
        paddingBottom: dimen.small_padding,

    },
    storeText: {
        fontSize: 14,
        color: Colors.button_text_color,
        marginLeft: 10,
        fontWeight: 'bold',
    },
    storeCountText: {
        fontSize: 13,
        color: Colors.button_text_color,
        marginLeft: 10,
    },
    bottomLine: {
        height: 1,
        backgroundColor: Colors.border_color,
        marginLeft: dimen.small_padding,
        marginRight: dimen.small_padding,
    },


});


