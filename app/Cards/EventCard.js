import {TouchableWithoutFeedback, View} from 'react-native';
import {MyImage} from '../Components/MyImageButton';
import dimen, {deviceWidth} from '../Styles/Dimen';
import AppText from '../Components/AppText';
import Colors from '../Styles/Colors';
import React, {useEffect, useState} from 'react';
import {console_log, getDataWithoutToken, isNetConnected} from '../Classes/auth';
import {BrandDescription} from "../Components/BrandDetailsComponents";


export const EventCard = (props) => {


    const [isLoading, setIsLoading] = useState(false);
    const [itemData, setItemData] = useState(null);
    const [headerImage, setHeaderImage] = useState(null);


    useEffect(() => {
        try {
            let mediaLink = props.data._links['wp:featuredmedia'][0].href;
            // console_log(mediaLink);
            getBrandImages(mediaLink);
        } catch (e) {
            console_log(e);
        }
    }, [props.data]);
    const getBrandImages = (mediaLink) => {
        isNetConnected().then(res => {
            getDataWithoutToken(mediaLink, 'GET', '', setIsLoading)
                .then(res => {
                    if (res) {
                        // console_log(res.media_details.sizes.full.source_url);
                        setHeaderImage(res.media_details.sizes.medium_large.source_url);
                        // setHeaderImage()
                    } else {

                    }
                })
                .catch(err => console_log(err));
        });
    };


    const callStorePress = () => {
        props.callStorePress(props.data);
    };
    return (
        <TouchableWithoutFeedback onPress={() => props.onPress ? props.onPress(props.data) : {}}>
            <View style={{flex: 1}}>
                <MyImage
                    source={headerImage !== null ? {uri: headerImage} : require('../Asset/shopping.png')}
                    // source={require('../Asset/event_banner.png')}
                    imageContainerStyle={{
                        width: deviceWidth,
                        height: deviceWidth / 1.3,
                        resizeMode: 'cover',
                    }}/>

                <TouchableWithoutFeedback
                    style={{flex: 1}}
                    onPress={callStorePress}>
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: dimen.app_padding,
                    }}>
                        <MyImage
                            source={require('../Asset/call_store.png')}
                            imageContainerStyle={{width: 60, height: 60, marginBottom: dimen.small_padding}}
                        />
                        <AppText style={{
                            fontSize: 14,
                            color: Colors.normal_text_color,
                            marginBottom: dimen.small_padding,
                        }}>
                            CALL NOW
                        </AppText>
                    </View>
                </TouchableWithoutFeedback>


                <View style={{
                    flex: 1,
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    padding: dimen.app_padding,
                    paddingTop: 0,
                }}>
                    <AppText
                    heading={true}
                        style={{
                        fontSize: 23,
                        color: Colors.normal_text_color,
                    }}>
                        {props.data.title.rendered}
                    </AppText>

                    <AppText style={{
                        fontSize: 13,
                        color: Colors.normal_text_color,
                    }}>
                        Location: {props.data.event_location}
                    </AppText>

                    <AppText style={{
                        fontSize: 12,
                        color: Colors.similar_brands_text_color,
                    }}>
                        {props.data.event_date}
                    </AppText>

                </View>

                <BrandDescription
                    // description={'This is the description of the brand, it will show detailed description of the brand in text form This is the description of the brand, it will show detailed description of the brand in text form'}
                    description={props.data.description}
                />

                <View style={{
                    height: 30, borderTopColor: Colors.search_place_holder_color, borderWidth: 1,
                    marginLeft: dimen.app_padding, marginRight: dimen.app_padding
                }}/>
            </View>
        </TouchableWithoutFeedback>
    );
};
