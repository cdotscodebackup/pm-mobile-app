import * as React from 'react';
import {
    View,
    Image,
    StyleSheet,
    TouchableWithoutFeedback,
    TouchableOpacity,
    TouchableNativeFeedback,
} from 'react-native';
import AppText from '../Components/AppText';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import MyImageButton, {MyImage} from '../Components/MyImageButton';
import {console_log} from '../Classes/auth';


export const NotificationCard = (props) => {
    const _onPress = () => {
        props.productOffersPress(props.data);
    };

    return (
        <View onPress={_onPress}>
            <View style={styles.mainContainer}>
                <MyImage
                    source={require('../Asset/notification_item_icon.png')}
                    tintColor={Colors.normal_text_color}
                    imageContainerStyle={{width:20,height:20}}
                />
                <AppText style={styles.notificationText} >
                    New offer at Sapphire
                </AppText>

            </View>
            <View style={styles.bottomLine}/>
        </View>
    );
};

const styles = StyleSheet.create({

    mainContainer: {
        flex:1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding:dimen.app_padding,

    },
    notificationText:{
      fontSize:14,
      color:Colors.normal_text_color,
        marginLeft:10
    },
    bottomLine:{height:1,backgroundColor:Colors.border_color, marginLeft: dimen.app_padding, marginRight:dimen.app_padding},




});


