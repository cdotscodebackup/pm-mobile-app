
export const Keys = {

    client_type_key:'client',
    vendor_type_key:'vendor',
    guest_type_key:'guest',

    fb_data_key:'fb_data_key',
    google_data_key:'google_data_key',

    fcm_token_key:'fcm_token_key',

    user_type_key:'current_user_type',
    token_key:'token_key',
    user_data_key:'user_data_key',
    product_id_key:'product_id_key',


    preference_key:'preference_key',
    ads_key:'ads_key',
    static_page_key:'static_page_key',


    sort_new_arrival:'new_arrival',
    sort_popular:'popular',

};
