export const BASE_URL = 'http://packagesmall.com/wp-json/wp/v2/';
export const BASE__URL = 'http://packagesmall.com/';
// export const BASE_URL = 'http://54.169.75.99/wp-json/wp/v2/shops';


export const MyUrls = {

    login_url: BASE_URL + 'users/login',
    register_url: BASE_URL + 'users/register',
    profile_update_url: BASE_URL + 'users/profile-update',
    forget_password_url: BASE_URL + 'users/lost-password',
    log_out_url: BASE_URL + 'logout',

    shop_media_url: BASE_URL + 'media/',

    shops_url: BASE_URL + 'shops',
    shop_category_url: BASE_URL + 'shop-category',
    cuisine_url: BASE_URL + 'dine_cuisine',
    cuisine_category_url: BASE_URL + 'dine-cuisine-category',
    entertainment_url: BASE_URL + 'entertainment',
    entertainment_category_url: BASE_URL + 'shop-category',


    offers_url: BASE_URL + 'offers',
    events_url: BASE_URL + 'events',


    map_url: BASE__URL + 'mall-map-app/',



};
