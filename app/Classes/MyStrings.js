const MyStrings = {

    forgot_password: 'Forgot your password?',
    login_button: 'Sign in',
    orText: 'Or',
    quickConnect: 'Quick connect with',
    enterCodeText:'Please Enter the Four Digit Code',
    sentViaSms:'Sent via SMS',
    submit:'Submit',
    didNotReceiveCode:'Did Not Receive Code?',
    callNow:'Call Now',
    termsAndConditions:'Terms and conditions apply.',
    dontHaveAccount:'Don\'t have an account? ',
    disclaimer1:'By signing in or creating an account, you agree with our ',
    terms:'Terms & Conditions',
    and:' and ',
    privacy:'Privacy Statement',

    disclaimertext:'I agree to the ',

    //forget password
    forgotPassword:'Forgot password?',

    //new password
    createNP:'Forgot password?',
    newPassToContinue:'Please enter your new password to continue',
    newPassPlaceholder:'Enter your new password',
    rePassPlaceholder:'Re-enter your new password',


    //user ProductCategories
    searchFieldPlaceHolder:'Search anything...',


    internet_error:'No internet connection'




};
export default MyStrings;


//region css
export const  productDetailsStyle='.descData h4\n' +
    '{\n' +
    '    font-size: 15px;\n' +
    '    color: #144d88;\n' +
    '    font-weight: 700;\n' +
    '    margin: 0;\n' +
    '}\n' +
    '.descData h4 span\n' +
    '{\n' +
    '    margin-left: 15px;\n' +
    '    font-weight: 400;\n' +
    '    color: #000;\n' +
    '}\n' +
    '.descData p\n' +
    '{\n' +
    '    font-size: 14px;\n' +
    '    color: #144d88;\n' +
    '    margin: 0;\n' +
    '}\n' +
    '.panel-body .btnMain{\n' +
    '       background: #154d88;\n' +
    '        border: #154d88 solid 1px;\n' +
    '        color: #fff;\n' +
    '        margin-bottom: 10px;\n' +
    '    }\n' +
    '.panel-body .btnMain:hover\n' +
    '{\n' +
    '    background: #154d88;\n' +
    '    border: #154d88 solid 1px;\n' +
    '    color: #fff;\n' +
    '}\n' +
    '.first_table {\n' +
    '    float: left;\n' +
    '    margin: 0 auto;\n' +
    '    width: 33%;\n' +
    '    margin-left: .2%;\n' +
    '    font-family: \'Roboto\', sans-serif;\n' +
    '    font-weight: 400;\n' +
    '    font-size: 14px;\n' +
    '    line-height: 20px;\n' +
    '   \n' +
    '    text-align: left;\n' +
    '}\n' +
    '.first_table td {\n' +
    '    padding: 8px;\n' +
    '    color: #000;\n' +
    '}\n' +
    '.first_table td strong{\n' +
    '    color: #144d88;\n' +
    '}\n' +
    '\n' +
    '@media screen and (max-width: 650px) {\n' +
    '    .first_table {\n' +
    '        width: 100%;\n' +
    '    }\n' +
    '    .first_table td\n' +
    '    {\n' +
    '        width: 40%;\n' +
    'border-bottom: #ccc solid 1px;\n' +
    '    }\n' +
    '    .boxHeader .panel-title {\n' +
    '        font-size: 21px;\n' +
    '    }\n' +
    '}'


//endregion

export const staticPageCss={
    csss:'.aboutInner {\n' +
        '    text-align: justify;\n' +
        '}'+
        'faqInner p {\n' +
        '    font-size: 16px;\n' +
        '    line-height: 28px;\n' +
        '    font-family: "Roboto", sans-serif;\n' +
        '}'+
        '.boxBody small {\n' +
        '    margin: 10px 0;\n' +
        '    display: block;\n' +
        '    color: #0000ff;\n' +
        '}'+
        '.panel-group {\n' +
        '    margin-bottom: 5px;\n' +
        '}'+
        '.panel-group .panel {\n' +
        '    border-radius: 0;\n' +
        '    border: 0;\n' +
        '    box-shadow: none;\n' +
        '}'+
        '.boxHeader {\n' +
        '    background: #154d88 !important;\n' +
        '    border-left: #00c1b0 solid 30px !important;\n' +
        '}'+
        '.faqInner .boxHeader {\n' +
        '    padding: 15px 10px !important;\n' +
        '}'+
        '.panel-default > .panel-heading + .panel-collapse > .panel-body {\n' +
        '    border-top-color: transparent;\n' +
        '    background: transparent;\n' +
        '    border: none;\n' +
        '    border-color: transparent;\n' +
        '}'+
        '.panel-body ul {\n' +
        '    padding-left: 10px;\n' +
        '}'+
        '.faqInner ul li {\n' +
        '    font-size: 16px;\n' +
        '    font-family: "Roboto", sans-serif;\n' +
        '    font-weight: 400;\n' +
        '}'+'.panel-title > a {\n' +
        '    text-decoration: none;\n' +
        '    color: #fff;\n' +
        '}'+'.faqInner .panel-title {\n' +
        '    text-transform: inherit;\n' +
        '    font-size: 18px;\n' +
        '    font-family: "Roboto", sans-serif;\n' +
        '    font-weight: 400;\n' +
        '}'+'.boxHeader h2 {\n' +
        '    margin: 0;\n' +
        '    font-size: 24px;\n' +
        '    font-weight: 500;\n' +
        '    font-family: "Raleway", sans-serif;\n' +
        '    color: #fff;\n' +
        '    text-transform: uppercase;\n' +
        '}'+'.faqInner .faqHeader {\n' +
        '    margin-bottom: 20px;\n' +
        '}'+'.aboutInner {\n' +
        '    padding: 10px 10px 10px !important;\n' +
        '}'+'.faqInner {\n' +
        '    padding: 10px 10px 10px !important;\n' +
        '}'
}


