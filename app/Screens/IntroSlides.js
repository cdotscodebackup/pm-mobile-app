import React from 'react';
import {
    StyleSheet, Text, TouchableNativeFeedback, View,
} from 'react-native';
import MyCarousel from '../Components/Carousel';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';

export const IntroSlides = ({navigation}) => {

    const getStartedPress = () => {
        navigation.replace('Preference', {skip: true, back: false});
        // navigation.replace('Dashboard')
    };

    return (
        <View style={styles.mainContainer}>
            <View style={styles.carousalContainer}>
                <MyCarousel images={[]}/>
            </View>

            <TouchableNativeFeedback onPress={getStartedPress}>
                <View style={styles.getStartedContainer}>
                    <Text style={styles.getStartedText}>
                        GET STARTED
                    </Text>
                </View>
            </TouchableNativeFeedback>
        </View>
    );


};
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: Colors.app_background_color,

    },
    welcomeContainer: {
        alignItems: 'center',
        marginTop: dimen.app_padding,
    },
    welcomeText: {
        fontSize: 18,
        // fontStyle:'bold'
    },
    getStartedContainer: {
        alignItems: 'center',
        marginBottom: dimen.app_padding,
        marginLeft: dimen.app_padding,
        marginRight: dimen.app_padding,
    },
    getStartedText: {

        width: '100%',
        textAlign: 'center',
        padding: dimen.app_padding,
        backgroundColor: Colors.primary_color,
        color: Colors.button_text_color,
    },
    carousalContainer: {

        flex: 1,
        // backgroundColor:'green',
        // margin: dimen.app_padding,
        // padding: dimen.app_padding,
        paddingBottom: 0,

    },
});

