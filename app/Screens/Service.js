import React, {useState, useEffect} from 'react';
import {View, StyleSheet, TouchableWithoutFeedback, TouchableNativeFeedback, StatusBar, ScrollView} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';
import Colors from '../Styles/Colors';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen, {deviceWidth} from '../Styles/Dimen';
import MyImageButton, {MyImage, MyImageBackground} from '../Components/MyImageButton';
import {BackArrow} from '../Components/BackArrow';
import {HeaderContainer, HeaderTitle} from '../Components/Common';


export const Service = ({route, navigation}) => {

    const [serviceData, setServiceData] = useState(null);


    useEffect(() => {
        let data = route.params;
        // console_log(data.data.text);
        setServiceData(data);
    }, []);


    return (
        <MyBackgroundImage isBottom={false}>
            <HeaderContainer>
                <BackArrow onPress={() => navigation.pop()}/>
                <HeaderTitle title={'SERVICE'}/>
            </HeaderContainer>

            {serviceData !== null &&
            <View>
                <View>
                    <MyImage
                        // source={require('../Asset/parking_summary.png')}
                        source={serviceData.data.icon}
                        imageContainerStyle={{
                            width: 40,
                            height: 40,
                            alignSelf: 'center',
                            padding: dimen.app_padding,
                            marginTop: dimen.app_padding * 3,
                            resizeMode: 'contain',
                        }}/>
                    <AppText style={{
                        fontSize: 18,
                        fontWeight: 'bold',
                        alignSelf: 'center',
                        color: Colors.normal_text_color,
                        padding: dimen.app_padding,
                    }}>
                        {serviceData.data.text}
                    </AppText>
                    <View style={{
                        height: 1,
                        backgroundColor: Colors.brand_store_border_color,
                        marginRight: 20,
                        marginLeft: 20,
                    }}/>
                </View>

                <AppText style={{fontSize: 14, color: Colors.normal_text_color, padding: 20}}>
                    {serviceData.data.desc}
                </AppText>

            </View>
            }


        </MyBackgroundImage>
    );

};


const styles = StyleSheet.create({


});
