import React, {useState, useRef, useEffect} from 'react';
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    Linking, ScrollView,
} from 'react-native';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import Colors from '../Styles/Colors';
import dimen, {deviceHeight, deviceWidth} from '../Styles/Dimen';

import {HeaderContainer, HeaderTitle} from '../Components/Common';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import AppText from '../Components/AppText';
import {BackArrow} from '../Components/BackArrow';
import {console_log, errorAlert} from '../Classes/auth';
import {BrandDescription} from '../Components/BrandDetailsComponents';
import MyTextButton from '../Components/MyTextButton';
import RBSheet from "react-native-raw-bottom-sheet";
import {BookRideComponent, CallComponent} from "../Components/CallComponent";

export const GettingHere = ({navigation}) => {

    const mapRef = useRef(null);
    const refRBSheet = useRef();


    const directionsPress = () => {
        let packagesUrl = 'https://www.google.com/maps/place/Packages+Mall/@31.4717537,74.3542473,18.61z/data=!4m5!3m4!1s0x0:0x32ba63a1571efb2a!8m2!3d31.4717761!4d74.3545551';
        Linking.canOpenURL(packagesUrl).then(res => {
            Linking.openURL(packagesUrl).then(res => {
            }).catch(err => errorAlert('Unable to open'));
        }).catch(err => errorAlert('Unable to open'));


        return;
        //region linking
        let latitude = 31.471168;
        let longitude = 74.355224;
        let label = 'Lahroe';
        const url = Platform.select({
            ios: 'maps:' + latitude + ',' + longitude,
            android: 'geo:' + latitude + ',' + longitude,
        });
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                return Linking.openURL(url);
            } else {
                const browser_url =
                    'https://www.google.de/maps/@' +
                    latitude +
                    ',' +
                    longitude +
                    '?q=' +
                    label;
                return Linking.openURL(browser_url);
            }
        });
        return;
        Linking.canOpenURL('geo:31.471168,74.355224').then(res => {
            Linking.openURL('geo:31.471168,74.355224').then(res => {
            }).catch(err => errorAlert('Unable to open'));
        }).catch(err => errorAlert('Unable to open'));
//endregion

    };


    const bookARide = () => {
        refRBSheet.current.open();
    };
    const bookARideType = (type) => {
        let packagesUrl = '';
        if (type === 'uber')
            packagesUrl = 'https://m.uber.com/ul/?action=setPickup&client_id=123123&pickup=my_location&dropoff[formatted_address]=Packages%20Mall%2C%20Main%20Walton%20Road%2C%20Nishtar%20Town%2C%20Lahore%2C%20Pakistan&dropoff[latitude]=31.471776&dropoff[longitude]=74.354555';
        else
            packagesUrl = 'careem://';
        Linking.canOpenURL(packagesUrl).then(res => {
            Linking.openURL(packagesUrl).then(res => {
            }).catch(err => errorAlert('Unable to open'));
        }).catch(err => errorAlert('Unable to open'));

    };
    return (
        <MyBackgroundImage isBottom={false}>
            <HeaderContainer>
                <HeaderTitle title={'PLAN A VISIT'}/>
                <BackArrow onPress={() => navigation.goBack()}/>
            </HeaderContainer>

            <ScrollView style={{flex: 1}}>
                <View style={{marginTop: 10}}>
                    <MapView
                        ref={mapRef}
                        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                        style={styles.map}
                        region={{
                            latitude: 31.471168,
                            longitude: 74.355224,
                            latitudeDelta: 0.004455,
                            longitudeDelta: 0.0044521,
                        }}
                    >
                        <Marker
                            coordinate={{
                                latitude: 31.471168,
                                longitude: 74.355224,
                            }}
                        />
                    </MapView>
                </View>
                <GetDirectionButton onPress={directionsPress}/>
                <ByCarItem
                    title={'By Car'}
                    desc={'Packages Mall is located in the heart of Lahore, at an interchange on Walton Road. It’s right in the\n' +
                    'middle of DHA and Model Town, linking to all the close by areas.'}
                />
                <Divider/>
                <ByCarItem
                    title={'By Careem - Uber'}
                    desc={'Uber and Careem services are also offered to our visitors. Designated gates have been set for\n' +
                    'the drop-off and pick-up points.\n' +
                    '\n' +
                    'Uber drop-off and pick-up area is marked at Gate 1 and Gate 6\n' +
                    'Careem drop-off and pick-up area is marked at Gate 1 and Gate 6'}
                />
                <Divider/>

            </ScrollView>

            <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                padding: dimen.app_padding,
                backgroundColor: Colors.search_bg_color
            }}>
                <View style={{flex: 1}}>
                    <MyTextButton
                        onPress={bookARide}
                        buttonText={'BOOK A RIDE'}
                        buttonTextStyle={{fontSize: 12, color: Colors.primary_color}}
                        buttonContainerStyle={{
                            backgroundColor: 'transparent',
                            borderColor: Colors.primary_color,
                            borderWidth: 1,
                            padding: 10,
                        }}
                    />
                </View>

                {/*<View style={{flex: 1, marginLeft: 6}}>*/}
                {/*<MyTextButton*/}
                {/*    onPress={directionsPress}*/}
                {/*    buttonText={'NAVIGATE'}*/}
                {/*    buttonTextStyle={{color: Colors.normal_text_color}}*/}
                {/*    buttonContainerStyle={{*/}
                {/*        backgroundColor: Colors.primary_color,*/}
                {/*        borderColor: Colors.primary_color,*/}
                {/*        borderWidth: 1,*/}
                {/*        padding: 10,*/}
                {/*    }}*/}
                {/*/>*/}
                {/*</View>*/}
            </View>


            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={false}
                height={deviceHeight / 4}
                customStyles={{
                    wrapper: {
                        // backgroundColor: Colors.bottom_up_bg_color,
                        backgroundColor: 'transparent',
                    },
                    draggableIcon: {
                        backgroundColor: Colors.search_bg_color,
                    },
                    container: {
                        backgroundColor: Colors.bottom_up_bg_color,
                    }
                }}
            >
                <BookRideComponent onPress={bookARideType}/>
            </RBSheet>


        </MyBackgroundImage>
    );

};


const Divider = () => {
    return (
        <View style={{
            backgroundColor: Colors.brand_store_border_color,
            height: 1, marginRight: dimen.app_padding, marginLeft: dimen.app_padding,
        }}/>

    );
};

const ByCarItem = (props) => {

    return (
        <View>
            <AppText style={{
                fontSize: 24,
                // fontFamily: 'crimson-roman',
                color: Colors.normal_text_color,
                marginLeft: dimen.small_padding,
            }}
            heading={true}
            >
                {props.title}
            </AppText>
            <BrandDescription
                // description={'This is the description of the brand, it will show detailed description of the brand in text form This is the description of the brand, it will show detailed description of the brand in text form'}
                description={props.desc}
            />
        </View>
    );
};

const GetDirectionButton = (props) => {

    return (
        <View>
            <TouchableWithoutFeedback onPress={() => props.onPress()}>
                <View style={{alignItems: 'center', padding: dimen.app_padding}}>
                    <AppText style={{
                        color: Colors.primary_color,
                        fontSize: 15,
                        borderBottomWidth: 1,
                        borderBottomColor: Colors.primary_color,
                        paddingVertical: 0
                    }}>
                        Click Here For Directions
                    </AppText>
                </View>
            </TouchableWithoutFeedback>
            <View style={{
                backgroundColor: Colors.brand_store_border_color,
                height: 1, marginRight: dimen.app_padding, marginLeft: dimen.app_padding,
            }}/>
        </View>
    );
};


const styles = StyleSheet.create({

    dashboardContainer: {
        backgroundColor: Colors.dashboard_bg_color,

    },
    dashboardHeadingContainer: {
        paddingLeft: dimen.app_padding * 2,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: dimen.app_padding * 2,
        marginBottom: dimen.app_padding * 2,


    },
    nameText: {
        fontSize: 16,
        backgroundColor: Colors.primary_color,
        color: Colors.button_text_color,
        fontWeight: 'bold',
        paddingRight: dimen.app_padding * 2,
        paddingLeft: dimen.app_padding * 2,
        padding: dimen.small_padding,
        borderTopLeftRadius: 25,
        borderBottomLeftRadius: 25,
    },

    container: {
        ...StyleSheet.absoluteFillObject,
        flex: 1,
        alignItems: 'center',
    },
    map: {
        // ...StyleSheet.absoluteFillObject,
        zIndex: -1,
        width: deviceWidth,
        height: deviceWidth / 1.4,

    },

});
