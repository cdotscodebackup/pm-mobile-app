import React, {useState, useEffect} from 'react';
import {View, StyleSheet, TouchableWithoutFeedback, TouchableNativeFeedback, StatusBar, ScrollView} from 'react-native';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';

import {WebView} from 'react-native-webview';
import {HeaderContainer, HeaderTitle} from '../Components/Common';
import {checkNetwork, NoInternetView} from '../Components/NoInternetView';
import {Loader} from '../Components/Loader';
import {MyUrls} from "../Classes/MyUrls";

export const Map = ({navigation}) => {
    const [isLoading, setIsLoading] = useState(false);
    const [isInternet, setIsInternet] = useState(true);


    useEffect(() => {
        let sub = navigation.addListener('focus', () => {
            checkNetwork(setIsInternet);
        });

        return sub;
    }, []);

    return (
        <MyBackgroundImage isBottom={false}>
            <HeaderContainer>
                <HeaderTitle title={'MALL MAP'}/>
            </HeaderContainer>

            {isInternet &&
            <View style={{flex: 1, marginTop: 15}}>
                <WebView
                    source={{uri: MyUrls.map_url}}
                    style={{backgroundColor: Colors.app_background_color}}
                    onLoadStart={() => setIsLoading(true)}
                    onLoadEnd={() => setIsLoading(false)}
                />

                <Loader loading={isLoading}/>
            </View>
            }

            <NoInternetView status={!isInternet}/>


        </MyBackgroundImage>
    );

};


const styles = StyleSheet.create({

});
