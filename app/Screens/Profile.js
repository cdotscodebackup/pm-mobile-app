import React, {useState, useEffect, useRef} from 'react';
import {View, StyleSheet, TouchableWithoutFeedback, TouchableNativeFeedback, StatusBar, ScrollView} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';
import Colors from '../Styles/Colors';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen, {deviceHeight} from '../Styles/Dimen';
import MyImageButton, {MyImage, MyImageBackground} from '../Components/MyImageButton';
import RBSheet from 'react-native-raw-bottom-sheet';
import {CallComponent} from '../Components/CallComponent';
import {HeaderContainer, HeaderTitle} from '../Components/Common';
import {ServiceItem} from '../Cards/ServiceCard';
import {BackArrow} from '../Components/BackArrow';
import {clearAllData, console_log, getObject} from '../Classes/auth';
import {Keys} from '../Classes/Keys';


export const Profile = ({navigation}) => {

    const [searchText, setSearchText] = useState('');
    const [isLogin, setIsLogin] = useState(false);
    const [name, setName] = useState('');

    useEffect(() => {
        getObject(Keys.user_data_key)
            .then(res => {
                if (res.status) {
                    setName(res.data.user_meta.first_name[0] + ' ' + res.data.user_meta.last_name[0]);
                } else {
                }
            })
            .catch(err => console_log('false'));

    }, []);


    const logoutPress = () => {
        clearAllData().then(res => {
            navigation.pop();
        })
            .catch(err => console_log(err));

    };


    return (
        <MyBackgroundImage isBottom={false}>
            <HeaderContainer>
                <HeaderTitle title={'PROFILE'}/>
                <BackArrow onPress={() => navigation.pop()}/>
            </HeaderContainer>
            <ScrollView>

                <User name={name}/>

                <ServiceItem icon={require('../Asset/profile.png')}
                             text={'Personal Information'}
                             onPress={() => navigation.navigate('PersonalInformation')}
                />

                <ServiceItem icon={require('../Asset/preference_icon.png')}
                             text={'My Preferences'}
                             onPress={() => navigation.navigate('Preference', {skip: false, back: true})}
                />
                <LegalText/>

                <ServiceItem icon={require('../Asset/privacy_policy.png')}
                             text={'Privacy Policy'}
                             onPress={() => navigation.navigate('MyWebView',
                                 {
                                     link: 'http://packagesmall.com/privacy-policy-web-view/',
                                     title: 'PRIVACY POLICY',
                                 })}
                />
                <ServiceItem icon={require('../Asset/terms_conditions.png')}
                             text={'Terms & Conditions'}
                             onPress={() => navigation.navigate('MyWebView',
                                 {
                                     link: 'http://packagesmall.com/terms-and-conditions-web-view/',
                                     title: 'TERMS AND CONDITIONS',
                                 })}/>

                <ServiceItem icon={require('../Asset/antifraud.png')}
                             text={'Anti-Fraud Disclaimer'}
                             onPress={() => navigation.navigate('MyWebView',
                                 {
                                     link: 'http://packagesmall.com/anti-fraud-disclaimer/',
                                     title: 'ANTI-FRAUD DISCLAIMER',
                                 })}/>

                <LogoutButton logoutPress={logoutPress}/>
                <Version/>

            </ScrollView>
        </MyBackgroundImage>
    );

};


const User = (props) => {

    return (
        <TouchableWithoutFeedback onPress={props.onPress}>
            <View style={{flexDirection: 'row', alignItems: 'center', padding: dimen.app_padding}}>
                <MyImage
                    source={require('../Asset/my_account.png')}
                    imageContainerStyle={{width: 34, height: 34}}
                />

                <View style={{marginLeft: 10}}>
                    <AppText style={{fontSize: 24, color: Colors.button_text_color}} heading={true}>
                        {props.name}
                    </AppText>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

const LogoutButton = (props) => {


    return (
        <TouchableWithoutFeedback onPress={props.logoutPress}>
            <View style={{padding: dimen.app_padding, marginTop: 10, marginBottom: 10}}>
                <AppText style={{color: Colors.primary_color, fontSize: 15}}>
                    Log out
                </AppText>
            </View>
        </TouchableWithoutFeedback>
    );
};

const Version = (props) => {
    return (
        <View style={{padding: dimen.app_padding}}>
            <AppText style={{color: Colors.similar_brands_text_color, fontSize: 10}}>
                Version 1.0.0
            </AppText>
            <AppText style={{color: Colors.similar_brands_text_color, fontSize: 10}}>
                {'\u00A9'}2020 The Packages Mall
            </AppText>
        </View>
    );
};

const LegalText = () => {

    return (
        <AppText style={{
            fontSize: 15,
            color: Colors.similar_brands_text_color,
            margin: dimen.app_padding,
            marginBottom: 5,
            // backgroundColor:'red'
        }}>
            LEGAL
        </AppText>
    );
};


const styles = StyleSheet.create({});
