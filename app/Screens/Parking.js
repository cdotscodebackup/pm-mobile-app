import React, {useState, useEffect} from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import Colors from '../Styles/Colors';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen, {deviceWidth} from '../Styles/Dimen';
import MyImageButton, {MyImage, MyImageBackground} from '../Components/MyImageButton';
import {HeaderContainer, HeaderTitle} from '../Components/Common';


export const Parking = ({navigation}) => {


    return (
        <MyBackgroundImage isBottom={false}>
            <HeaderContainer>
                <HeaderTitle title={'MALL PARKING'}/>
            </HeaderContainer>

            <ScrollView style={{flex:1,}}>
                <View>
                    <AppText style={{fontSize: 24, color: Colors.normal_text_color, padding: dimen.app_padding}}
                             heading={true}>
                        Parking Summary
                    </AppText>

                    <View style={{justifyContent: 'center', alignItems: 'stretch'}}>
                        <AppText style={{
                            fontSize: 18,
                            alignSelf: 'center',
                            color: Colors.normal_text_color,
                            padding: 10,
                            paddingBottom: 0,
                        }}
                                 heading={true}>
                            Total Parking
                        </AppText>
                        <AppText style={{
                            fontSize: 18, color: Colors.normal_text_color,
                            padding: 10, paddingTop: 0, alignSelf: 'center',
                        }}
                                 heading={true}>
                            Spaces
                        </AppText>


                        <AppText style={{
                            fontSize: 24, fontWeight: 'bold', alignSelf: 'center',
                            color: Colors.normal_text_color,
                            padding: 10, paddingTop: 0, marginTop: 0
                        }}
                                 heading={true}>
                            2,000
                        </AppText>

                        <View style={{
                            backgroundColor: Colors.brand_store_border_color,
                            // width:'100%',
                            // flex:1,
                            height: 1, marginRight: dimen.app_padding, marginLeft: dimen.app_padding,
                        }}/>

                    </View>

                    <AppText style={{fontSize: 14, color: Colors.normal_text_color, padding: dimen.app_padding}}>
                        Finding a parking spot should be the least of your concerns when you come to Packages Mall. Our
                        ample 2,000 cars parking allows you to easily access and park your car in one of our parking
                        spaces
                        including the green, pink and open parking.
                    </AppText>


                    <AppText style={styles.headingPar}
                             heading={true}>
                        Pink Parking
                    </AppText>
                    <AppText style={styles.descPar}>
                        Indoor parking is also available at Packages Mall. The Green color distinguishes the indoor
                        parking from one another.
                    </AppText>


                    <AppText style={styles.headingPar}
                             heading={true}>
                        Green Parking
                    </AppText>

                    <AppText style={styles.descPar}>
                        Indoor parking is also available at Packages Mall. The Green color distinguishes the indoor
                        parking from one another.
                    </AppText>

                    <AppText style={styles.headingPar}
                             heading={true}>
                        Open Parking
                    </AppText>

                    <AppText style={styles.descPar}>
                        Packages Mall provides a very spacious open parking to help you park near your favorite stores.
                    </AppText>

                </View>

            </ScrollView>

        </MyBackgroundImage>
    );

};


const styles = StyleSheet.create({
    headingPar: {fontSize: 18, color: Colors.normal_text_color, padding: dimen.app_padding,paddingBottom:0},
    descPar: {fontSize: 14, color: Colors.normal_text_color, padding: dimen.app_padding,paddingTop:5,},
});
