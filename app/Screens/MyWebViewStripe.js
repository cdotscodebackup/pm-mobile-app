import React, {memo, useState, useEffect} from 'react';
import {View, TouchableOpacity, Image} from 'react-native';
import {WebView} from 'react-native-webview';
import {console_log, saveObject} from '../Classes/auth';
import AsyncStorage from '@react-native-community/async-storage';


const STRIPE_PK = 'pk_test_gkEAGFAdUkkaZtIHSXbW1jCm00aS7wS0PA';

const PaymentView = ({route, navigation}) => {


    const [html, setHtml] = useState('');

    useEffect(() => {
        // get data from async storage
        let products = [
            {
                name: 'product1',
                price: 10,
            },
            {
                name: 'product2',
                price: 40,
            },
        ];
        let vat = 30;
        let dataSaveKey = 'productsData';



        // Code to save data
        // let dataToSave = {products: products, vat: vat};
        // AsyncStorage.setItem(dataSaveKey, JSON.stringify(dataToSave))
        //     .then(() => {
        //         console.log('data saved');
        //     })
        //     .catch((err) => {
        //         console.log('error while saving' + err);
        //     });
        // return;


        AsyncStorage.getItem(dataSaveKey)
            .then(res => {
                if (res !== null) {
                    let pData = JSON.parse(res);
                    setHtml(createHtml(pData.products, pData.vat));
                } else {
                    setHtml(createHtml([], 0));
                }
            })
            .catch(err => {
                setHtml(createHtml([], 0));
            });


    }, []);


    const createHtml = (products, vat) => {

        let pro = ' cartItems(' + JSON.stringify(products) + ',' + vat + ')';
        const htmlContent = `
            
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Payment Page</title>
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
                      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
                <script src="https://js.stripe.com/v3/"></script>
                <style>
            
                    .card-holder {
                        display: flex;
                        flex-direction: column;
                        height: 200px;
                        justify-content: space-around;
                        background-color: red;
                        border-radius: 20px;
                        padding: 10px;
                        padding-top: 20px;
                        padding-bottom: 20px;
                        margin-top: 50px;
                        margin-bottom: 50px;
                    }
            
                    .card-element {
                        height: 100px;
                        display: flex;
                        flex-direction: column;
                        justify-content: space-around;
                    }
            
                    .card-name {
                        padding: 20px;
                        color: #FFF;
                        font-weight: 500;
                        font-size: 25px;
                        background-color: transparent;
                        border: none;
            
                    }
            
                    input {
                        outline: none;
                        color: #FFF;
                        font-size: 25px;
                        font-weight: 500;
                        background-color: transparent;
                    }
            
                    .row {
                        margin-top: 50px;
                        display: flex;
                        flex-direction: row;
                        justify-content: center;
                        align-items: center;
                    }
            
                    .products-info {
            
                        height: 150px;
                        width: 100%;
                        padding: 20px;
                        text-align: center;
                    }
            
                    .card-errors {
                        color: red;
                    }
            
                    .pay-btn {
                        display: flex;
                        height: 50px;
                        justify-content: center;
                        align-items: center;
                    }
            
                    .product-details {
                        float: left;
                        width: 37%;
                    }
            
                    .product-price {
                        text-align: right;
                        width: 100%;
                    }
            
                    .product-quantity {
                        float: left;
                        width: 10%;
                    }
            
                    .product-removal {
                        float: left;
                        width: 9%;
                    }
            
                    .product-line-price {
                        float: left;
                        width: 12%;
                        text-align: right;
                    }
            
                    /* This is used as the traditional .clearfix class */
                    .group:before, .shopping-cart:before,
                    .column-labels:before,
                    .product:before,
                    .totals-item:before,
                    .group:after,
                    .shopping-cart:after,
                    .column-labels:after,
                    .product:after,
                    .totals-item:after {
                        content: "";
                        display: table;
                    }
            
                    .group:after, .shopping-cart:after,
                    .column-labels:after,
                    .product:after,
                    .totals-item:after {
                        clear: both;
                    }
            
                    .group, .shopping-cart,
                    .column-labels,
                    .product,
                    .totals-item {
                        zoom: 1;
                    }
            
                    /* Apply clearfix in a few places */
                    /* Apply dollar signs */
                    .product .product-price:before,
                    .product .product-line-price:before,
                    .totals-value:before {
                        content: "$";
                    }
            
                    /* Body/Header stuff */
                    body {
                        padding: 0px 30px 30px 20px;
                        font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, sans-serif;
                        font-weight: 100;
                    }
            
                    h1 {
                        font-weight: 100;
                    }
            
                    label {
                        color: #aaa;
                    }
            
                    .shopping-cart {
                        margin-top: -45px;
                    }
            
                    /* Column headers */
                    .column-labels label {
                        padding-bottom: 15px;
                        margin-bottom: 15px;
                        border-bottom: 1px solid #eee;
                    }
            
                    .column-labels .product-image,
                    .column-labels .product-details,
                    .column-labels .product-removal {
                        text-indent: -9999px;
                    }
            
                    /* Product entries */
                    .product {
                        margin-bottom: 20px;
                        padding-bottom: 10px;
                        border-bottom: 1px solid #eee;
                    }
            
                    .product .product-image {
                        text-align: center;
                    }
            
                    .product .product-image img {
                        width: 100px;
                    }
            
                    .product .product-details .product-title {
                        /*margin-right: 20px;*/
                        width: 100%;
                        font-family: "HelveticaNeue-Medium", "Helvetica Neue Medium";
                    }
            
                    .product .product-details .product-description {
                        margin: 5px 20px 5px 0;
                        line-height: 1.4em;
                    }
            
                    .product .product-quantity input {
                        width: 40px;
                    }
            
                    .product .remove-product {
                        border: 0;
                        padding: 4px 8px;
                        background-color: #c66;
                        color: #fff;
                        font-family: "HelveticaNeue-Medium", "Helvetica Neue Medium";
                        font-size: 12px;
                        border-radius: 3px;
                    }
            
                    .product .remove-product:hover {
                        background-color: #a44;
                    }
            
                    /* Totals section */
                    .totals .totals-item {
                        float: right;
                        clear: both;
                        width: 100%;
                        margin-bottom: 10px;
                    }
            
                    .totals .totals-item label {
                        float: left;
                        clear: both;
                        width: 79%;
                    }
            
                    .totals .totals-item .totals-value {
                        float: right;
                        width: 21%;
                        text-align: right;
                    }
            
                    .totals .totals-item-total {
                        font-family: "HelveticaNeue-Medium", "Helvetica Neue Medium";
                    }
            
                    .checkout {
                        float: right;
                        border: 0;
                        margin-top: 20px;
                        padding: 6px 25px;
                        background-color: #6b6;
                        color: #fff;
                        font-size: 25px;
                        border-radius: 3px;
                    }
            
                    .checkout:hover {
                        background-color: #494;
                    }
            
                    /* Make adjustments for tablet */
                    @media screen and (max-width: 650px) {
                        .shopping-cart {
                            margin: 0;
                            padding-top: 20px;
                            border-top: 1px solid #eee;
                        }
            
                        .column-labels {
                            display: none;
                        }
            
                        .product-image {
                            float: right;
                            width: auto;
                        }
            
                        .product-image img {
                            margin: 0 0 10px 10px;
                        }
            
                        .product-details {
                            float: none;
                            display: inline-flex;
                            margin-bottom: 10px;
                            width: 100%;
                        }
            
                        .product-price {
                            clear: both;
                            /*width: 70px;*/
                        }
            
                        .product-quantity {
                            width: 100px;
                        }
            
                        .product-quantity input {
                            margin-left: 20px;
                        }
            
                        .product-quantity:before {
                            content: "x";
                        }
            
                        .product-removal {
                            width: auto;
                        }
            
                        .product-line-price {
                            float: right;
                            width: 70px;
                        }
                    }
            
                    /* Make more adjustments for phone */
                    @media screen and (max-width: 350px) {
                        .product-removal {
                            float: right;
                        }
            
                        .product-line-price {
                            float: right;
                            clear: left;
                            width: auto;
                            margin-top: 10px;
                        }
            
                        .product .product-line-price:before {
                            content: "Item Total: $";
                        }
            
                        .totals .totals-item label {
                            width: 60%;
                        }
            
                        .totals .totals-item .totals-value {
                            width: 40%;
                        }
                    }
            
                    .btn-info {
                        color: #fff;
                        background-color: #28a745 !important;
                        border-color: #28a745 !important;
                    }
            
                    b, strong {
                        color: #000 !important;
                        font-weight: bold !important;
                    }
            
            
                </style>
            
            </head>
            <body onload="loadJs()">
            
            <!-- product info -->
            <div class="container-fluid">
            
                <div class="row">
                    <label class="card-errors" id="card-errors"></label>
                </div>
            
                <form>
                    <div class="card-holder">
                        <input type="text" placeholder="Card Holder Name" id="card-name" class="card-name"/>
                        <div id="card-element" class="card-element">
                            <div class="form-group">
                                <label for="card_number">Carn Number</label>
                                <input type="text" class="form-control" id="card_number" data-stripe="number">
                            </div>
                            <div class="form-row">
                                <label>
                                    <span>Card number</span>
                                    <input type="text" size="20" data-stripe="number">
                                </label>
                            </div>
            
                            <div class="form-row">
                                <label>
                                    <span>Expiration (MM/YY)</span>
                                    <input type="text" size="2" data-stripe="exp_month">
                                </label>
                                <span> / </span>
                                <input type="text" size="2" data-stripe="exp_year">
                            </div>
            
                            <div class="form-row">
                                <label>
                                    <span>CVC</span>
                                    <input type="text" size="4" data-stripe="cvc">
                                </label>
                            </div>
            
                            <div class="form-row">
                                <label>
                                    <span>Billing Zip</span>
                                    <input type="hidden" size="6" data-stripe="address_zip" value="400012">
                                </label>
                            </div>
            
            
                        </div>
                    </div>
            
                    <div class="shopping-cart" id="shopping-cart">
                    </div>
                    <div class="pay-btn">
                        <input type="submit" class="btn btn-info btn-lg" value="Pay Now"/>
                    </div>
            
                </form>
            
            
            </div>
            <script>
                var stripe = Stripe('${STRIPE_PK}');
                var elements = stripe.elements();
                function loadJs(){
                    
                    let pro=[
                        {
                            name: 'product1',
                            price: 10,
                        },
                        {
                            name: 'product2',
                            price: 20,
                        },
                    ]
                    
                    `
            +
            pro
            +
            `
                }
            
            
                var card = elements.create("card", {
                    hidePostalCode: true,
                    style: {
                        base: {
                            color: '#FFF',
                            fontWeight: 500,
                            fontFamily: 'Source Code Pro, Consolas, Menlo, monospace',
                            fontSize: '20px',
                            fontSmoothing: 'antialiased',
                            '::placeholder': {
                                color: '#664385',
                            },
                            ':-webkit-autofill': {
                                color: '#e39f48',
                            },
                        },
                        invalid: {
                            color: '#FC011F',
                            '::placeholder': {
                                color: '#FFCCA5',
                            },
                        },
                    }
                });
                // Add an instance of the card Element into the 'card-element' <div>.
                card.mount('#card-element');
                /**
                 * Error Handling
                 */
                //show card error if entered Invalid Card Number
                function showCardError(error){
                    document.getElementById('card-errors').innerHTML = ""
                    if(error){
                        document.getElementById('card-errors').innerHTML = error
                    }
                }
            
                card.on('change', function(event) {
                    if (event.complete) {
                        showCardError()
                        // enable payment button
                    } else if (event.error) {
                        const { message} = event.error
                        console.log(message)
                        showCardError(message)
                    }
                });
            
                card.mount('#card-element');
            
                /**
                 * Payment Request Element
                 */
                var paymentRequest = stripe.paymentRequest({
                    country: "IN",
                    currency: "inr",
                    total: {
                        amount: 10*100,
                        label: "Total"
                    }
                });
                var form =  document.querySelector('form');
                form.addEventListener('submit', function(e) {
                    e.preventDefault();
            
                    var additionalData = {
                        name: document.getElementById('card-name').value,
                        address_line1: undefined,
                        address_city:  undefined,
                        address_state: undefined,
                        address_zip: undefined,
                    };
            
                    stripe.createToken(card, additionalData).then(function(result) {
            
                        console.log(result);
                        if (result.token) {
                            window.postMessage(JSON.stringify(result));
                        } else {
                            window.postMessage(JSON.stringify(result));
                        }
                    });
                })
                
                function cartItems(products, vat) {
            
                    var html = '';
                    var total = 0;
                    var subTotal = 0;
                    if (products.length > 0) {
                        for (var i = 0; i < products.length; i++) {
                            html += '<div class="product">';
                            html += '<div class="product-details">';
                            html += '<div class="product-title">' + products[i].name + '</div>';
                            html += '<div class="product-price">' + products[i].price + '</div>';
                            html += '</div>';
                            html += '</div>';
                            subTotal = subTotal + products[i].price;
                        }
                    }
            
                    total = total + subTotal + vat;
            
            
                    html += ' <div class="bgColor">';
                    html += '<div class="totals">';
                    html += '<div class="totals-item">';
                    html += '<label>Subtotal</label>';
                    html += '<div class="totals-value" id="cart-subtotal">' + subTotal + '</div>';
                    html += '</div>';
                    html += '<div class="totals-item">';
                    html += '<label>VAT (20%)</label>';
                    html += '<div class="totals-value" id="cart-tax">' + vat + '</div>';
                    html += '</div>';
                    html += '<div class="totals-item totals-item-total">';
                    html += '<label><b>Total</b></label>';
                    html += '<div class="totals-value" id="cart-total"><b>' + total + '</b></div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
            
                    document.getElementById("shopping-cart").innerHTML = html;
            
            
                }
            </script>
            </body>
            </html>

    `;


        return htmlContent;
    };

    const onCheckStatus = (response) => {
        props.onCheckStatus(response);
    };

    const htmlContent = `

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Payment Page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://js.stripe.com/v3/"></script>
    <style>

        .card-holder {
            display: flex;
            flex-direction: column;
            height: 200px;
            justify-content: space-around;
            background-color: red;
            border-radius: 20px;
            padding: 10px;
            padding-top: 20px;
            padding-bottom: 20px;
            margin-top: 50px;
            margin-bottom: 50px;
        }

        .card-element {
            height: 100px;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
        }

        .card-name {
            padding: 20px;
            color: #FFF;
            font-weight: 500;
            font-size: 25px;
            background-color: transparent;
            border: none;

        }

        input {
            outline: none;
            color: #FFF;
            font-size: 25px;
            font-weight: 500;
            background-color: transparent;
        }

        .row {
            margin-top: 50px;
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
        }

        .products-info {

            height: 150px;
            width: 100%;
            padding: 20px;
            text-align: center;
        }

        .card-errors {
            color: red;
        }

        .pay-btn {
            display: flex;
            height: 50px;
            justify-content: center;
            align-items: center;
        }

        .product-details {
            float: left;
            width: 37%;
        }

        .product-price {
            text-align: right;
            width: 100%;
        }

        .product-quantity {
            float: left;
            width: 10%;
        }

        .product-removal {
            float: left;
            width: 9%;
        }

        .product-line-price {
            float: left;
            width: 12%;
            text-align: right;
        }

        /* This is used as the traditional .clearfix class */
        .group:before, .shopping-cart:before,
        .column-labels:before,
        .product:before,
        .totals-item:before,
        .group:after,
        .shopping-cart:after,
        .column-labels:after,
        .product:after,
        .totals-item:after {
            content: "";
            display: table;
        }

        .group:after, .shopping-cart:after,
        .column-labels:after,
        .product:after,
        .totals-item:after {
            clear: both;
        }

        .group, .shopping-cart,
        .column-labels,
        .product,
        .totals-item {
            zoom: 1;
        }

        /* Apply clearfix in a few places */
        /* Apply dollar signs */
        .product .product-price:before,
        .product .product-line-price:before,
        .totals-value:before {
            content: "$";
        }

        /* Body/Header stuff */
        body {
            padding: 0px 30px 30px 20px;
            font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-weight: 100;
        }

        h1 {
            font-weight: 100;
        }

        label {
            color: #aaa;
        }

        .shopping-cart {
            margin-top: -45px;
        }

        /* Column headers */
        .column-labels label {
            padding-bottom: 15px;
            margin-bottom: 15px;
            border-bottom: 1px solid #eee;
        }

        .column-labels .product-image,
        .column-labels .product-details,
        .column-labels .product-removal {
            text-indent: -9999px;
        }

        /* Product entries */
        .product {
            margin-bottom: 20px;
            padding-bottom: 10px;
            border-bottom: 1px solid #eee;
        }

        .product .product-image {
            text-align: center;
        }

        .product .product-image img {
            width: 100px;
        }

        .product .product-details .product-title {
            /*margin-right: 20px;*/
            width: 100%;
            font-family: "HelveticaNeue-Medium", "Helvetica Neue Medium";
        }

        .product .product-details .product-description {
            margin: 5px 20px 5px 0;
            line-height: 1.4em;
        }

        .product .product-quantity input {
            width: 40px;
        }

        .product .remove-product {
            border: 0;
            padding: 4px 8px;
            background-color: #c66;
            color: #fff;
            font-family: "HelveticaNeue-Medium", "Helvetica Neue Medium";
            font-size: 12px;
            border-radius: 3px;
        }

        .product .remove-product:hover {
            background-color: #a44;
        }

        /* Totals section */
        .totals .totals-item {
            float: right;
            clear: both;
            width: 100%;
            margin-bottom: 10px;
        }

        .totals .totals-item label {
            float: left;
            clear: both;
            width: 79%;
        }

        .totals .totals-item .totals-value {
            float: right;
            width: 21%;
            text-align: right;
        }

        .totals .totals-item-total {
            font-family: "HelveticaNeue-Medium", "Helvetica Neue Medium";
        }

        .checkout {
            float: right;
            border: 0;
            margin-top: 20px;
            padding: 6px 25px;
            background-color: #6b6;
            color: #fff;
            font-size: 25px;
            border-radius: 3px;
        }

        .checkout:hover {
            background-color: #494;
        }

        /* Make adjustments for tablet */
        @media screen and (max-width: 650px) {
            .shopping-cart {
                margin: 0;
                padding-top: 20px;
                border-top: 1px solid #eee;
            }

            .column-labels {
                display: none;
            }

            .product-image {
                float: right;
                width: auto;
            }

            .product-image img {
                margin: 0 0 10px 10px;
            }

            .product-details {
                float: none;
                display: inline-flex;
                margin-bottom: 10px;
                width: 100%;
            }

            .product-price {
                clear: both;
                /*width: 70px;*/
            }

            .product-quantity {
                width: 100px;
            }

            .product-quantity input {
                margin-left: 20px;
            }

            .product-quantity:before {
                content: "x";
            }

            .product-removal {
                width: auto;
            }

            .product-line-price {
                float: right;
                width: 70px;
            }
        }

        /* Make more adjustments for phone */
        @media screen and (max-width: 350px) {
            .product-removal {
                float: right;
            }

            .product-line-price {
                float: right;
                clear: left;
                width: auto;
                margin-top: 10px;
            }

            .product .product-line-price:before {
                content: "Item Total: $";
            }

            .totals .totals-item label {
                width: 60%;
            }

            .totals .totals-item .totals-value {
                width: 40%;
            }
        }

        .btn-info {
            color: #fff;
            background-color: #28a745 !important;
            border-color: #28a745 !important;
        }

        b, strong {
            color: #000 !important;
            font-weight: bold !important;
        }


    </style>

</head>
<body onload="loadJs()">

<!-- product info -->
<div class="container-fluid">

    <div class="row">
        <label class="card-errors" id="card-errors"></label>
    </div>

    <form>
        <div class="card-holder">
            <input type="text" placeholder="Card Holder Name" id="card-name" class="card-name"/>
            <div id="card-element" class="card-element">
                <div class="form-group">
                    <label for="card_number">Carn Number</label>
                    <input type="text" class="form-control" id="card_number" data-stripe="number">
                </div>
                <div class="form-row">
                    <label>
                        <span>Card number</span>
                        <input type="text" size="20" data-stripe="number">
                    </label>
                </div>

                <div class="form-row">
                    <label>
                        <span>Expiration (MM/YY)</span>
                        <input type="text" size="2" data-stripe="exp_month">
                    </label>
                    <span> / </span>
                    <input type="text" size="2" data-stripe="exp_year">
                </div>

                <div class="form-row">
                    <label>
                        <span>CVC</span>
                        <input type="text" size="4" data-stripe="cvc">
                    </label>
                </div>

                <div class="form-row">
                    <label>
                        <span>Billing Zip</span>
                        <input type="hidden" size="6" data-stripe="address_zip" value="400012">
                    </label>
                </div>


            </div>
        </div>

        <div class="shopping-cart" id="shopping-cart">
        </div>
        <div class="pay-btn">
            <input type="submit" class="btn btn-info btn-lg" value="Pay Now"/>
        </div>

    </form>


</div>
<script>

    var stripe = Stripe('${STRIPE_PK}');
    var elements = stripe.elements();
    function loadJs(){
        let pro=[
            {
                name: 'product1',
                price: 10,
            },
            {
                name: 'product2',
                price: 20,
            },
        ]
        cartItems(pro,20)
    }


    var card = elements.create("card", {
        hidePostalCode: true,
        style: {
            base: {
                color: '#FFF',
                fontWeight: 500,
                fontFamily: 'Source Code Pro, Consolas, Menlo, monospace',
                fontSize: '20px',
                fontSmoothing: 'antialiased',
                '::placeholder': {
                    color: '#664385',
                },
                ':-webkit-autofill': {
                    color: '#e39f48',
                },
            },
            invalid: {
                color: '#FC011F',
                '::placeholder': {
                    color: '#FFCCA5',
                },
            },
        }
    });
    // Add an instance of the card Element into the 'card-element' <div>.
    card.mount('#card-element');
    /**
     * Error Handling
     */
    //show card error if entered Invalid Card Number
    function showCardError(error){
        document.getElementById('card-errors').innerHTML = ""
        if(error){
            document.getElementById('card-errors').innerHTML = error
        }
    }

    card.on('change', function(event) {
        if (event.complete) {
            showCardError()
            // enable payment button
        } else if (event.error) {
            const { message} = event.error
            console.log(message)
            showCardError(message)
        }
    });

    card.mount('#card-element');

    /**
     * Payment Request Element
     */
    var paymentRequest = stripe.paymentRequest({
        country: "IN",
        currency: "inr",
        total: {
            amount: 10*100,
            label: "Total"
        }
    });
    var form =  document.querySelector('form');
    form.addEventListener('submit', function(e) {
        e.preventDefault();

        var additionalData = {
            name: document.getElementById('card-name').value,
            address_line1: undefined,
            address_city:  undefined,
            address_state: undefined,
            address_zip: undefined,
        };

        stripe.createToken(card, additionalData).then(function(result) {

            console.log(result);
            if (result.token) {
                window.postMessage(JSON.stringify(result));
            } else {
                window.postMessage(JSON.stringify(result));
            }
        });
    })
    
    function cartItems(products, vat) {

        var html = '';
        var total = 0;
        var subTotal = 0;
        if (products.length > 0) {
            for (var i = 0; i < products.length; i++) {
                html += '<div class="product">';
                html += '<div class="product-details">';
                html += '<div class="product-title">' + products[i].name + '</div>';
                html += '<div class="product-price">' + products[i].price + '</div>';
                html += '</div>';
                html += '</div>';
                subTotal = subTotal + products[i].price;
            }
        }

        total = total + subTotal + vat;


        html += ' <div class="bgColor">';
        html += '<div class="totals">';
        html += '<div class="totals-item">';
        html += '<label>Subtotal</label>';
        html += '<div class="totals-value" id="cart-subtotal">' + subTotal + '</div>';
        html += '</div>';
        html += '<div class="totals-item">';
        html += '<label>VAT (20%)</label>';
        html += '<div class="totals-value" id="cart-tax">' + vat + '</div>';
        html += '</div>';
        html += '<div class="totals-item totals-item-total">';
        html += '<label><b>Total</b></label>';
        html += '<div class="totals-value" id="cart-total"><b>' + total + '</b></div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';

        document.getElementById("shopping-cart").innerHTML = html;


    }
</script>
</body>
</html>

    `;

    const injectedJavaScript = `(function() {
        window.postMessage = function(data){
            window.ReactNativeWebView.postMessage(data);
        };
    })()`;

    const onMessage = (event) => {
        const {data} = event.nativeEvent;
        console.log(data);
        onCheckStatus(data);
    };

    return (
        <View style={{flex: 1}}>


            <WebView
                javaScriptEnabled={true}
                style={{flex: 1}}
                originWhitelist={['*']}
                source={{html: html}}
                injectedJavaScript={injectedJavaScript}
                onMessage={onMessage}
            />

            <View style={{position:'absolute',top:20,left:20}}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Image source={require('../Asset/back_arrow.png')}
                           tintColor={'black'}
                           style={[{width: 20, height: 20, resizeMode: 'contain'}]}/>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default (PaymentView);
