import React,{useRef} from 'react';
import {StyleSheet,Text, View,TouchableOpacity} from 'react-native';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';





export const Panic=()=>{




    return(
        <MyBackgroundImage>
            <TopBar title={'Panic'} icon={require('../Asset/back_arrow.png')}/>


        </MyBackgroundImage>
    )

}

const styles = StyleSheet.create({
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
})
