import React, {useState, useEffect} from 'react';
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    TouchableNativeFeedback,
    StatusBar,
    ScrollView,
    Linking,
} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';
import Colors from '../Styles/Colors';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen, {deviceWidth} from '../Styles/Dimen';
import MyImageButton, {MyImage, MyImageBackground} from '../Components/MyImageButton';
import {BackArrow} from '../Components/BackArrow';
import {console_log, errorAlert, getDataWithoutToken, isNetConnected} from '../Classes/auth';
import {MyUrls} from '../Classes/MyUrls';
import {EventCard} from '../Cards/EventCard';
import {HeaderContainer, HeaderTitle} from '../Components/Common';
import {Loader2} from '../Components/Loader';


export const EventDetails = ({route, navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [events, setEvents] = useState([]);
    const [eventDetail, setEventDetail] = useState(null);
    const [eventTitle, setEventTitle] = useState('');

    useEffect(() => {
        let {eventDetail} = route.params;
        setEventDetail(eventDetail);
        setEventTitle(eventDetail.title.rendered);
        // getEvents();
    }, []);


    const callStorePress = (item) => {
        Linking.canOpenURL('tel: ' + item.phone_no)
            .then(res => {
                Linking.openURL('tel: ' + item.phone_no)
                    .then()
                    .catch();
            })
            .catch(err => {
                errorAlert('No phone number available');
            });
    };


    return (
        <MyBackgroundImage isBottom={false}>
            <HeaderContainer>
                <HeaderTitle title={'EVENT DETAIL'}/>
                <BackArrow onPress={() => navigation.pop()}/>
            </HeaderContainer>

            {eventDetail !== null &&
            <View style={{flex: 1}}>
                <EventCard
                    data={eventDetail}
                    callStorePress={callStorePress}
                />
            </View>
            }

        </MyBackgroundImage>
    );

};


const styles = StyleSheet.create({});
