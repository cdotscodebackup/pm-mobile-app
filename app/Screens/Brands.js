import React, {useState, useEffect} from 'react';
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    TouchableNativeFeedback,
    StatusBar,
    ScrollView,
    ActivityIndicator,
} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';
import Colors from '../Styles/Colors';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen, {deviceWidth} from '../Styles/Dimen';
import {clearAllData, console_log, getDataWithoutToken, isNetConnected} from '../Classes/auth';
import {BackArrow} from '../Components/BackArrow';
import {MyUrls} from '../Classes/MyUrls';
import {
    BrandListHeader,
    FilterHeader,
    FilterList,
    HeaderContainer,
    HeaderTitle,
    SearchComponent,
} from '../Components/Common';
import {ShopCard} from '../Cards/ShopCard';
import {Loader, Loader2} from '../Components/Loader';

export const Brands = ({route, navigation}) => {

    const [isLoading, setIsLoading] = useState('');
    const [searchText, setSearchText] = useState('');
    const [categoryData, setCategoryData] = useState(null);
    const [categories, setCategories] = useState([]);
    const [filteredCategories, setFilteredCategories] = useState([]);
    const [brands, setBrands] = useState([]);
    const [filteredBrands, setFilteredBrands] = useState([]);
    const [filterList, setFilterList] = useState([{id: 1}, {id: 2}]);

    useEffect(() => {

        let {categoryData, brands_link} = route.params;
        setCategoryData(categoryData);
        // console_log(categoryData)

        getShopsBrands(categoryData, brands_link);


    }, []);


    const setCategoryItems = (data) => {

        let sortedData = [];
        let previousChar = ' ';
        data.sort(function (a, b) {
            let textA = a.title.rendered.toUpperCase();
            let textB = b.title.rendered.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
        data.forEach((item, index) => {

            let firstChar = item.title.rendered.toLowerCase().charAt(0);
            if (firstChar.toLowerCase() !== previousChar.toLowerCase()) {
                previousChar = firstChar;
                let newItem = {
                    isFirst: true,
                    char: firstChar,
                };
                sortedData.push({...item, ...newItem});
            } else {
                let newItem = {
                    isFirst: false,
                    char: firstChar,
                };
                sortedData.push({...item, ...newItem});
            }
        });
        setBrands(sortedData);
        setFilteredBrands(sortedData);
    };

    const getShopsBrands = (categoryData, brands_link) => {
        isNetConnected().then(res => {
            getDataWithoutToken(brands_link, 'GET', '', setIsLoading)
                .then(res => {
                    if (res && res.length > 0) {
                        if (categoryData !== null) {
                            let filtered = res.filter(item => {
                                // if (item['shop-category'].includes(categoryData.id)) {
                                let fid = item['dine-cuisine-category'] !== undefined ? 'dine-cuisine-category'
                                    : item['shop-category'] !== undefined ? 'shop-category' : '';

                                if (item[fid].includes(categoryData.id)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            });
                            setCategoryItems(filtered);
                        } else {
                            setCategoryItems(res);
                        }
                    } else {
                        setCategoryItems([]);
                    }
                });
        });
    };

    const filterCategories = (text) => {
        let fCategories = brands.filter(item => {
            return item.title.rendered.toLowerCase().includes(text.toLowerCase());
        });
        setFilteredBrands(fCategories);
    };

    const categoryPress = (item) => {
        navigation.navigate('BrandStore', {itemData: item, similarBrands: brands});
    };

    const searchTextChange = (text) => {
        setSearchText(text);
        filterCategories(text);
    };


    return (
        <MyBackgroundImage isBottom={false}>
            <HeaderContainer>
                <BackArrow onPress={() => navigation.pop()}/>
                <HeaderTitle title={categoryData !== null ? categoryData.name.toUpperCase() : 'ENTERTAINMENT'}/>
                {/*<HeaderTitle title={'FASHION WOMEN'}/>*/}
            </HeaderContainer>

            <View style={styles.searchAndFilterContainer}>
                <SearchComponent text={searchText} setText={searchTextChange}/>
                {/*<FilterList filterList={filterList}/>*/}
            </View>


            <View style={styles.listContainer}>
                <FlatList
                    style={{flex: 1}}
                    data={filteredBrands}
                    keyExtractor={(item, index) => item.id.toString()}
                    numColumns={1}
                    bounces={false}
                    renderItem={({item, index}) => (
                        <ShopCard data={item} onPress={categoryPress}/>
                    )}
                    ListHeaderComponent={
                        <View>
                            <Loader2 loading={isLoading}/>
                            <BrandListHeader title={'DISPLAYING ' + filteredBrands.length + ' RESULTS'}/>
                        </View>
                    }
                />

            </View>


        </MyBackgroundImage>
    );

};


const colors = {
    background: {
        light: 'white',
        dark: '#8e8e93',
    },

    seperatorLine: '#e6ebf2',

    text: {
        dark: '#1c1b1e',
    },

    primary: '#007aff',
};
const sizes = {
    itemHeight: 40,
    headerHeight: 30,

    spacing: {
        regular: 15,
    },
};


const styles = StyleSheet.create({
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: Colors.search_bg_color,
        padding: 0,
        paddingLeft: 10,
    },
    inputField: {
        padding: 3,
        paddingLeft: 10,
        color: Colors.normal_text_color,
        flex: 1,
    },

    container: {
        flex: 1,
        backgroundColor: colors.background.light,
    },

    listItemContainer: {
        flex: 1,
        height: sizes.itemHeight,
        paddingHorizontal: sizes.spacing.regular,
        justifyContent: 'center',
        borderTopColor: colors.seperatorLine,
        borderTopWidth: 1,
        backgroundColor: Colors.app_background_color,
    },

    listItemLabel: {
        color: Colors.normal_text_color,
        fontSize: 14,
    },

    sectionHeaderContainer: {
        height: sizes.headerHeight,
        backgroundColor: Colors.app_background_color,
        justifyContent: 'center',
    },

    sectionHeaderLabel: {
        color: Colors.normal_text_color,
    },
    listContainer: {flex: 1, padding: dimen.app_padding},
    searchAndFilterContainer: {padding: dimen.app_padding},

});
