import React, {useState, useEffect} from 'react';
import {View, StyleSheet, TouchableWithoutFeedback, TouchableNativeFeedback, StatusBar} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';
import Colors from '../Styles/Colors';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen from '../Styles/Dimen';
import {PreferenceCard} from '../Cards/PreferenceCard';
import MyTextButton from '../Components/MyTextButton';
import {HeaderContainer, HeaderSkip, HeaderTitle} from '../Components/Common';
import {console_log, getDataWithoutToken, getObject, isNetConnected, saveObject} from '../Classes/auth';
import {MyUrls} from '../Classes/MyUrls';
import {BackArrow} from '../Components/BackArrow';
import {Loader, Loader2} from '../Components/Loader';
import {Keys} from "../Classes/Keys";


export const Preference = ({route, navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [searchText, setSearchText] = useState('');
    const [categories, setCategories] = useState([]);
    const [preferences, setPreferences] = useState([]);
    const [updatePreference, setUpdatePreference] = useState(1);
    const [back, setBack] = useState(false);
    const [skip, setSkip] = useState(false);

    useEffect(() => {
        let {back, skip} = route.params;
        console_log(route.params);
        setBack(back);
        setSkip(skip);

        getCategories();

        getObject(Keys.preference_key).then(res => {
            if (res.status) {
                setPreferences(res.data)
            }
        })

    }, []);

    const getCategories = () => {
        isNetConnected().then(res => {
            getDataWithoutToken(MyUrls.shop_category_url, 'GET', '', setIsLoading)
                .then(res => {
                    if (res && res.length > 0) {
                        setCategories((res));
                    } else {
                        setCategories([]);
                    }
                });
        });
    };

    const donePress = () => {
        if (back) {
            navigation.pop();
        } else {
            navigation.replace('Dashboard');
        }
    };
    const skipPress = () => {
        navigation.replace('Dashboard');
    };

    const addPreference = (item) => {
        preferences.push(item.id);
        setUpdatePreference(updatePreference + 1);
        saveObject(Keys.preference_key, preferences).then().catch()
    };
    const removePreference = (item) => {
        let fil = preferences.filter(pre => pre !== item.id);
        setPreferences(fil);
        setUpdatePreference(updatePreference + 1);
        console_log(fil)
        saveObject(Keys.preference_key, fil).then().catch()
    };

    return (
        <MyBackgroundImage isBottom={false}>
            <HeaderContainer>
                <HeaderTitle title={'PREFERENCE'}/>
                {skip &&
                <HeaderSkip onPress={skipPress}/>}
                {back &&
                <BackArrow onPress={() => navigation.pop()}/>}
            </HeaderContainer>
            <View style={{padding: dimen.app_padding, marginTop: dimen.app_padding}}>
                <AppText style={[{fontSize: 22, color: Colors.button_text_color}]}
                         heading={true}>
                    What are the categories that
                </AppText>
                <AppText style={{fontSize: 22, color: Colors.button_text_color, marginTop: 1}}
                         heading={true}>
                    interest you?
                </AppText>

                <AppText style={{fontSize: 12, color: Colors.gray, marginTop: 5}}>
                    You can always change them from your profile
                </AppText>
            </View>

            <View style={{flex: 1}}>
                <Loader2 loading={isLoading}/>
                <FlatList
                    style={{flex: 1}}
                    data={categories}
                    keyExtractor={(item, index) => item.id.toString()}
                    numColumns={1}
                    bounces={false}
                    extraData={updatePreference}
                    renderItem={({item, index}) => (
                        <PreferenceCard
                            data={item}
                            addPreference={addPreference}
                            removePreference={removePreference}
                            status={preferences.includes(item.id)}
                        />
                    )}

                />
                <MyTextButton
                    onPress={donePress}
                    buttonText={'DONE'}
                    buttonContainerStyle={{margin: dimen.app_padding, padding: 10}}
                />
            </View>


        </MyBackgroundImage>
    );

};


const styles = StyleSheet.create({

    dashboardContainer: {
        backgroundColor: Colors.dashboard_bg_color,

    },
    dashboardHeadingContainer: {
        paddingLeft: dimen.app_padding * 2,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: dimen.app_padding * 2,
        marginBottom: dimen.app_padding * 2,


    },
    nameText: {
        fontSize: 16,
        backgroundColor: Colors.primary_color,
        color: Colors.button_text_color,
        fontWeight: 'bold',
        paddingRight: dimen.app_padding * 2,
        paddingLeft: dimen.app_padding * 2,
        padding: dimen.small_padding,
        borderTopLeftRadius: 25,
        borderBottomLeftRadius: 25,
    },

});
