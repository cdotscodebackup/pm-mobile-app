import React, {useState, useEffect} from 'react';
import {View, StyleSheet, TouchableWithoutFeedback, TouchableNativeFeedback, StatusBar, ScrollView} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';
import Colors from '../Styles/Colors';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen from '../Styles/Dimen';

import {WebView} from 'react-native-webview';
import {BackArrow} from '../Components/BackArrow';
import {HeaderContainer, HeaderTitle} from '../Components/Common';
import {Loader} from '../Components/Loader';
import {checkNetwork, CheckNetwork, NoInternetView} from '../Components/NoInternetView';

export const MyWebView = ({route, navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [isInternet, setIsInternet] = useState(true);
    const [data, setData] = useState(null);
    const [title, setTitle] = useState('');


    useEffect(() => {
        let {link, title} = route.params;
        setData(link);
        setTitle(title ? title : '');
        checkNetwork(setIsInternet)
    }, []);

    return (
        <MyBackgroundImage isBottom={false}>
            <HeaderContainer>
                <HeaderTitle title={title}/>
                <BackArrow onPress={() => navigation.pop()}/>
            </HeaderContainer>
            {isInternet &&
            <View style={{flex: 1, backgroundColor: Colors.app_background_color}}>
                {data !== null &&
                <WebView
                    style={{backgroundColor: Colors.app_background_color}}
                    source={{uri: data}}
                    // source={{ uri: 'https://www.google.com' }}
                    // startInLoadingState={true}
                    onLoadStart={() => setIsLoading(true)}
                    onLoadEnd={() => setIsLoading(false)}
                />
                }

                <Loader loading={isLoading}/>
            </View>
            }

            <NoInternetView status={!isInternet}/>
        </MyBackgroundImage>
    );

};




const styles = StyleSheet.create({

});
