import React, {useEffect, useRef, useState} from 'react';
import {
    View, StatusBar, StyleSheet, ScrollView, TouchableWithoutFeedback,
} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import Colors from '../Styles/Colors';
import dimen, {deviceHeight} from '../Styles/Dimen';
import {
    actionAlert,
    console_log,
    errorAlert,
    getDataWithoutToken,
    getObject,
    getTwoDigit,
    isNetConnected, saveObject,
} from '../Classes/auth';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {Sae} from 'react-native-textinput-effects';
import DateTimePicker from '@react-native-community/datetimepicker';
import {EditSave, HeaderContainer, HeaderTitle} from '../Components/Common';
import {BackArrow} from '../Components/BackArrow';
import {DesiRadioButtons} from './SignUp';
import {Keys} from '../Classes/Keys';
import {MyUrls} from '../Classes/MyUrls';
import {MyImage} from "../Components/MyImageButton";
import {CountriesList} from "../Components/CountriesList";
import RBSheet from "react-native-raw-bottom-sheet";

FontAwesomeIcon.loadFont().then().catch();

const radioData = [
    {id: 0, color: '#fff', label: 'Mr.'},
    {id: 1, color: '#fff', label: 'Mrs.'},
    {id: 2, color: '#fff', label: 'Miss.'},
];

export const PersonalInformation = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [isEdit, setIsEdit] = useState(false);
    const [enableLogin, setEnableLogin] = useState(false);

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [dob, setDob] = useState('');
    const [mobile, setMobile] = useState('');
    const [nationality, setNationality] = useState('');
    const [countryOfResidence, setCountryOfResidence] = useState('');

    const [password, setPassword] = useState('');
    const [userData, setUserData] = useState(null);

    const [showPassword, setShowPassword] = useState(false);
    const [termsCheckBox, setTermsCheckBox] = useState(false);


    const [selectedRadio, setSelectedRadio] = useState(0);

    const [cpType, setCpType] = useState('');

    const refRBSheet = useRef();


    const [dateString, setDateString] = useState('');
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);


    const showCountryPicker = (type) => {
        setCpType(type);
        refRBSheet.current.open();
    };
    const onCountrySelect = (item) => {
        if (cpType === 'nationality') {
            setNationality(item.name);
        } else {
            setCountryOfResidence(item.name);
        }
        refRBSheet.current.close();


    };


    useEffect(() => {
        getObject(Keys.user_data_key)
            .then(res => {
                if (res.status) {
                    console_log(res.data);
                    setUserData(res.data);
                    setFirstName(res.data.user_meta.first_name[0]);
                    setLastName(res.data.user_meta.last_name[0]);
                    setDateString(res.data.user_meta.date_of_birth[0]);
                    setMobile(res.data.user_meta.phone_number[0]);
                    setNationality(res.data.user_meta.nationality[0]);
                    setCountryOfResidence(res.data.user_meta.country_of_residence[0]);
                    setEmail(res.data.user.data.user_email);
                    setSelectedRadio(radioData.find(r => r.label === res.data.user_meta.designation[0]).id);

                } else {
                    console_log('no data found');
                }
            })
            .catch(err => console_log(false));
    }, []);

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
        setDateString(getTwoDigit(currentDate.getDate()) + '/' + getTwoDigit(currentDate.getMonth() + 1) + '/' + currentDate.getFullYear());
        // setDateString(getMonthName(currentDate.getMonth() + 1) + ' ' + currentDate.getDate() + ', ' + currentDate.getFullYear());

    };
    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };
    const showDatePicker = () => {
        if (isEdit) {
            showMode('date');
        }
    };
    const cardColorSelected = (obj) => {
        if (isEdit) {
            setSelectedRadio(obj.id);
        }
    };

    const editPress = (edit) => {
        if (edit) {
            updatedProfile();
        }
        setIsEdit(!isEdit);
    };

    const updatedProfile = () => {
        if (firstName === '') {
            errorAlert('First name is required');
            return;
        }
        if (lastName === '') {
            errorAlert('Last name is required');
            return;
        }
        if (email === '') {
            errorAlert('Email address is required');
            return;
        }
        if (dateString === '') {
            errorAlert('Please enter your date of birth');
            return;
        }
        if (mobile === '') {
            errorAlert('Please enter your mobile phone number');
            return;
        }

        let rdb = radioData.find(r => r.id === selectedRadio);


        let formData = {
            user_id: userData.user.ID,
            designation: rdb !== undefined ? rdb.label : 'Mr.',
            // username: firstName + '_' + lastName + Math.random() * 100000,
            first_name: firstName,
            last_name: lastName,
            // email: email,
            date_of_birth: dateString,
            phone_number: mobile,
            nationality: nationality,
            country_of_residence: countryOfResidence,
        };


        isNetConnected().then(res => {
            getDataWithoutToken(MyUrls.profile_update_url, 'POST', JSON.stringify(formData), setIsLoading)
                .then(res => {
                    // console_log(res);
                    if (res && res.status === true) {
                        saveObject(Keys.user_data_key, res)
                            .then(res => {
                                // navigation.pop();
                                // console_log('saved')
                            })
                            .catch(err => console.log(err));
                        actionAlert('Profile updated successfully.',
                            () => {
                                navigation.pop();
                            });

                    } else {
                        errorAlert(res.message);
                    }
                })
                .catch(err => console_log(err));
        });


    };


    return (
        <MyBackgroundImage isBottom={false}>

            <HeaderContainer>
                <HeaderTitle title={'PERSONAL INFORMATION'}/>
                <BackArrow onPress={() => navigation.pop()}/>
                <EditSave isEdit={isEdit} onPress={editPress}/>
            </HeaderContainer>

            <ScrollView style={{flex: 1}}>

                <View style={{flex: 1}}>

                    <DesiRadioButtons radioData={radioData}
                                      selectedRadio={selectedRadio}
                                      cardColorSelected={cardColorSelected}
                    />

                    <Sae
                        label={'First Name'}
                        iconClass={FontAwesomeIcon}
                        iconName={'pencil'}
                        // activeColor={'#da7071'}
                        // passiveColor={'#dadada'}
                        inputPadding={16}
                        labelHeight={24}
                        iconSize={0}
                        editable={isEdit}
                        labelStyle={styles.labelStyle}
                        iconColor={Colors.app_background_color}
                        borderHeight={1}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        style={[styles.inputStyle, {marginTop: 0}]}
                        value={firstName}
                        onChangeText={setFirstName}
                    />

                    <Sae
                        label={'Last Name'}
                        iconClass={FontAwesomeIcon}
                        iconName={'pencil'}
                        inputPadding={16}
                        labelHeight={24}
                        iconSize={0}
                        editable={isEdit}
                        labelStyle={styles.labelStyle}
                        iconColor={Colors.app_background_color}
                        borderHeight={1}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        style={styles.inputStyle}
                        value={lastName}
                        onChangeText={setLastName}
                    />

                    <Sae
                        label={'Email Address'}
                        iconClass={FontAwesomeIcon}
                        iconName={'pencil'}
                        inputPadding={16}
                        labelHeight={24}
                        iconSize={0}
                        labelStyle={styles.labelStyle}
                        iconColor={Colors.app_background_color}
                        borderHeight={1}
                        editable={false}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        style={[styles.inputStyle]}
                        value={email}
                        onChangeText={setEmail}
                    />

                    <TouchableWithoutFeedback onPress={showDatePicker}>
                        <View style={{}}>
                            <Sae
                                label={'Date of Birth'}
                                value={dateString}
                                iconClass={FontAwesomeIcon}
                                iconName={'pencil'}
                                inputPadding={16}
                                labelHeight={24}
                                iconSize={0}
                                labelStyle={styles.labelStyle}
                                iconColor={Colors.app_background_color}
                                borderHeight={1}
                                editable={false}
                                autoCapitalize={'none'}
                                autoCorrect={false}
                                style={[styles.inputStyle, {}]}
                            />
                            {show && (
                                <DateTimePicker
                                    testID="dateTimePicker"
                                    value={date}
                                    mode={mode}
                                    is24Hour={true}
                                    display="default"
                                    maximumDate={new Date()}
                                    onChange={onChange}
                                />
                            )}
                            <View style={{
                                position: 'absolute',
                                opacity: 0, width: '100%',
                                backgroundColor: 'black',
                                top: 0, bottom: 0, left: 0, right: 0,
                            }}/>
                        </View>
                    </TouchableWithoutFeedback>

                    <Sae
                        label={'Mobile Number'}
                        iconClass={FontAwesomeIcon}
                        iconName={'pencil'}
                        inputPadding={16}
                        labelHeight={24}
                        iconSize={0}
                        labelStyle={styles.labelStyle}
                        iconColor={Colors.app_background_color}
                        borderHeight={1}
                        autoCapitalize={'none'}
                        editable={isEdit}
                        autoCorrect={false}
                        keyboardType={'phone-pad'}
                        style={styles.inputStyle}
                        value={mobile}
                        onChangeText={setMobile}
                    />





                    <TouchableWithoutFeedback onPress={() => showCountryPicker('nationality')}>
                        <View style={{}}>
                            <Sae
                                label={'Nationality'}
                                value={nationality}
                                iconClass={FontAwesomeIcon}
                                iconName={'pencil'}
                                inputPadding={16}
                                labelHeight={24}
                                iconSize={0}
                                labelStyle={styles.labelStyle}
                                iconColor={Colors.app_background_color}
                                borderHeight={1}
                                editable={false}
                                autoCapitalize={'none'}
                                autoCorrect={false}
                                style={[styles.inputStyle, {}]}
                            />
                            <View style={styles.countryDownContainer}>
                                <MyImage
                                    source={require('../Asset/down.png')}
                                    tintColor={Colors.button_text_color}
                                    imageContainerStyle={styles.countryDownIcon}
                                />
                            </View>
                            <View style={{
                                position: 'absolute',
                                opacity: 0, width: '100%',
                                backgroundColor: 'black',
                                top: 0, bottom: 0, left: 0, right: 0,
                            }}/>
                        </View>
                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback onPress={() => showCountryPicker('countryOfResidence')}>
                        <View style={{}}>
                            <Sae
                                label={'Country of Residence'}
                                value={countryOfResidence}
                                iconClass={FontAwesomeIcon}
                                iconName={'pencil'}
                                inputPadding={16}
                                labelHeight={24}
                                iconSize={0}
                                labelStyle={styles.labelStyle}
                                iconColor={Colors.app_background_color}
                                borderHeight={1}
                                editable={false}
                                autoCapitalize={'none'}
                                autoCorrect={false}
                                style={[styles.inputStyle, {}]}
                            />
                            <View style={styles.countryDownContainer}>
                                <MyImage
                                    source={require('../Asset/down.png')}
                                    tintColor={Colors.button_text_color}
                                    imageContainerStyle={styles.countryDownIcon}
                                />
                            </View>
                            <View style={{
                                position: 'absolute',
                                opacity: 0, width: '100%',
                                backgroundColor: 'black',
                                top: 0, bottom: 0, left: 0, right: 0,
                            }}/>
                        </View>
                    </TouchableWithoutFeedback>



                </View>
            </ScrollView>


            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={false}
                height={deviceHeight / 1.7}
                customStyles={{
                    wrapper: {
                        backgroundColor: 'transparent',
                    },
                    draggableIcon: {
                        backgroundColor: Colors.normal_text_color,
                    },
                    container:{
                        backgroundColor: Colors.bottom_up_bg_color,
                    }
                }}
            >
                <CountriesList onCountrySelect={onCountrySelect}/>
            </RBSheet>

        </MyBackgroundImage>
    );
};


const styles = StyleSheet.create({

    loginText: {
        color: Colors.button_text_color,
        fontSize: 26,
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: dimen.app_padding,
    },
    loginDescText: {
        color: Colors.button_text_color,
        fontSize: 18,
        alignSelf: 'center',
        textAlign: 'center',
    },
    termsText: {
        color: Colors.button_text_color,
        fontSize: 15,
        alignSelf: 'flex-start',
        // textAlign: 'center',
        marginTop: 0,
        marginLeft: 10,
    },
    inputContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        overflow: 'hidden',
        backgroundColor: Colors.app_background_color,
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
        paddingTop: dimen.app_padding,

    },
    buttonContainer: {
        // flex:1,
        borderRadius: 25,
        marginTop: dimen.app_padding,
        justifyContent: 'center',
        alignItems: 'center',
        padding: dimen.app_padding,
        backgroundColor: Colors.login_button_bg_color,

    },

    quickConnectText: {
        fontSize: 14,
        marginRight: 10,
        marginLeft: 10,
        color: Colors.button_text_color,
        fontWeight: 'bold',

    },
    line: {
        height: 1,
        width: 30,
        flexGrow: 1,
        alignSelf: 'center',
        backgroundColor: Colors.border_color,

    },
    quickConnectContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: dimen.app_padding,

    },
    inputStyle: {
        margin: dimen.app_padding,
        // marginBottom: 5,
        paddingBottom: 0,
        borderBottomColor: Colors.button_text_color,
        borderBottomWidth: 1,
        marginBottom: 5,

    },
    labelStyle: {
        color: Colors.button_text_color,
        fontWeight: 'normal',
        marginBottom: 4,
    },

    showPasswordIcon: {
        width: 20,
        height: 20,
    },
    showPasswordContainer: {
        position: 'absolute',
        // backgroundColor:'red',
        right: 20,
        bottom: 20,
    },
    termsCheckBoxStyle: {
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        borderColor: Colors.button_text_color,
    },


    countryDownIcon: {
        width: 15,
        height: 15,
    },
    countryDownContainer: {
        position: 'absolute',
        right: 20,
        bottom: 10,
    },


});
