import React, {useState, useEffect} from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';

import {BackArrow} from '../Components/BackArrow';
import ScrollableTabView, {ScrollableTabBar, DefaultTabBar} from 'react-native-scrollable-tab-view';
import {HeaderContainer, HeaderTitle} from '../Components/Common';


export const OpeningHours = ({route, navigation}) => {

    const [data, setData] = useState(null);
    useEffect(() => {
        let data = route.params;
        setData(data);
    }, []);

    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        {key: 'first', title: 'First'},
        {key: 'second', title: 'Second'},
        {key: 'third', title: 'Third'},
        {key: 'fourth', title: 'Fourth'},
    ]);

    // const renderScene = SceneMap({
    //     first: FirstRoute,
    //     second: SecondRoute,
    //     third: SecondRoute,
    //     fourth: SecondRoute,
    // });

    const OpeningHourTime = (props) => {

        if (props.active) {
            return (
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View style={{
                        paddingRight: 10,
                        borderRightWidth: 2,
                        borderColor: Colors.primary_color,
                        width: 50,
                    }}>
                        <AppText style={{fontSize: 16, textAlignVertical: 'center', color: Colors.primary_color}}>
                            {props.title}
                        </AppText>
                    </View>
                    <View style={{
                        paddingLeft: 10,
                        // borderRightWidth: 2,
                        // borderColor: Colors.brand_store_border_color,
                    }}>
                        <AppText style={{fontSize: 16, color: Colors.primary_color}}>
                            {props.value}
                        </AppText>
                    </View>
                </View>
            );
        } else {
            return (
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View style={{
                        paddingRight: 10,
                        borderRightWidth: 2,
                        borderColor: Colors.brand_store_border_color,
                        width: 50,
                    }}>
                        <AppText style={{fontSize: 16, textAlignVertical: 'center', color: Colors.normal_text_color}}>
                            {props.title}
                        </AppText>
                    </View>
                    <View style={{
                        paddingLeft: 10,
                        // borderRightWidth: 2,
                        // borderColor: Colors.brand_store_border_color,
                    }}>
                        <AppText style={{fontSize: 16, color: Colors.normal_text_color}}>
                            {props.value}
                        </AppText>
                    </View>
                </View>
            );
        }
    };

    return (
        <MyBackgroundImage isBottom={false}>
            <HeaderContainer>
                <HeaderTitle title={'OPENING HOURS'}/>
                <BackArrow onPress={() => navigation.pop()}/>
            </HeaderContainer>

            <View style={{flex: 1, marginTop: 5}}>


                <ScrollableTabView
                    style={{flex: 1}}
                    initialPage={0}
                    tabBarUnderlineStyle={{backgroundColor: Colors.primary_color, height: 2}}
                    tabBarActiveTextColor={Colors.primary_color}
                    tabBarInactiveTextColor={Colors.normal_text_color}
                    renderTabBar={() => <ScrollableTabBar
                        style={{borderBottomColor: Colors.brand_store_border_color}}/>}
                >
                    <View tabLabel={'RETAIL OUTLETS'} style={{flex: 1}}>
                        <View style={{flex: 1, padding: dimen.app_padding}}>
                            <OpeningHourTime title={'MON'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 1)}/>
                            <OpeningHourTime title={'TUE'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 2)}/>
                            <OpeningHourTime title={'WED'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 3)}/>
                            <OpeningHourTime title={'THU'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 4)}/>
                            <OpeningHourTime title={'FRI'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 5)}/>
                            <OpeningHourTime title={'SAT'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 6)}/>
                            <OpeningHourTime title={'SUN'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 7)}/>
                        </View>
                    </View>
                    <View tabLabel={'FOOD & BEVERAGE OUTLETS'} style={{flex: 1}}>
                        <View style={{flex: 1, padding: dimen.app_padding}}>
                            <OpeningHourTime title={'MON'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 1)}/>
                            <OpeningHourTime title={'TUE'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 2)}/>
                            <OpeningHourTime title={'WED'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 3)}/>
                            <OpeningHourTime title={'THU'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 4)}/>
                            <OpeningHourTime title={'FRI'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 5)}/>
                            <OpeningHourTime title={'SAT'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 6)}/>
                            <OpeningHourTime title={'SUN'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 7)}/>
                        </View>
                    </View>
                    <View tabLabel={'CARREFOUR AND PHARMACIES'} style={{flex: 1}}>
                        <View style={{flex: 1, padding: dimen.app_padding}}>
                            <OpeningHourTime title={'MON'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 1)}/>
                            <OpeningHourTime title={'TUE'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 2)}/>
                            <OpeningHourTime title={'WED'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 3)}/>
                            <OpeningHourTime title={'THU'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 4)}/>
                            <OpeningHourTime title={'FRI'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 5)}/>
                            <OpeningHourTime title={'SAT'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 6)}/>
                            <OpeningHourTime title={'SUN'} value={'10:00 AM - 08:00 PM'}
                                             active={(new Date().getDay() === 7)}/>
                        </View>
                    </View>
                    <View tabLabel={'BANKS'} style={{flex: 1}}>
                        <View style={{flex: 1, padding: dimen.app_padding}}>
                            <AppText style={{fontSize: 16, textAlignVertical: 'center', color: Colors.normal_text_color}}>
                                According to the Instruction by State Bank of Pakistan
                            </AppText>
                        </View>
                    </View>

                </ScrollableTabView>


            </View>

        </MyBackgroundImage>
    );

};


const styles = StyleSheet.create({

    dashboardContainer: {
        backgroundColor: Colors.dashboard_bg_color,

    },
    dashboardHeadingContainer: {
        paddingLeft: dimen.app_padding * 2,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: dimen.app_padding * 2,
        marginBottom: dimen.app_padding * 2,


    },
    nameText: {
        fontSize: 16,
        backgroundColor: Colors.primary_color,
        color: Colors.button_text_color,
        fontWeight: 'bold',
        paddingRight: dimen.app_padding * 2,
        paddingLeft: dimen.app_padding * 2,
        padding: dimen.small_padding,
        borderTopLeftRadius: 25,
        borderBottomLeftRadius: 25,
    },
    scene: {
        flex: 1,
    },

});
