import React, {useRef, useState} from 'react';
import {View, StyleSheet, ScrollView, TouchableWithoutFeedback} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import Colors from '../Styles/Colors';
import {MyImage} from '../Components/MyImageButton';
import dimen, {deviceHeight} from '../Styles/Dimen';
import {
    actionAlert,
    console_log,
    errorAlert,
    getDataWithoutToken,
    getTwoDigit,
    isNetConnected,
} from '../Classes/auth';
import MyTextButton from '../Components/MyTextButton';
import {Sae} from 'react-native-textinput-effects';
import {CheckBox} from '../Components/CheckBox';
import DateTimePicker from '@react-native-community/datetimepicker';
import RadioForm, {RadioButton, RadioButtonInput} from 'react-native-simple-radio-button';
import {MyUrls} from '../Classes/MyUrls';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {CallComponent} from '../Components/CallComponent';
import RBSheet from 'react-native-raw-bottom-sheet';
import {CountriesList} from '../Components/CountriesList';
import {MyInputField} from '../Components/MyInputField';

FontAwesomeIcon.loadFont().then().catch();


const radioData = [
    {id: 0, color: '#fff', label: 'Mr.'},
    {id: 1, color: '#fff', label: 'Mrs.'},
    {id: 2, color: '#fff', label: 'Miss.'},
];

export const SignUp = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [enableLogin, setEnableLogin] = useState(false);

    const [firstName, setFirstName] = useState('');
    const [firstNameFocus, setFirstNameFocus] = useState(false);
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [dateString, setDateString] = useState('');
    const [mobile, setMobile] = useState('');
    const [nationality, setNationality] = useState('');
    const [countryOfResidence, setCountryOfResidence] = useState('');
    const [password, setPassword] = useState('');

    const [showPassword, setShowPassword] = useState(false);
    const [termsCheckBox, setTermsCheckBox] = useState(false);
    const [selectedRadio, setSelectedRadio] = useState(0);


    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const [cpType, setCpType] = useState('');

    const refRBSheet = useRef();


    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
        // setDateString(getTwoDigit(currentDate.getDate()) + '/' + getTwoDigit(currentDate.getMonth() + 1) + '/' + currentDate.getFullYear());
        setTextWrapper(getTwoDigit(currentDate.getDate()) + '/' + getTwoDigit(currentDate.getMonth() + 1) + '/' + currentDate.getFullYear(), 'dob');

        // setDateString(getMonthName(currentDate.getMonth() + 1) + ' ' + currentDate.getDate() + ', ' + currentDate.getFullYear());

    };
    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };
    const showDatePicker = () => {
        showMode('date');
    };

    const setTextWrapper = (text, type) => {
        if (type === 'first_name') {
            setFirstName(text);
            checkEnable(text, lastName, email, dateString, mobile, password, termsCheckBox);
        } else if (type === 'last_name') {
            setLastName(text);
            checkEnable(firstName, text, email, dateString, mobile, password, termsCheckBox);
        } else if (type === 'email') {
            setEmail(text);
            checkEnable(firstName, lastName, text, dateString, mobile, password, termsCheckBox);
        } else if (type === 'dob') {
            setDateString(text);
            checkEnable(firstName, lastName, email, text, mobile, password, termsCheckBox);
        } else if (type === 'mobile') {
            setMobile(text);
            checkEnable(firstName, lastName, email, dateString, text, password, termsCheckBox);
        } else if (type === 'password') {
            setPassword(text);
            checkEnable(firstName, lastName, email, dateString, mobile, text, termsCheckBox);
        } else if (type === 'term') {
            setTermsCheckBox(text);
            checkEnable(firstName, lastName, email, dateString, mobile, text, text);
        }
    };
    const checkEnable = (firstName, lastName, email, dateString, mobile, password, terms) => {
        if (firstName !== '' && lastName !== '' && email !== '' &&
            dateString !== '' && mobile !== '' && password !== '' && terms
        ) {
            setEnableLogin(true);
        } else {
            setEnableLogin(false);
        }
    };


    const registerPress = () => {
        if (firstName === '') {
            errorAlert('First name is required');
            return;
        }
        if (lastName === '') {
            errorAlert('Last name is required');
            return;
        }
        if (email === '') {
            errorAlert('Email address is required');
            return;
        }
        if (dateString === '') {
            errorAlert('Please enter your date of birth');
            return;
        }
        if (mobile === '') {
            errorAlert('Please enter your mobile phone number');
            return;
        }
        if (password === '') {
            errorAlert('Please enter your password');
            return;
        }
        if (termsCheckBox === false) {
            errorAlert('You need to agree to terms and conditions');
            return;
        }

        let formData = {
            designation: selectedRadio.label,
            username: firstName + '_' + lastName + Math.random() * 100000,
            first_name: firstName,
            last_name: lastName,
            email: email,
            date_of_birth: dateString,
            phone_number: mobile,
            nationality: nationality,
            country_of_residence: countryOfResidence,
            password: password,
        };


        isNetConnected().then(res => {
            getDataWithoutToken(MyUrls.register_url, 'POST', JSON.stringify(formData), setIsLoading)
                .then(res => {
                    console_log(res);
                    if (res && res.status === true) {
                        actionAlert('Registration completed successfully',
                            () => {
                                navigation.pop();
                            });

                    } else {
                        errorAlert(res.message);
                    }
                })
                .catch(err => console_log(err));
        });


    };
    const cardColorSelected = (obj) => {
        setSelectedRadio(obj.id);
        // console_log(obj);
    };

    const showCountryPicker = (type) => {
        setCpType(type);
        refRBSheet.current.open();
    };
    const onCountrySelect = (item) => {
        if (cpType === 'nationality') {
            setNationality(item.name);
        } else {
            setCountryOfResidence(item.name);
        }
        refRBSheet.current.close();


    };

    return (
        <MyBackgroundImage isBottom={false}>
            <ScrollView style={{flex: 1}}>
                <TouchableWithoutFeedback onPress={() => navigation.pop()}>
                    <View style={{alignSelf: 'flex-start', padding: dimen.app_padding}}>
                        <MyImage source={require('../Asset/back_arrow.png')}
                                 tintColor={Colors.button_text_color}
                                 imageContainerStyle={{width: 20, height: 20}}
                        />
                    </View>
                </TouchableWithoutFeedback>
                <View style={{flex: 1}}>
                    <View style={{flex: 1, alignItems: 'center'}}>
                        <AppText style={styles.loginText} heading={true}>
                            Create an account
                        </AppText>
                    </View>
                    <DesiRadioButtons radioData={radioData}
                                      selectedRadio={selectedRadio}
                                      cardColorSelected={cardColorSelected}
                    />

                    {/*<MyInputField*/}
                    {/*    label={'First Name'}*/}
                    {/*    value={firstName}*/}
                    {/*    setText={setTextWrapper}*/}

                    {/*/>*/}

                    <Sae
                        label={'First Name'}
                        iconClass={FontAwesomeIcon}
                        iconName={'pencil'}
                        inputPadding={16}
                        labelHeight={24}
                        iconSize={0}
                        onFocus={() => setFirstNameFocus(true)}
                        onBlur={() => setFirstNameFocus(false)}
                        labelStyle={!firstNameFocus ? styles.labelStyle : styles.labelFocusedStyle}
                        iconColor={Colors.app_background_color}
                        borderHeight={1}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        style={[styles.inputStyle, {marginTop: 0}]}
                        value={firstName}
                        onChangeText={(text) => setTextWrapper(text, 'first_name')}
                    />

                    <Sae
                        label={'Last Name'}
                        iconClass={FontAwesomeIcon}
                        iconName={'pencil'}
                        inputPadding={16}
                        labelHeight={24}
                        iconSize={0}
                        labelStyle={styles.labelStyle}
                        iconColor={Colors.app_background_color}
                        borderHeight={1}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        style={styles.inputStyle}
                        value={lastName}
                        onChangeText={(text) => setTextWrapper(text, 'last_name')}
                    />

                    <Sae
                        label={'Email Address'}
                        iconClass={FontAwesomeIcon}
                        iconName={'pencil'}
                        inputPadding={16}
                        labelHeight={24}
                        iconSize={0}
                        labelStyle={styles.labelStyle}
                        iconColor={Colors.app_background_color}
                        borderHeight={1}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        style={styles.inputStyle}
                        value={email}
                        onChangeText={(text) => setTextWrapper(text, 'email')}
                    />

                    <TouchableWithoutFeedback onPress={showDatePicker}>
                        <View style={{}}>
                            <Sae
                                label={'Date of Birth'}
                                value={dateString}
                                iconClass={FontAwesomeIcon}
                                iconName={'pencil'}
                                inputPadding={16}
                                labelHeight={24}
                                iconSize={0}
                                labelStyle={styles.labelStyle}
                                iconColor={Colors.app_background_color}
                                borderHeight={1}
                                editable={false}
                                autoCapitalize={'none'}
                                autoCorrect={false}
                                style={[styles.inputStyle, {}]}
                            />
                            {show && (
                                <DateTimePicker
                                    testID="dateTimePicker"
                                    value={date}
                                    mode={mode}
                                    is24Hour={true}
                                    display="default"
                                    maximumDate={new Date()}
                                    onChange={onChange}
                                />
                            )}
                            <View style={{
                                position: 'absolute',
                                opacity: 0, width: '100%',
                                backgroundColor: 'black',
                                top: 0, bottom: 0, left: 0, right: 0,
                            }}/>
                        </View>
                    </TouchableWithoutFeedback>


                    <Sae
                        label={'Mobile Number'}
                        iconClass={FontAwesomeIcon}
                        iconName={'pencil'}
                        inputPadding={16}
                        labelHeight={24}
                        iconSize={0}
                        labelStyle={styles.labelStyle}
                        iconColor={Colors.app_background_color}
                        borderHeight={1}
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        keyboardType={'phone-pad'}
                        style={styles.inputStyle}
                        value={mobile}
                        onChangeText={(text) => setTextWrapper(text, 'mobile')}
                    />


                    <TouchableWithoutFeedback onPress={() => showCountryPicker('nationality')}>
                        <View style={{}}>
                            <Sae
                                label={'Nationality'}
                                value={nationality}
                                iconClass={FontAwesomeIcon}
                                iconName={'pencil'}
                                inputPadding={16}
                                labelHeight={24}
                                iconSize={0}
                                labelStyle={styles.labelStyle}
                                iconColor={Colors.app_background_color}
                                borderHeight={1}
                                editable={false}
                                autoCapitalize={'none'}
                                autoCorrect={false}
                                style={[styles.inputStyle, {}]}
                            />
                            <View style={styles.countryDownContainer}>
                                <MyImage
                                    source={require('../Asset/down.png')}
                                    tintColor={Colors.button_text_color}
                                    imageContainerStyle={styles.countryDownIcon}
                                />
                            </View>
                            <View style={{
                                position: 'absolute',
                                opacity: 0, width: '100%',
                                backgroundColor: 'black',
                                top: 0, bottom: 0, left: 0, right: 0,
                            }}/>
                        </View>
                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback onPress={() => showCountryPicker('countryOfResidence')}>
                        <View style={{}}>
                            <Sae
                                label={'Country of Residence'}
                                value={countryOfResidence}
                                iconClass={FontAwesomeIcon}
                                iconName={'pencil'}
                                inputPadding={16}
                                labelHeight={24}
                                iconSize={0}
                                labelStyle={styles.labelStyle}
                                iconColor={Colors.app_background_color}
                                borderHeight={1}
                                editable={false}
                                autoCapitalize={'none'}
                                autoCorrect={false}
                                style={[styles.inputStyle, {}]}
                            />
                            <View style={styles.countryDownContainer}>
                                <MyImage
                                    source={require('../Asset/down.png')}
                                    tintColor={Colors.button_text_color}
                                    imageContainerStyle={styles.countryDownIcon}
                                />
                            </View>
                            <View style={{
                                position: 'absolute',
                                opacity: 0, width: '100%',
                                backgroundColor: 'black',
                                top: 0, bottom: 0, left: 0, right: 0,
                            }}/>
                        </View>
                    </TouchableWithoutFeedback>


                    <View style={{}}>
                        <Sae
                            label={'Password'}
                            iconClass={FontAwesomeIcon}
                            iconName={'pencil'}
                            inputPadding={16}
                            labelHeight={24}
                            iconSize={0}
                            labelStyle={styles.labelStyle}
                            iconColor={Colors.app_background_color}
                            borderHeight={1}
                            secureTextEntry={!showPassword}
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            style={styles.inputStyle}
                            value={password}
                            onChangeText={(text) => setTextWrapper(text, 'password')}
                        />
                        {password.length > 0 &&
                        <TouchableWithoutFeedback
                            onPress={() => setShowPassword(!showPassword)}>
                            <View style={styles.showPasswordContainer}>
                                <MyImage
                                    source={showPassword ? require('../Asset/eye_not_active.png') :
                                        require('../Asset/eye_active.png')
                                    }
                                    imageContainerStyle={styles.showPasswordIcon}
                                />
                            </View>
                        </TouchableWithoutFeedback>
                        }
                    </View>


                    <View style={styles.inputContainer}>
                        <View style={{
                            flex: 1, flexDirection: 'row',
                            justifyContent: 'flex-start', alignItems: 'flex-start',
                        }}>
                            <CheckBox status={termsCheckBox}
                                      setStatus={() => setTextWrapper(!termsCheckBox, 'term')}
                                      style={[styles.termsCheckBoxStyle, {marginTop: 5}]}/>

                            <AppText style={styles.termsText}>
                                I have read and agree to the terms and conditions and privacy policy
                            </AppText>
                        </View>


                        <View style={{height: 40}}/>

                        <View>
                            <MyTextButton
                                onPress={registerPress}
                                buttonText={'REGISTER'}
                                buttonTextStyle={{fontWeight: 'bold'}}
                                buttonContainerStyle={{}}
                            />

                            {!enableLogin &&
                            <View style={{
                                position: 'absolute',
                                opacity: 0.6,
                                width: '100%',
                                backgroundColor: 'black', top: 0, bottom: 0, left: 0, right: 0,
                            }}/>}
                        </View>
                        <View style={{height: 15}}/>

                    </View>
                </View>
            </ScrollView>


            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={false}
                height={deviceHeight / 1.7}
                customStyles={{
                    wrapper: {
                        backgroundColor: 'transparent',
                    },
                    draggableIcon: {
                        backgroundColor: Colors.normal_text_color,
                    },
                    container:{
                        backgroundColor: Colors.bottom_up_bg_color,
                    }
                }}
            >
                <CountriesList onCountrySelect={onCountrySelect}/>
            </RBSheet>
        </MyBackgroundImage>
    );
};


export const DesiRadioButtons = (props) => {


    return (
        <View style={{
            padding: 20,
            paddingBottom: 0,
            paddingTop: 10,
        }}>
            <RadioForm formHorizontal={true}
                       animation={true}
                       style={{justifyContent: 'space-evenly'}}>
                {
                    props.radioData.map((obj, i) => (
                        <RadioButton labelHorizontal={true} key={i}>
                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                // backgroundColor: 'red',
                            }}>
                                <RadioButtonInput
                                    obj={obj}
                                    index={i}
                                    animattion={true}
                                    isSelected={props.selectedRadio === i}
                                    onPress={() => props.cardColorSelected(obj)}
                                    borderWidth={2}
                                    buttonInnerColor={Colors.primary_color}
                                    buttonOuterColor={Colors.primary_color}
                                    buttonSize={11}
                                    buttonOuterSize={20}
                                    buttonStyle={{}}
                                    buttonWrapStyle={{marginRight: 5}}
                                />
                                <AppText style={{
                                    color: Colors.button_text_color,
                                    fontSize: 17, marginLeft: 0, marginTop: 5,
                                    // textAlignVertical:'bottom'
                                }}>
                                    {obj.label}
                                </AppText>
                            </View>

                        </RadioButton>
                    ))
                }
            </RadioForm>
        </View>
    );
};

const styles = StyleSheet.create({

    loginText: {
        color: Colors.button_text_color,
        fontSize: 26,
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: dimen.app_padding,
    },
    loginDescText: {
        color: Colors.button_text_color,
        fontSize: 18,
        alignSelf: 'center',
        textAlign: 'center',
    },
    termsText: {
        color: Colors.button_text_color,
        fontSize: 15,
        alignSelf: 'flex-start',
        // textAlign: 'center',
        marginTop: 0,
        marginLeft: 10,
    },
    inputContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        overflow: 'hidden',
        backgroundColor: Colors.app_background_color,
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
        paddingTop: dimen.app_padding,

    },
    buttonContainer: {
        // flex:1,
        borderRadius: 25,
        marginTop: dimen.app_padding,
        justifyContent: 'center',
        alignItems: 'center',
        padding: dimen.app_padding,
        backgroundColor: Colors.login_button_bg_color,

    },

    quickConnectText: {
        fontSize: 14,
        marginRight: 10,
        marginLeft: 10,
        color: Colors.button_text_color,
        fontWeight: 'bold',

    },
    line: {
        height: 1,
        width: 30,
        flexGrow: 1,
        alignSelf: 'center',
        backgroundColor: Colors.border_color,

    },
    quickConnectContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: dimen.app_padding,

    },
    inputStyle: {
        margin: dimen.app_padding,
        // marginBottom: 5,
        paddingBottom: 0,
        borderBottomColor: Colors.button_text_color,
        borderBottomWidth: 1,
        marginBottom: 5,

    },
    labelStyle: {
        color: Colors.button_text_color,
        fontWeight: 'normal',
        marginBottom: 4,
    },
    labelFocusedStyle: {
        color: Colors.primary_color,
        fontWeight: 'normal',
        marginBottom: 4,
    },

    showPasswordIcon: {
        width: 20,
        height: 20,
    },

    showPasswordContainer: {
        position: 'absolute',
        right: 20,
        bottom: 20,
    },
    countryDownIcon: {
        width: 15,
        height: 15,
    },
    countryDownContainer: {
        position: 'absolute',
        right: 20,
        bottom: 10,
    },
    termsCheckBoxStyle: {
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        borderColor: Colors.button_text_color,
    },

});
