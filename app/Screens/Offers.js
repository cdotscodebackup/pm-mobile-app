import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Linking} from 'react-native';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import {BackArrow} from '../Components/BackArrow';
import {console_log, errorAlert, getDataWithoutToken, isNetConnected} from '../Classes/auth';
import {MyUrls} from '../Classes/MyUrls';
import {Loader, Loader2} from '../Components/Loader';
import {OfferCard} from '../Cards/OffersCard';
import {HeaderContainer, HeaderTitle} from '../Components/Common';
import ScrollableTabView, {ScrollableTabBar} from 'react-native-scrollable-tab-view';
import Colors from '../Styles/Colors';


export const Offers = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [shops, setShops] = useState([]);
    const [offers, setOffers] = useState([]);
    const [dineOffers, setDineOffers] = useState([]);
    const [entertainmentOffers, setEntertainmentOffers] = useState([]);
    const [shopsOffers, setShopsOffers] = useState([]);

    useEffect(() => {
        getShops();
    }, []);

    const getShops = (url) => {
        isNetConnected().then(res => {
            getDataWithoutToken(MyUrls.offers_url, 'GET', '', setIsLoading)
                .then(res => {
                    if (res && res.length > 0) {
                        setOffers(res);
                        // All=>87
                        // Dine=>89
                        // Entertainment=>90
                        // Shops =>88
                        setDineOffers(res.filter(off => off['offers-category'].includes(89)));
                        setEntertainmentOffers(res.filter(off => off['offers-category'].includes(90)));
                        setShopsOffers(res.filter(off => off['offers-category'].includes(88)));
                    } else {
                        setOffers([]);
                    }
                }).catch(err => setOffers([]));
        }).catch(err => setOffers([]));

    };


    const callStorePress = (item) => {
        Linking.canOpenURL('tel: ' + item.phone_no)
            .then(res => {
                Linking.openURL('tel: ' + item.phone_no)
                    .then()
                    .catch();
            })
            .catch(err => {
                errorAlert('No phone number available');
            });
    };


    return (
        <MyBackgroundImage isBottom={false}>
            <HeaderContainer>
                <HeaderTitle title={'OFFERS'}/>
                <BackArrow onPress={() => navigation.pop()}/>
            </HeaderContainer>
            <View style={{height:10}}/>

            <ScrollableTabView
                style={{flex: 1}}
                initialPage={0}
                tabBarUnderlineStyle={{backgroundColor: Colors.primary_color, height: 2}}
                tabBarActiveTextColor={Colors.primary_color}
                tabBarInactiveTextColor={Colors.similar_brands_text_color}
                tabBarTextStyle={{fontWeight:'normal',fontSize:13}}
                renderTabBar={() => <ScrollableTabBar
                    style={{borderBottomColor: Colors.brand_store_border_color}}/>}
            >
                <View tabLabel={'ALL'} style={{flex: 1}}>
                    <View style={{flex: 1,}}>
                        <FlatList
                            style={{flex: 1}}
                            data={offers}
                            keyExtractor={(item, index) => item.id.toString()}
                            numColumns={1}
                            bounces={false}
                            renderItem={({item, index}) => (
                                <OfferCard
                                    data={item}
                                    callStorePress={callStorePress}
                                />
                            )}
                            ListHeaderComponent={<View style={{height: 30}}/>}
                        />
                    </View>
                </View>

                <View tabLabel={'DINE'} style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <FlatList
                            style={{flex: 1}}
                            data={dineOffers}
                            keyExtractor={(item, index) => item.id.toString()}
                            numColumns={1}
                            bounces={false}
                            renderItem={({item, index}) => (
                                <OfferCard
                                    data={item}
                                    callStorePress={callStorePress}
                                />
                            )}
                            ListHeaderComponent={<View style={{height: 30}}/>}

                        />
                    </View>
                </View>

                <View tabLabel={'ENTERTAINMENT'} style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <FlatList
                            style={{flex: 1}}
                            data={entertainmentOffers}
                            keyExtractor={(item, index) => item.id.toString()}
                            numColumns={1}
                            bounces={false}
                            renderItem={({item, index}) => (
                                <OfferCard
                                    data={item}
                                    callStorePress={callStorePress}
                                />
                            )}
                            ListHeaderComponent={<View style={{height: 30}}/>}

                        />
                    </View>
                </View>

                <View tabLabel={'SHOPS'} style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <FlatList
                            style={{flex: 1}}
                            data={shopsOffers}
                            keyExtractor={(item, index) => item.id.toString()}
                            numColumns={1}
                            bounces={false}
                            renderItem={({item, index}) => (
                                <OfferCard
                                    data={item}
                                    callStorePress={callStorePress}
                                />
                            )}
                            ListHeaderComponent={<View style={{height: 30}}/>}

                        />
                    </View>
                </View>

            </ScrollableTabView>

            <Loader loading={isLoading}/>

        </MyBackgroundImage>
    );

};


const styles = StyleSheet.create({});
