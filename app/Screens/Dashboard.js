import React, {useState, useRef, useEffect} from 'react';
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    TouchableNativeFeedback,
    StatusBar,
    ScrollView, Text,
    TouchableOpacity,
} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import Colors from '../Styles/Colors';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen, {deviceHeight, deviceWidth} from '../Styles/Dimen';
import MyImageButton, {MyImage, MyImageBackground} from '../Components/MyImageButton';
import {CallDialog} from '../Components/SearchDropDown';

import RBSheet from 'react-native-raw-bottom-sheet';
import {CallComponent, ContactItem} from '../Components/CallComponent';
import {MyUrls} from '../Classes/MyUrls';
import {console_log, getDataWithoutToken, isNetConnected} from '../Classes/auth';
import {call} from 'react-native-reanimated';
import {checkUserSession} from './Services';

export const Dashboard = ({navigation}) => {

    const [showCall, setShowCall] = useState(false);
    const [searchText, setSearchText] = useState('');
    const [exploreItems, setExploreItems] = useState([
        {id: 1, text: 'LATEST', icon: require('../Asset/whatsnew.png')},
        {id: 2, text: 'EVENTS', icon: require('../Asset/event.png')},
        {id: 3, text: 'OFFERS', icon: require('../Asset/offers.png')},
        {id: 4, text: 'PLAN A VISIT', icon: require('../Asset/plan_visit.png')},
        {id: 5, text: 'FEEDBACK', icon: require('../Asset/feedback.png')},
    ]);

    const [isLoading, setIsLoading] = useState(false);
    const [events, setEvents] = useState([]);

    const [isLogin, setIsLogin] = useState(false);
    const [name, setName] = useState('');

    useEffect(() => {
        let sub = navigation.addListener('focus', () => {
            checkUserSession(setIsLogin, setName);
        });
        return sub;
    }, []);


    useEffect(() => {
        getEvents();
    }, []);

    const getEvents = () => {
        isNetConnected().then(res => {
            console_log(MyUrls.events_url)
            getDataWithoutToken(MyUrls.events_url, 'GET', '', setIsLoading)
                .then(res => {
                    if (res && res.length > 0) {
                        setEvents(res);
                    } else {
                        setEvents([]);
                    }
                });
        });
    };

    const refRBSheet = useRef();
    const callPress = () => {
        refRBSheet.current.open();
    };

    const profilePress = () => {
        console_log(isLogin);
        if (!isLogin) {
            navigation.navigate('Login');
        } else {
            navigation.navigate('Profile');
        }
    };
    const notificationPress = () => {
        navigation.navigate('Notifications');
    };
    const exploreItemPress = (item) => {
        if (item.id === 1) {
            navigation.navigate('MyWebView',
                {link: 'http://packagesmall.com/latest-app/', title: ''});
        }
        if (item.id === 2) {
            navigation.navigate('Events');
        }
        if (item.id === 3) {
            navigation.navigate('Offers');
        }
        if (item.id === 4) {
            navigation.navigate('GettingHere');
        }
        if (item.id === 5) {
            navigation.navigate('MyWebView',
                {link: 'http://packagesmall.com/customer-feedback/', title: 'FEEDBACK'});
        }
    };
    const searchPress = () => {
        navigation.navigate('Search');
    };


    const eventPress = (item) => {
        navigation.navigate('EventDetails', {eventDetail: item});
    };

    return (
        <MyBackgroundImage isBottom={true}>
            <TopBar
                callPress={callPress}
                notificationPress={notificationPress}
                profilePress={profilePress}
            />
            <SearchBar searchPress={searchPress}/>
            <ScrollView style={{flex: 1}}>

                {/*explore*/}
                <View style={{padding: 10}}>
                    <AppText style={{
                        fontSize: 24,
                        // fontFamily: 'crimson-roman',
                        color: Colors.normal_text_color,
                    }}
                    heading={true}
                    >
                        Explore
                    </AppText>

                    <FlatList
                        style={{marginTop: 5}}
                        data={exploreItems}
                        keyExtractor={(item, index) => item.id.toString()}
                        numColumns={1}
                        bounces={false}
                        horizontal={true}
                        renderItem={({item, index}) => (
                            <ExploreItem data={item} onPress={exploreItemPress}/>
                        )}
                    />

                </View>

                {/*what can we help you do*/}
                <View style={{marginTop: dimen.app_padding}}>
                    <AppText style={{
                        fontSize: 24,
                        // fontFamily: 'crimson-roman',
                        color: Colors.normal_text_color,
                        marginLeft: dimen.small_padding,
                    }}
                    heading={true}
                    >
                        What can we help
                    </AppText>
                    <AppText style={{
                        fontSize: 24,
                        // fontFamily: 'crimson-roman',
                        color: Colors.normal_text_color,
                        marginLeft: dimen.small_padding,
                    }}
                    heading={true}
                    >
                        you do?
                    </AppText>
                    <View style={{height: 10}}/>
                    <FlatList
                        style={{marginTop: 5}}
                        data={events}
                        keyExtractor={(item, index) => item.id.toString()}
                        numColumns={1}
                        bounces={false}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        renderItem={({item, index}) => (
                            <HelpItem
                                data={item}
                                isLast={(events.length - 1) !== index}
                                onPress={eventPress}
                            />
                        )}
                    />

                </View>


                {/*shop*/}
                <ShoppingItem text={'SHOP'}
                              icon={require('../Asset/shopping.png')}
                              onPress={() => navigation.navigate('Categories', {
                                  title: 'SHOP',
                                  link: MyUrls.shop_category_url,
                                  brands_link: MyUrls.shops_url,
                              })}/>
                <ShoppingItem text={'DINE'}
                              onPress={() => navigation.navigate('Categories', {
                                  title: 'DINE',
                                  link: MyUrls.cuisine_category_url,
                                  brands_link: MyUrls.cuisine_url,
                              })}
                              icon={require('../Asset/dining.png')}/>
                <ShoppingItem text={'ENTERTAINMENT'}
                              onPress={() =>
                                  navigation.navigate('Brands',
                                      {categoryData: null, brands_link: MyUrls.entertainment_url})
                                  // navigation.navigate('Categories', {
                                  //     title: 'ENTERTAINMENT',
                                  //     link: MyUrls.entertainment_category_url,
                                  //     brands_link: MyUrls.entertainment_url,
                                  // })
                              }
                              icon={require('../Asset/shopping.png')}/>
            </ScrollView>


            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={false}
                height={deviceHeight / 1.7}
                customStyles={{
                    wrapper: {
                        // backgroundColor: Colors.bottom_up_bg_color,
                        backgroundColor: 'transparent',
                    },
                    draggableIcon: {
                        backgroundColor: Colors.search_bg_color,
                    },
                    container:{
                        backgroundColor: Colors.bottom_up_bg_color,
                    }
                }}
            >
                <CallComponent/>
            </RBSheet>
        </MyBackgroundImage>
    );

};


const ShoppingItem = (props) => {
    const onPress = () => {
        props.onPress(props.text);
    };
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={{}}>
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 15,marginBottom: 7,}}>
                    <AppText style={{
                        fontSize: 13,
                        // marginTop: 10,
                        // marginBottom: 10,
                        color: Colors.normal_text_color,
                        marginLeft: 20,
                        fontWeight: 'bold',
                    }}>
                        {props.text}
                    </AppText>
                    <View style={{
                        width: 20, height: 2,
                        marginLeft: 7,
                        backgroundColor: Colors.normal_text_color,
                    }}/>
                </View>
                <MyImage
                    source={props.icon}
                    imageContainerStyle={{width: deviceWidth, height: deviceWidth / 2, resizeMode: 'cover'}}
                />
            </View>
        </TouchableWithoutFeedback>
    );
};
const SearchBar = (props) => {

    return (
        <TouchableWithoutFeedback onPress={props.searchPress}>
            <View style={styles.searchContainer}>
                <MyImage
                    source={require('../Asset/search.png')}
                    imageContainerStyle={{width: 16, height: 16}}
                />
                <AppText style={{color: Colors.light_text_login, fontSize: 12, marginLeft: dimen.small_padding}}>
                    Search for stores, restaurants and more...
                </AppText>
            </View>
        </TouchableWithoutFeedback>
    );
};
const TopBar = (props) => {

    return (
        <View style={styles.topBarContainer}>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>
                <TouchableWithoutFeedback onPress={props.callPress}>
                    <View>
                        <MyImage
                            source={require('../Asset/phone.png')}
                            imageContainerStyle={{width: 22, height: 22}}
                        />
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={props.notificationPress}>
                    <View>
                        <MyImage
                            source={require('../Asset/notifecation.png')}
                            imageContainerStyle={{width: 22, height: 22, marginLeft: 20}}
                        />
                    </View>
                </TouchableWithoutFeedback>
            </View>
            <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center'}}>
                <MyImage
                    source={require('../Asset/header_logo.png')}
                    imageContainerStyle={{width: 85, height: 60, justifyContent: 'flex-start'}}
                />
            </View>
            <TouchableNativeFeedback onPress={props.profilePress}>
                <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'flex-end'}}>
                    <MyImage
                        source={require('../Asset/profile.png')}
                        imageContainerStyle={{width: 26, height: 26}}
                    />
                </View>
            </TouchableNativeFeedback>

        </View>
    );
};
const ExploreItem = (props) => {
    const onPress = () => {
        props.onPress(props.data);
    };
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={{alignItems: 'center', marginRight: 20, marginTop: dimen.small_padding}}>
                <MyImage source={props.data.icon}
                         imageContainerStyle={{width: 55, height: 55}}
                />
                <AppText style={{color: Colors.normal_text_color, fontSize: 12, marginTop: dimen.small_padding}}>
                    {props.data.text}
                </AppText>
            </View>
        </TouchableWithoutFeedback>
    );
};
const HelpItem = (props) => {
    const onPress = () => {
        props.onPress(props.data);
    };
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={[helpItemStyle.itemContainer, props.isLast ?
                {marginRight: dimen.app_padding} : {}]}>
                <View style={{flex: 1}}>
                    <AppText style={{
                        color: Colors.normal_text_color,
                        fontSize: 12, fontWeight: 'bold', marginTop: 6,
                    }}>
                        EVENT
                    </AppText>
                    <AppText style={{
                        color: Colors.normal_text_color,
                        fontSize: 14,
                        fontWeight: 'bold',
                        marginTop: 6,
                        flex: 1,
                    }}
                             numberOfLines={1}
                    >
                        {/*CHAT WITH US*/}
                        {props.data.title.rendered.toUpperCase()}
                    </AppText>
                    <AppText style={{
                        color: Colors.normal_text_color,
                        fontSize: 11, marginTop: 6, fontWeight: 'bold',
                    }}>
                        {/*21 JUN - 31 DEC*/}
                        {props.data.event_date.toUpperCase()}
                    </AppText>
                </View>
                <MyImage source={require('../Asset/event_logo.png')}
                         imageContainerStyle={{
                             width: 55,
                             height: 55,
                             // position: 'absolute',
                             // right: 0,
                             // top: 0,
                             // bottom: 0,
                             // alignSelf:'center'
                         }}
                />
            </View>
        </TouchableWithoutFeedback>
    );
};


const styles = StyleSheet.create({

    dashboardContainer: {
        backgroundColor: Colors.dashboard_bg_color,
    },
    dashboardHeadingContainer: {
        paddingLeft: dimen.app_padding * 2,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: dimen.app_padding * 2,
        marginBottom: dimen.app_padding * 2,


    },
    nameText: {
        fontSize: 16,
        backgroundColor: Colors.primary_color,
        color: Colors.button_text_color,
        fontWeight: 'bold',
        paddingRight: dimen.app_padding * 2,
        paddingLeft: dimen.app_padding * 2,
        padding: dimen.small_padding,
        borderTopLeftRadius: 25,
        borderBottomLeftRadius: 25,
    },
    topBarContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        padding: 18,
    },
    searchContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 18,
        marginTop: 5,
        marginBottom: 5,
        borderBottomWidth: 1,
        borderBottomColor: Colors.normal_text_color,
        paddingBottom: dimen.small_padding,
    },

});

const helpItemStyle = StyleSheet.create({
    itemContainer: {
        width: deviceWidth - (deviceWidth / 8),
        alignItems: 'center',
        // marginRight: dimen.app_padding,
        // marginTop: dimen.small_padding,
        backgroundColor: Colors.primary_color,
        padding: dimen.app_padding + 5,
        paddingTop: dimen.small_padding,
        paddingBottom: dimen.app_padding,
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
});

