import React, {useState, useEffect, useRef} from 'react';
import {View, StyleSheet, TouchableWithoutFeedback, ScrollView} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import Colors from '../Styles/Colors';
import dimen, {deviceHeight} from '../Styles/Dimen';
import {MyImage} from '../Components/MyImageButton';
import RBSheet from 'react-native-raw-bottom-sheet';
import {CallComponent} from '../Components/CallComponent';
import {HeaderContainer, HeaderTitle} from '../Components/Common';


export const More = ({navigation}) => {
    const [searchText, setSearchText] = useState('');

    const refRBSheet = useRef();
    const callPress = () => {
        refRBSheet.current.open();
    };

    return (
        <MyBackgroundImage isBottom={false}>
            <HeaderContainer>
                <HeaderTitle title={'MORE'}/>
            </HeaderContainer>
            <ScrollView>

                <ServiceItem icon={require('../Asset/getting_here.png')}
                             text={'Plan A Visit'}
                             onPress={() => navigation.navigate('GettingHere')}/>
                <ServiceItem icon={require('../Asset/opening_hours.png')} text={'Opening Hours'}
                             onPress={() => navigation.navigate('OpeningHours')}/>
                <ServiceItem icon={require('../Asset/mall_contacts.png')} text={'Mall Contacts'}
                             onPress={callPress}/>

                <AppText style={{
                    fontSize: 15,
                    color: Colors.similar_brands_text_color,
                    margin: dimen.app_padding,
                    marginBottom: 5,
                    // backgroundColor:'red'
                }}>
                    LEGAL
                </AppText>

                <ServiceItem icon={require('../Asset/privacy_policy.png')}
                             text={'Privacy Policy'}
                             onPress={() => navigation.navigate('MyWebView',
                                 {
                                     link: 'http://packagesmall.com/privacy-policy-web-view/',
                                     title: 'PRIVACY POLICY',
                                 })}
                />
                <ServiceItem icon={require('../Asset/terms_conditions.png')}
                             text={'Terms & Conditions'}
                             onPress={() => navigation.navigate('MyWebView',
                                 {
                                     link: 'http://packagesmall.com/terms-and-conditions-web-view/',
                                     title: 'TERMS AND CONDITIONS',
                                 })}/>

                <ServiceItem icon={require('../Asset/antifraud.png')}
                             text={'Anti-Fraud Disclaimer'}
                             onPress={() => navigation.navigate('MyWebView',
                                 {
                                     link: 'http://packagesmall.com/anti-fraud-disclaimer/',
                                     title: 'ANTI-FRAUD DISCLAIMER',
                                 })}/>
            </ScrollView>

            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={false}
                height={deviceHeight / 1.7}
                customStyles={{
                    wrapper: {
                        backgroundColor: 'transparent',
                    },
                    draggableIcon: {
                        backgroundColor: Colors.search_bg_color,
                    },
                    container: {
                        backgroundColor: Colors.bottom_up_bg_color,
                    }
                }}
            >
                <CallComponent/>
            </RBSheet>

        </MyBackgroundImage>
    );

};


export const ServiceItem = (props) => {
    const onPress = () => {
        props.onPress();
    };
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    padding: 20,
                    paddingLeft: 20,
                }}>
                    <MyImage
                        source={props.icon}
                        imageContainerStyle={{width: 23, height: 23}}
                    />
                    <AppText
                        style={{
                            fontSize: 17, color: Colors.button_text_color,
                            marginLeft: 20,
                        }}>
                        {props.text}
                    </AppText>
                    <MyImage
                        source={require('../Asset/right_arrow.png')}
                        imageContainerStyle={{width: 13, height: 13, position: 'absolute', right: 20}}
                    />
                </View>
                <View style={{
                    height: 0.5,
                    backgroundColor: Colors.border_color,
                    marginRight: dimen.app_padding,
                    marginLeft: dimen.app_padding,
                }}/>
            </View>
        </TouchableWithoutFeedback>
    );
};


const styles = StyleSheet.create({});
