import React, {useState} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';
import {FlatList} from 'react-native-gesture-handler';
import dimen from '../Styles/Dimen';
import {NotificationCard} from '../Cards/NotificationCard';
import AppText from '../Components/AppText';
import Colors from '../Styles/Colors';
import {BackArrow} from '../Components/BackArrow';
import {EmptyList} from '../Components/Loader';
import {HeaderContainer, HeaderTitle} from '../Components/Common';
import {MyImage} from '../Components/MyImageButton';


export const Notifications = ({navigation}) => {
    const [notifications, setNotifications] = useState([]);

    return (
        <MyBackgroundImage isBottom={false}>

            <HeaderContainer>
                <BackArrow onPress={() => navigation.pop()}/>
                <HeaderTitle title={'NOTIFICATIONS'}/>
                {/*<HeaderTitle title={'FASHION WOMEN'}/>*/}
            </HeaderContainer>

            {notifications.length !== 0 &&
            <View style={{flex: 1, justifyContent: 'flex-start'}}>
                <FlatList
                    style={{flex: 1}}
                    data={notifications}
                    keyExtractor={(item, index) => item.id.toString()}
                    numColumns={1}
                    bounces={false}
                    renderItem={({item, index}) => (
                        <NotificationCard data={item}/>
                    )}
                />
            </View>
            }

            {notifications.length === 0 &&
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                margin: dimen.app_padding,
            }}>
                <MyImage source={require('../Asset/notifecation.png')}
                         imageContainerStyle={{width: 50, height: 50}}/>
                <AppText
                    heading={true}
                    style={{
                        fontSize: 18,
                        // fontFamily: 'crimson-roman',
                        marginTop: 22,
                        color: Colors.normal_text_color,
                    }}>
                    No Notifications
                </AppText>

                <AppText style={{
                    fontSize: 12, color: Colors.normal_text_color,
                    marginTop: 22,
                }}>
                    You have no new Notification
                </AppText>
            </View>
            }


        </MyBackgroundImage>
    );

};

const styles = StyleSheet.create({});
