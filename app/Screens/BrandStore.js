import React, {useState, useEffect} from 'react';
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    TouchableNativeFeedback,
    StatusBar,
    ScrollView,
    Linking
} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import Colors from '../Styles/Colors';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen, {deviceWidth} from '../Styles/Dimen';
import MyImageButton, {MyImage, MyImageBackground} from '../Components/MyImageButton';
import {clearAllData, console_log, errorAlert, getDataWithoutToken, isNetConnected} from '../Classes/auth';
import MyTextButton, {MyGradientView} from '../Components/MyTextButton';
import {BackArrow} from '../Components/BackArrow';
import {
    BrandDescription,
    BrandDetailsDivider,
    NearestParkingComponent, SimilarBrandCard,
    SimilarBrandHeading,
} from '../Components/BrandDetailsComponents';


export const BrandStore = ({route, navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [categories, setCategories] = useState([]);
    const [similarBrands, setSimilarBrands] = useState([]);
    const [filterList, setFilterList] = useState([{id: 1}, {id: 2}]);
    const [itemData, setItemData] = useState(null);
    const [headerImage, setHeaderImage] = useState(null);
    const [updateList, setUpdateList] = useState(0);


    useEffect(() => {
        let {itemData, similarBrands} = route.params;


        updateData(itemData, similarBrands)
        setSimilarBrands(similarBrands)

        // setItemData(itemData);
        //
        // setCategories(similarBrands.filter(b => b.id !== itemData.id))
        // console_log('similar brands : ' + similarBrands.length)
        // try {
        //     let mediaLink = itemData._links['wp:featuredmedia'][0].href;
        //     console_log(mediaLink);
        //     getBrandImages(mediaLink);
        // } catch (e) {
        //     console_log(e);
        // }


    }, []);

    const updateData = (itemData, similarBrands) => {

        setItemData(itemData);
        if (similarBrands)
            setCategories(similarBrands.filter(b => b.id !== itemData.id))
        // setUpdateList(updateList + 1)
        // console_log('similar brands : ' + similarBrands.length)
        try {
            let mediaLink = itemData._links['wp:featuredmedia'][0].href;
            console_log(mediaLink);
            getBrandImages(mediaLink);
        } catch (e) {
            console_log(e);
        }

    }


    const getBrandImages = (mediaLink) => {
        isNetConnected().then(res => {
            getDataWithoutToken(mediaLink, 'GET', '', setIsLoading)
                .then(res => {
                    if (res) {
                        // console_log(res.media_details.sizes.full.source_url);
                        setHeaderImage(res.media_details.sizes.full.source_url);
                        // setHeaderImage()
                    } else {

                    }
                })
                .catch(err => console_log(err))
        });
    };
    const similarBrandPress = (itemData) => {
        updateData(itemData, similarBrands)
    }
    const callStore = () => {
        console_log(itemData.phone_number)
        if (itemData.phone_number==='N/A'){
            errorAlert('Phone number not available.')
            return
        }
        Linking.canOpenURL('tel: ' + itemData.phone_number)
            .then(res => {
                Linking.openURL('tel: ' + itemData.phone_number)
                    .then()
                    .catch();
            })
            .catch(err => {
                errorAlert('No phone number available');
            });
    }


    return (
        <MyBackgroundImage isBottom={false}>
            {itemData !== null &&
            <View style={styles.mainContainer}>
                <ScrollView>
                    <MyGradientView
                        // image={require('../Asset/shopping.png')}
                        image={headerImage !== null ? {uri: headerImage} : require('../Asset/shopping.png')}
                        style={{}}>
                        <View style={styles.gradientInnerContainer}>
                            <BackArrow onPress={() => navigation.pop()}/>
                            <AppText style={styles.brandNameText} heading={true}>
                                {/*DECOR FASHION*/}
                                {itemData.title !== undefined ? itemData.title.rendered.toUpperCase() : ''}
                            </AppText>
                            <AppText style={styles.floorNameText}>
                                {/*GROUND*/}
                                {itemData.level}
                            </AppText>
                            <AppText style={styles.openingHoursText}>
                                {/*OPENING HOURS 11:00 AM - 12:00 MID NIGHT*/}
                                {typeof itemData.timing !== 'undefined' ? "OPENING HOURS  " + itemData.timing.toUpperCase() : ''}
                            </AppText>

                            <MyImageButton
                                source={require('../Asset/call_store.png')}
                                imageContainerStyle={styles.callButtonImage}
                                onPress={callStore}
                            />
                            <AppText style={styles.callButtonText}>
                                CALL STORE
                            </AppText>
                            <BrandDetailsDivider style={{width: '100%'}}/>
                        </View>
                    </MyGradientView>

                    <NearestParkingComponent/>

                    <BrandDescription
                        // description={'This is the description of the brand, it will show detailed description of the brand in text form This is the description of the brand, it will show detailed description of the brand in text form'}
                        description={itemData.description}
                    />

                    <BrandDetailsDivider/>
                    {categories.length > 0 &&
                    <View style={{padding: dimen.app_padding}}>
                        <SimilarBrandHeading title={'Similar Brands'}/>
                        <FlatList
                            style={{marginTop: 5}}
                            data={categories}
                            extraData={updateList}
                            keyExtractor={(item, index) => item.id.toString()}
                            numColumns={1}
                            bounces={false}
                            showsHorizontalScrollIndicator={false}
                            horizontal={true}
                            renderItem={({item, index}) => (
                                <SimilarBrandCard data={item} onPress={similarBrandPress}/>
                            )}
                        />
                    </View>}


                </ScrollView>
                {/*<MyTextButton buttonText={'NAVIGATE TO STORE'}/>*/}
            </View>
            }
        </MyBackgroundImage>
    );

};


const styles = StyleSheet.create({
    mainContainer: {flex: 1, justifyContent: 'space-between'},
    gradientInnerContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
    },
    brandNameText: {
        fontSize: 26,
        textAlign: 'center',
        color: Colors.normal_text_color,
        marginBottom: dimen.app_padding * 2,
    },
    floorNameText: {
        fontSize: 10,
        color: Colors.normal_text_color,
        marginBottom: 10,
    },
    openingHoursText: {
        fontSize: 10,
        color: Colors.normal_text_color,
        marginBottom: dimen.app_padding * 2,
    },
    callButtonImage: {width: 60, height: 60, marginBottom: dimen.small_padding},
    callButtonText: {
        fontSize: 14,
        color: Colors.normal_text_color,
        marginBottom: dimen.small_padding,
    },


});
