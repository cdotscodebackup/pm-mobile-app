import React, {useState, useEffect} from 'react';
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    TouchableNativeFeedback,
    StatusBar,
    ScrollView,
    Linking,
} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';
import Colors from '../Styles/Colors';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen, {deviceWidth} from '../Styles/Dimen';
import MyImageButton, {MyImage, MyImageBackground} from '../Components/MyImageButton';
import {BackArrow} from '../Components/BackArrow';
import {console_log, errorAlert, getDataWithoutToken, isNetConnected} from '../Classes/auth';
import {MyUrls} from '../Classes/MyUrls';
import {EventCard} from '../Cards/EventCard';
import {HeaderContainer, HeaderTitle, SearchComponent} from '../Components/Common';
import {EmptyList, Loader2} from '../Components/Loader';


export const Events = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [searchText, setSearchText] = useState('');

    const [events, setEvents] = useState([]);
    const [filteredEvents, setFilteredEvents] = useState([]);

    useEffect(() => {
        getEvents();
    }, []);

    const getEvents = () => {
        isNetConnected().then(res => {
            getDataWithoutToken(MyUrls.events_url, 'GET', '', setIsLoading)
                .then(res => {
                    if (res && res.length > 0) {
                        setEvents(res);
                        setFilteredEvents(res);
                    } else {
                        setEvents([]);
                        setFilteredEvents([]);
                    }
                });
        });
    };

    const callStorePress = (item) => {
        Linking.canOpenURL('tel: ' + item.phone_no)
            .then(res => {
                Linking.openURL('tel: ' + item.phone_no)
                    .then()
                    .catch();
            })
            .catch(err => {
                errorAlert('No phone number available');
            });
    };
    const searchTextChange = (text) => {
        setSearchText(text);
        setFilteredEvents(events.filter(eve => (eve.title.rendered.toLowerCase().includes(text.toLowerCase()))));
        // filterCategories(text);
    };

    const eventPress = (item) => {
        navigation.navigate('EventDetails', {eventDetail: item});
    };
    return (
        <MyBackgroundImage isBottom={false}>
            <HeaderContainer>
                <HeaderTitle title={'EVENTS'}/>
                <BackArrow onPress={() => navigation.pop()}/>
            </HeaderContainer>

            <View style={{padding: dimen.app_padding}}>
                <SearchComponent text={searchText} setText={searchTextChange}/>
            </View>

            <View style={{flex: 1}}>

                <Loader2 loading={isLoading}/>

                <FlatList
                    style={{flex: 1}}
                    data={filteredEvents}
                    keyExtractor={(item, index) => item.id.toString()}
                    numColumns={1}
                    bounces={false}
                    renderItem={({item, index}) => (
                        <EventCard
                            data={item}
                            callStorePress={callStorePress}
                            onPress={eventPress}
                        />
                    )}

                    ListEmptyComponent={<EmptyList text={'No event found'}/>}
                />


            </View>


        </MyBackgroundImage>
    );

};


const styles = StyleSheet.create({});
