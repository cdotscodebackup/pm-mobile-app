import React, {useState, useEffect} from 'react';
import {View, StyleSheet, TouchableWithoutFeedback, TouchableNativeFeedback, StatusBar, ScrollView} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import TopBar from '../Components/TopBar';
import Colors from '../Styles/Colors';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen from '../Styles/Dimen';
import MyImageButton, {MyImage, MyImageBackground} from '../Components/MyImageButton';
import {clearAllData, console_log, getDataWithoutToken, isNetConnected} from '../Classes/auth';
import {BackArrow} from '../Components/BackArrow';
import {MyUrls} from '../Classes/MyUrls';
import {HeaderContainer, HeaderTitle} from '../Components/Common';


export const Categories = ({route, navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [searchText, setSearchText] = useState('');
    const [title, setTitle] = useState('');
    const [url, setUrl] = useState('');
    const [brands_link, setBrads_link] = useState('');
    const [categories, setCategories] = useState([]);

    useEffect(() => {
        let {title, link, brands_link} = route.params;
        setTitle(title);
        setUrl(link);
        setBrads_link(brands_link);

        getShopsCategories(link);
    }, []);

    const getLink = (title) => {
        if (title === 'SHOPPING') {

        } else if (title === 'DINE') {

        } else if (title === 'SHOPPING') {

        }
    };

    const getShopsCategories = (url) => {
        isNetConnected().then(res => {
            getDataWithoutToken(url, 'GET', '', setIsLoading)
                .then(res => {
                    if (res && res.length > 0) {
                        setCategories(res);
                    } else {
                        setCategories([]);
                    }
                });
        });
    };

    const categoryPress = (item) => {
        navigation.navigate('Brands', {categoryData: item, brands_link: brands_link});
        // console_log(item)
    };


    return (
        <MyBackgroundImage isBottom={false}>
            <HeaderContainer>
                <BackArrow onPress={() => navigation.pop()}/>
                <HeaderTitle title={title}/>
            </HeaderContainer>

            <View style={{padding: dimen.app_padding, marginTop: dimen.app_padding}}>
                <AppText style={{fontSize: 14, color: Colors.categories_heading_color}}>
                    DISPLAYING {categories.length} RESULTS
                </AppText>

                <FlatList
                    style={{marginTop: dimen.app_padding}}
                    data={categories}
                    keyExtractor={(item, index) => item.id.toString()}
                    numColumns={1}
                    bounces={false}
                    renderItem={({item, index}) => (
                        <CategoryCard data={item} categoryPress={categoryPress}/>
                    )}
                />

            </View>


        </MyBackgroundImage>
    );

};


export const CategoryCard = (props) => {


    return (
        <TouchableWithoutFeedback style={{flex: 1}} onPress={() => props.categoryPress(props.data)}>
            <View style={{flex: 1}}>
                <View style={{
                    flex: 1,
                    height: 70,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}>
                    <AppText style={{flex: 1, fontSize: 16, color: Colors.normal_text_color}}>
                        {props.data.name.toUpperCase()}
                    </AppText>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <View style={{
                            width: 20,
                            height: 20,
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginRight: dimen.app_padding,
                            backgroundColor: Colors.categories_count_bg_color,
                        }}>
                            <AppText style-={{fontSize: 13}}>{props.data.count}</AppText>
                        </View>
                        <MyImage source={require('../Asset/right_arrow.png')}
                                 imageContainerStyle={{width: 12, height: 12}}
                        />
                    </View>
                </View>
                <View
                    style={{borderBottomWidth: 1, borderBottomColor: Colors.categories_heading_color}}/>
            </View>
        </TouchableWithoutFeedback>

    );
};


const styles = StyleSheet.create({});
