import React, {useState} from 'react';
import {
    View, StatusBar, StyleSheet, ScrollView, TouchableNativeFeedback,
    ImageBackground, Image, TouchableWithoutFeedback,
} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import Colors from '../Styles/Colors';
import MyImageButton, {MyImage, MyImageBackground} from '../Components/MyImageButton';
import dimen, {deviceHeight, deviceWidth} from '../Styles/Dimen';
import {actionAlert, console_log, errorAlert, getDataWithoutToken, isNetConnected, saveObject} from '../Classes/auth';
import MyTextButton from '../Components/MyTextButton';


import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {Sae} from 'react-native-textinput-effects';

FontAwesomeIcon.loadFont().then().catch();

import AnimatedInput from 'react-native-animated-input';
import {Keys} from '../Classes/Keys';
import {MyUrls} from '../Classes/MyUrls';


export const ForgotPassword = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [enableLogin, setEnableLogin] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [showPassword, setShowPassword] = useState(false);


    const loginPress = () => {
        if (email === '') {
            errorAlert('Please enter your email address');
            return;
        }
        let formData = {
            user_login: email,
        };

        isNetConnected().then(res => {
            getDataWithoutToken(MyUrls.forget_password_url, 'POST', JSON.stringify(formData), setIsLoading)
                .then(res => {
                    if (res && res.status === true) {
                        console_log(res);
                        actionAlert('Please check your email for password reset instructions',
                            () => {
                                navigation.popToTop();
                            });
                    } else {
                        errorAlert(res.message);
                    }
                })
                .catch(err => console_log(err));
        });


    };

    const setEmailWrapper = (text) => {
        setEmail(text);
        if (text !== '') {
            setEnableLogin(true);
        } else {
            setEnableLogin(false);
        }
    };


    return (
        <MyBackgroundImage isBottom={false}>
            <ScrollView style={{flex: 1}}>
                <TouchableWithoutFeedback onPress={() => navigation.pop()}>
                    <View style={{alignSelf: 'flex-start', padding: dimen.app_padding}}>
                        <MyImage source={require('../Asset/back_arrow.png')}
                                 tintColor={Colors.button_text_color}
                                 imageContainerStyle={{width: 20, height: 20}}
                        />
                    </View>
                </TouchableWithoutFeedback>
                <View style={{flex: 1}}>
                    <View style={{flex: 1, alignItems: 'center'}}>
                        <AppText style={styles.loginText} heading={true}>
                            Forgot password
                        </AppText>
                        <AppText style={styles.loginDescText}>
                            We are here to help. Please enter
                        </AppText>
                        <AppText style={styles.loginDescText}>
                            your email address to receive a
                        </AppText>
                        <AppText style={styles.loginDescText}>
                            password reset link
                        </AppText>
                    </View>
                    <Sae
                        label={'Email address'}
                        iconClass={FontAwesomeIcon}
                        iconName={'pencil'}
                        // iconColor={'white'}
                        inputPadding={16}
                        labelHeight={24}
                        iconSize={0}
                        labelStyle={{color: Colors.button_text_color, fontWeight: 'normal', marginBottom: 4}}
                        inputStyle={{fontWeight: 'normal'}}
                        // animationDuration={0}
                        // active border height
                        iconColor={Colors.app_background_color}
                        borderHeight={1}
                        // TextInput props
                        autoCapitalize={'none'}
                        autoCorrect={false}
                        style={styles.inputStyle}
                        value={email}
                        onChangeText={setEmailWrapper}
                    />


                    <View style={styles.inputContainer}>
                        <View style={{height: dimen.app_padding * 3}}/>
                        <View>
                            <MyTextButton
                                onPress={loginPress}
                                buttonText={'RESET PASSWORD'}
                                buttonTextStyle={{fontWeight: 'bold'}}
                                buttonContainerStyle={{}}
                            />
                            {!enableLogin &&
                            <View style={{
                                position: 'absolute',
                                opacity: 0.6,
                                width: '100%',
                                backgroundColor: 'black', top: 0, bottom: 0, left: 0, right: 0,
                            }}/>}
                        </View>
                    </View>
                </View>
            </ScrollView>

        </MyBackgroundImage>
    );
};

const styles = StyleSheet.create({

    loginText: {
        color: Colors.button_text_color,
        fontSize: 26,
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: dimen.app_padding,
    },
    loginDescText: {
        color: Colors.button_text_color,
        fontSize: 14,
        alignSelf: 'center',
        textAlign: 'center',
    },
    forgetPasswordText: {
        color: Colors.button_text_color,
        fontSize: 14,
        alignSelf: 'flex-end',
        textAlign: 'center',
        marginTop: 5,
    },
    inputContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        overflow: 'hidden',
        backgroundColor: Colors.app_background_color,
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
        paddingTop: dimen.app_padding,

    },
    buttonContainer: {
        // flex:1,
        borderRadius: 25,
        marginTop: dimen.app_padding,
        justifyContent: 'center',
        alignItems: 'center',
        padding: dimen.app_padding,
        backgroundColor: Colors.login_button_bg_color,

    },

    quickConnectText: {
        fontSize: 14,
        marginRight: 10,
        marginLeft: 10,
        color: Colors.button_text_color,
        fontWeight: 'bold',

    },
    line: {
        height: 1,
        width: 30,
        flexGrow: 1,
        alignSelf: 'center',
        backgroundColor: Colors.border_color,

    },
    quickConnectContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: dimen.app_padding,

    },


    inputStyle: {
        margin: dimen.app_padding,
        // marginBottom: 5,
        paddingBottom: 0,
        borderBottomColor: Colors.button_text_color,
        borderBottomWidth: 1,
        marginBottom: 5,
        marginTop: 10,

    },
    labelStyle: {
        color: Colors.button_text_color,
        fontWeight: 'normal',
        marginBottom: 4,
    },

    showPasswordIcon: {
        width: 20,
        height: 20,
    },
    showPasswordContainer: {
        position: 'absolute',
        // backgroundColor:'red',
        right: 20,
        bottom: 20,
    },
    termsCheckBoxStyle: {
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        borderColor: Colors.button_text_color,
    },
});
