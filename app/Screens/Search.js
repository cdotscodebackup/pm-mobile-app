import React, {useState, useEffect} from 'react';
import {View, StyleSheet, TouchableWithoutFeedback, TouchableNativeFeedback, StatusBar, ScrollView} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import Colors from '../Styles/Colors';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import dimen from '../Styles/Dimen';
import {BackArrow} from '../Components/BackArrow';
import {
    BrandListHeader,
    FilterList,
    HeaderContainer,
    HeaderReset,
    HeaderTitle,
    SearchComponent,
} from '../Components/Common';
import {Loader2} from '../Components/Loader';
import {console_log, getDataWithoutToken, isNetConnected} from '../Classes/auth';
import {MyUrls} from '../Classes/MyUrls';
import {SearchItemCard} from '../Cards/SearchItemCard';

const FilterItemsList = [
    {id: 1, title: 'SHOPPING', status: false},
    {id: 2, title: 'DINING', status: false},
    {id: 3, title: 'ENTERTAINMENT', status: false},
    {id: 4, title: 'OFFERS', status: false},
];
export const Search = ({route, navigation}) => {

    const [isLoading, setIsLoading] = useState('');
    const [searchText, setSearchText] = useState('');
    const [categoryData, setCategoryData] = useState(null);
    const [allCategories, setAllCategories] = useState([]);
    const [filteredCategories, setFilteredCategories] = useState([]);
    const [brands, setBrands] = useState([]);
    const [filteredBrands, setFilteredBrands] = useState([]);
    const [updateFilterList, setUpdateFilterList] = useState(0);
    const [filterList, setFilterList] = useState(FilterItemsList);

    useEffect(() => {
        getSearchData().then().catch();

    }, []);

    const getSearchData = async () => {

        setIsLoading(true);
        try {
            let shops = await getShopsBrands(MyUrls.shops_url);
            let cuisine = await getShopsBrands(MyUrls.cuisine_url);
            let entertainment = await getShopsBrands(MyUrls.entertainment_url);

            setCategoryItems(shops.concat(cuisine).concat(entertainment));
            setIsLoading(false);

        } catch (e) {
            setIsLoading(false);
            console_log(e.toString());
        }
    };

    const getShopsBrands = (url) => {

        return new Promise((resolve, reject) => {

            isNetConnected().then(res => {
                getDataWithoutToken(url, 'GET', '', () => {
                })
                    .then(res => {
                        if (res && res.length > 0) {
                            let data = res.map(ite => {
                                return {
                                    ...ite,
                                    type: url === MyUrls.shops_url ? FilterItemsList[0].title :
                                        url === MyUrls.cuisine_url ? FilterItemsList[1].title :
                                            FilterItemsList[2].title,
                                };
                            });

                            resolve(data);
                        } else {
                            resolve([]);
                        }
                    })
                    .catch(err => {
                        reject([]);
                    });
            })
                .catch(err => reject([]));
        });
    };

    const setCategoryItems = (data) => {

        let sortedData = [];
        let previousChar = ' ';
        data.sort(function (a, b) {
            let textA = a.title.rendered.toUpperCase();
            let textB = b.title.rendered.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
        data.forEach((item, index) => {

            let firstChar = item.title.rendered.toLowerCase().charAt(0);
            if (firstChar.toLowerCase() !== previousChar.toLowerCase()) {
                previousChar = firstChar;
                let newItem = {
                    isFirst: true,
                    char: firstChar,
                };
                sortedData.push({...item, ...newItem});
            } else {
                let newItem = {
                    isFirst: false,
                    char: firstChar,
                };
                sortedData.push({...item, ...newItem});
            }
        });
        setBrands(sortedData);
        setFilteredBrands(sortedData);
    };
    const filterCategories = (text) => {
        let fCategories = brands.filter(item => {
            return item.title.rendered.toLowerCase().includes(text.toLowerCase());
        });
        setFilteredBrands(fCategories);
    };

    const filterWithFilterList = (list) => {
        let allFilteredItems = [];
        list.forEach(filItem => {
            if (filItem.status) {
                let fCategories = brands.filter(item => {
                    return item.type === filItem.title;
                });
                allFilteredItems = allFilteredItems.concat(fCategories);
            }
        });
        setFilteredBrands(allFilteredItems);
    };


    const categoryPress = (item) => {
        navigation.navigate('BrandStore', {itemData: item});
    };
    const searchTextChange = (text) => {
        setSearchText(text);
        filterCategories(text);
    };

    const setSelectedFilter = (item) => {
        let selectedFil = filterList.find(fil => fil.id === item.id);
        selectedFil.status = !selectedFil.status;
        setFilterList(filterList);
        setUpdateFilterList(updateFilterList + 1);

        filterWithFilterList(filterList);
    };
    const resetPress = () => {
        searchTextChange('');

        let flistnew = filterList.map(fi => {
            fi.status = false;
            return fi;
        });
        setFilterList(flistnew);
        setUpdateFilterList(updateFilterList + 1);
    };


    return (
        <MyBackgroundImage isBottom={false}>
            <View style={{
                justifyContent: 'flex-start',
                alignItems: 'center',
                padding: dimen.app_padding,
            }}>
                <BackArrow onPress={() => navigation.pop()}/>
                <HeaderTitle title={'SEARCH'}/>
                <HeaderReset resetPress={resetPress}/>
            </View>


            <View style={styles.listContainer}>
                <FlatList
                    style={{flex: 1}}
                    data={filteredBrands}
                    keyExtractor={(item, index) => item.id.toString()}
                    numColumns={1}
                    bounces={false}
                    renderItem={({item, index}) => (
                        <SearchItemCard data={item} onPress={categoryPress}/>
                    )}
                    ListHeaderComponent={
                        <View>
                            <View style={styles.searchAndFilterContainer}>
                                <SearchComponent
                                    text={searchText}
                                    setText={searchTextChange}
                                    textStyle={{fontSize: 18}}
                                />
                                <FilterList
                                    updateFilterList={updateFilterList}
                                    filterList={filterList}
                                    setSelected={setSelectedFilter}/>
                            </View>

                            <View style={{height: 20}}/>
                            <Loader2 loading={isLoading}/>
                            <BrandListHeader title={'DISPLAYING ' + filteredBrands.length + ' RESULTS'}/>
                        </View>
                    }
                />

            </View>


        </MyBackgroundImage>
    );

};


const styles = StyleSheet.create({
    listContainer: {flex: 1, padding: 20},
    searchAndFilterContainer: {
        // padding: dimen.app_padding,
        // marginTop: dimen.app_padding,
    },
});

