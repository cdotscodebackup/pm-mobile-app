import React, {useState, useEffect} from 'react';
import {View, StyleSheet, TouchableWithoutFeedback, TouchableNativeFeedback, StatusBar, ScrollView} from 'react-native';
import AppText from '../Components/AppText';
import MyBackgroundImage from '../Components/MyBackgroundImage';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import MyImageButton, {MyImage, MyImageBackground} from '../Components/MyImageButton';
import MyTextButton from '../Components/MyTextButton';
import {console_log, getObject} from '../Classes/auth';
import {HeaderContainer, HeaderTitle} from '../Components/Common';
import {ServiceItem} from '../Cards/ServiceCard';
import {Keys} from '../Classes/Keys';


export const checkUserSession = (setIsLogin, setName) => {

    getObject(Keys.user_data_key)
        .then(res => {
            if (res.status) {
                setIsLogin(true);
                setName(res.data.user_meta.first_name[0] + ' ' + res.data.user_meta.last_name[0]);
            } else {
                setIsLogin(false);
                console_log('res.user_meta.first_name');
            }
        })
        .catch(err => setIsLogin(false));

};

export const Services = ({navigation}) => {

    const [searchText, setSearchText] = useState('');
    const [isLogin, setIsLogin] = useState(false);
    const [name, setName] = useState('');

    useEffect(() => {
        let sub = navigation.addListener('focus', () => {
            checkUserSession(setIsLogin, setName);
        });
        return sub;
    }, []);


    const serviceItemPress = (item) => {
        navigation.navigate('Service', {data: item});
    };

    return (
        <MyBackgroundImage isBottom={true}>

            <HeaderContainer>
                <HeaderTitle title={'SERVICES'}/>
            </HeaderContainer>
            <ScrollView>

                {isLogin === false &&
                <GuestUser onPress={() => navigation.navigate('Login')}/>
                }
                {isLogin === true &&
                <LoginUser name={name} onPress={() => navigation.navigate('Profile')}/>
                }

                <View style={{height: 0}}/>
                <ServiceItem icon={require('../Asset/Wifi.png')}
                             text={'Free Wi-Fi'}
                             desc={ServicesData.wifi}
                             onPress={serviceItemPress}/>
                <ServiceItem icon={require('../Asset/bank.png')}
                             text={'Banks'}
                             desc={ServicesData.banks}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/first_aid.png')}
                             text={'First Aid'}
                             desc={ServicesData.firstAid}
                             onPress={serviceItemPress}/>
                <ServiceItem icon={require('../Asset/babychangingroom.png')}
                             text={'Baby Changing Room'}
                             desc={ServicesData.babyChangingRoom}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/atmmachines.png')}
                             text={'ATM Machines'}
                             desc={ServicesData.wheelchair}
                             onPress={serviceItemPress}/>
                <ServiceItem icon={require('../Asset/coveredparking.png')}
                             text={'Covered Ground Floor Parking'}
                             desc={ServicesData.coveredGroundParking}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/driver_lounge.png')}
                             text={'Driver\'s Lounge'}
                             desc={ServicesData.driverLounge}
                             onPress={serviceItemPress}/>
                <ServiceItem icon={require('../Asset/driver_cantern.png')}
                             text={'Driver\'s Canteen'}
                             desc={ServicesData.driverCanteen}
                             onPress={serviceItemPress}/>
                <ServiceItem icon={require('../Asset/in_mall_shuttle.png')}
                             text={'In Mall Shuttle'}
                             desc={ServicesData.inMallShuttle}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/pedisterian_shuttle.png')}
                             text={'Pedestrian Shuttle Service'}
                             desc={ServicesData.padestrianShuttle}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/waterless_car_wash.png')}
                             text={'Water Less Car Wash'}
                             desc={ServicesData.carWash}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/multiple_entry_lane.png')}
                             text={'Multiple Entry/ Exit Lanes'}
                             desc={ServicesData.multiline}
                             onPress={serviceItemPress}/>
                <ServiceItem icon={require('../Asset/carhire.png')}
                             text={'Car Hire Pickup Point'}
                             desc={ServicesData.carHirePickup}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/dedicatedcarparking.png')}
                             text={'Dedicated Car Parking for Ladies and Disabled Persons'}
                             desc={ServicesData.dedicatedParkingLadies}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/pharmacy.png')}
                             text={'Pharmacy'}
                             desc={ServicesData.pharmacy}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/optician.png')}
                             text={'Optician'}
                             desc={ServicesData.Optician}
                             onPress={serviceItemPress}/>


                <ServiceItem icon={require('../Asset/exclusive_kid_cinema.png')}
                             text={'Exclusive Kids Cinema'}
                             desc={ServicesData.kidCinema}
                             onPress={serviceItemPress}/>
                <ServiceItem icon={require('../Asset/customer_service.png')}
                             text={'Customer Service Desk on Each Main Entrance'}
                             desc={ServicesData.customerService}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/onsite_security.png')}
                             text={'24hr Onsite Security'}
                             desc={ServicesData.onsiteSecurity}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/kiddie_cart.png')}
                             text={'Kiddie Carts'}
                             desc={ServicesData.kiddieCart}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/wheel_chair.png')}
                             text={'Wheelchairs'}
                             desc={ServicesData.wheelchair}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/handicap_toilet.png')}
                             text={'Handicap Toilets'}
                             desc={ServicesData.handicapToilet}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/baby_chair.png')}
                             text={'Baby Chair In Food Court'}
                             desc={ServicesData.babyChairInFoodCourt}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/free_car_inspection.png')}
                             text={'Free Car Inspection By Car First'}
                             desc={ServicesData.carInspection}
                             onPress={serviceItemPress}/>

                <ServiceItem icon={require('../Asset/oil_change.png')}
                             text={'Oil Change / Car Maintenance by Havoline'}
                             desc={ServicesData.oilChange}
                             onPress={serviceItemPress}/>


            </ScrollView>
        </MyBackgroundImage>
    );

};


const GuestUser = (props) => {

    return (
        <View>
            <View style={{padding: dimen.app_padding, marginTop: dimen.app_padding}}>
                <AppText heading={true} style={{
                    fontSize: 26,
                    // fontFamily: 'crimson-roman',
                    color: Colors.button_text_color
                }}>
                    Guest User
                </AppText>
            </View>
            <MyTextButton
                onPress={() => props.onPress()}
                buttonText={'LOG IN OR REGISTER'}
                buttonTextStyle={{fontWeight: 'bold'}}
                buttonContainerStyle={{
                    margin: dimen.app_padding,
                    marginTop: 0,
                    padding: 10,
                    backgroundColor: Colors.primary_color,
                }}
            />
        </View>
    );
};

const LoginUser = (props) => {

    return (
        <TouchableWithoutFeedback onPress={props.onPress}>
            <View style={{flexDirection: 'row', alignItems: 'center', padding: dimen.app_padding}}>
                <MyImage
                    source={require('../Asset/my_account.png')}
                    imageContainerStyle={{width: 34, height: 34}}
                />

                <View style={{marginLeft: 10}}>
                    <AppText
                        heading={true}
                        style={{
                            fontSize: 24,
                            // fontFamily: 'crimson-roman',
                            color: Colors.button_text_color
                        }}>
                        {props.name}
                    </AppText>
                    <AppText style={{fontSize: 12, fontWeight: 'bold', color: Colors.primary_color}}>
                        VIEW PROFILE
                    </AppText>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({});


const ServicesData = {
    wifi: 'Don’t get bored during your visit just connect to our high-speed Wifi. Our platform is specially developed for the easiness of visitors. Connect @Packages Mall and enjoy your visit.',
    banks: 'Packages Mall offers bank facilities and services to all its valued shoppers, saving you the hassle of waiting in long queues',
    firstAid: 'Packages Mall offers first aid services for all its valued shoppers because we care about your safety and wellbeing. If you are not well or are injured, just ask our help desk and they will guide you to receive first aid',
    babyChangingRoom: 'We realize it can be a hassle for mothers to change their baby’s clothes when they are in the Mall. This is why Packages Mall has a dedicated baby changing room so all mothers can change their babies’ clothes with ease and convenience',
    prayerRooms: 'The Packages Mall has onsite prayer rooms for men and women on all levels. If you feel a problem in finding prayer rooms then connect with our Guest services desk.',
    atmMachines: 'Packages Mall offers ATM services for all its shoppers. Don’t worry about running out of cash, you can withdraw it and shop for your favorite brands',
    coveredGroundParking: 'Packages Mall offers its valued shoppers the facility of covered ground floor parking, making it easier to carry your shopping items to your cars',
    driverLounge: 'Packages Mall has a designated driver’s lounge, making sure that your drivers are in a comfortable environment while you can shop as long as you want without any worry',
    driverCanteen: 'Packages Mall has a designated drivers canteen so that your drivers can spend their time enjoying snacks while you enjoy your shopping',
    inMallShuttle: 'Packages Mall offers shuttle service inside the mall. No need to walk for miles, just use shuttle service to reach your desired outlet and spend all your energy shopping from your favorite brands',
    padestrianShuttle: 'Packages Mall offers shuttle service to pedestrians so you don’t need to spend your energy walking. We care about your convenience',
    carWash: 'No more water stains on your car. Packages Mall offers water car wash service that will make your car spotless and shine bright\n',
    multiline: 'Packages Mall has multiple entry and exit lanes, making sure that our valued shoppers have a pleasant experience, free from any inconvenience\n',
    carHirePickup: 'Packages Mall offers cars for hire, making sure that our shoppers have a ride when they need it\n',
    dedicatedParkingLadies: 'Packages mall has a designated parking for ladies and disabled persons, making sure that they have the best shopping experience with convenience\n',
    pharmacy: 'Packages Mall has a pharmacy for all your medical needs. Get all your medicines and supplies along with your other shopping\n',
    Optician: 'Packages Mall has an onsite optician available to take care of all your optic needs. Whether it is glasses or contact lenses, we got you covered\n',
    kidCinema: 'Packages Mall has its very own kids cinema, with the best movies for your children. Come over and enjoy wholesome family entertainment\n',
    customerService: 'Have any questions or concerns? Worry not because Packages Mall has customer service desks on each main entrance. Our representatives will assist you and make sure you have the best shopping experience\n',
    onsiteSecurity: 'We at Packages Mall deeply care about all our valued shoppers and ensure their safety and security. We have 24 hour security onsite, making sure you and your loved ones feel secure at all times\n',
    kiddieCart: 'Free up your hands and let the kids enjoy the Mall with you in a bright, cheerful ride! Kiddie carts with shopping trolley available in the Entrance Lobbies.\n',
    wheelchair: 'Packages Mall cares deeply about handicapped shoppers and want to make sure that they have the most comfortable shopping experience. This is why we offer wheelchairs to facilitate their shopping and make sure they have a good time\n',
    handicapToilet: 'Packages Mall has special toilets for handicapped shoppers, ensuring their safety and wellbeing at all times\n',
    babyChairInFoodCourt: 'Packages Mall cares about families and ensures that they are well accommodated in the food court after their shopping spree. We have baby chairs to ensure that you enjoy your well deserved meals comfortably\n',
    carInspection: 'Packages Mall offers free car inspection to all valued shoppers, making sure that your car is performing at its level best and identifying any faults that need to be fixed\n',
    oilChange: 'Packages Mall offers car maintenance and oil change facility by our exclusive partner Havoline. Get the perfect performance from your car by using our services\n',


    // whee_chair: 'Wheelchairs are available for your use; please ask Information Desk or any Customer Care Officer.',
    // harassment_policy: 'We will never compromise on our Harassment policy. Offenders will be asked to leave the premises immediately.',
    // mall_map: 'The packages Mall provides you interactive digital directories at guest service desks. With the\n' +
    //     ' help of this, you can browse through all levels of the mall or search for a specific store.\n' +
    //     'You can choose the best route to your targeted store and can also plan a multi-store visit.',
    // kiddie_cart: 'The Packages Mall provides Baby Strollers and wheelchairs to Mothers and elders for their convenience. ',
    // lost_found: 'The packages Mall provide lost and found service for the convenience of our visitors. If someone misplaced or lost an item in the mall then connect with Lost and Found. Because there is a good chance it has found its way to our Lost and Found. And if you’ve found someone’s belongings then report it to Lost and Found.',
    // parking_space: '2000+ parking spots for cars, 2000 more for motorbikes. Reserved parking for women and handicapped customers.',
    // book_ride: 'If you’ve requested for Cream or Uber, there are assigned pick-up  and drop-off points for your convenience.',
    // tramp: 'Enjoy the best mall experience with amazing chauffeur service to take to your favorite stores. Our buggy service is amazing and can be booked from our guest services desk at levels of the mall. ',
    // cinepax: 'Enjoy the best cinema experience at packages Mall. Go and watch movies at Cinepax Pakistan’s first national multiplex cinema brand.',
    // shop_deliver: 'Shopping bags during shopping are irritating for most peoples. Now spend some money on packages Mall to get this service and drop your shopping bags at any shop desks. We will safely deliver to your home/hotel.\n' +
    //     'Terms and Conditions will apply.',
    // shop_drive: 'Now feel free to enjoy your shopping experience at packages Mall and gives us chance to take care of your shopping. Our free services seal and store shopping bags for the convenience of our visitors. Just collect your shopping bags from the shop and drop service desk before exiting the Mall.',
    // touch_map: 'The packages Mall provides you interactive digital directories at guest service desks. With the help of this, you can browse through all levels of the mall or search for a specific store.\n' +
    //     'You can choose the best route to your targeted store and can also plan a multi-store visit.',
    // Personal_Shopper_Service: 'The Packages Mall provides personal shopper services with a team of experts. Our experts will help you to choose the best stuff according to your taste and guide you about new products of your brand.',
    // Shoe_Shine_Service: 'The packages Mall is responsible to provide the best hospitality to our guests. Just enjoy the best hospitality with our complimentary shoeshine service. Everybody can have shoes buffed and shined and ready to visit stores.',
    // Security_and_ERU: 'Security of guests is the first priority of the Packages Mall. Security teams and emergency response teams are available and 24 hours ready to cater to any immediate situation and react in case of emergency.',
    // Prayer_Rooms: 'The Packages Mall has onsite prayer rooms for men and women on all levels. If you feel a problem in finding prayer rooms then connect with our Guest services desk.',
    // Car_Window_Tinting: 'Enhance the look of your car and add an extra protection layer with our convenient onsite service.',
    // Luggage_Drop_Services: 'Enjoy a consistent shopping experience at The Packages Mall with a completely coordinated baggage attendant and airport check in.',
    // Power_Banks: 'If your mobile or any gadget’s battery is low then don’t worry the Packages Mall provides you the service of a power bank. Just visit Guest Services Desk and loan a power bank on a temporary basis. Just provide a valid ID or leave a security deposit, and you can take the Power Bank with you as you enjoy your shopping.',
    // Public_Transportation: 'The packages Mall is located on Main Walton Road and you can easily get any public transport from there or you can request for Careem or Uber.',

};

