import dimen, {deviceWidth} from '../Styles/Dimen';
import TextStyles from '../Styles/TextStyles';
import {ImageBackground, TouchableOpacity, View} from 'react-native';
import React from 'react';
import AppText from './AppText';
import Colors from '../Styles/Colors';
import {MyImage} from './MyImageButton';



const MyTextButton = (props) => {
    return (
        <MyTextButtonSimple onPress={props.onPress}
                            buttonText={props.buttonText}
                            buttonContainerStyle={[TextStyles.buttonStyle, props.buttonContainerStyle]}
                            buttonTextStyle={[TextStyles.buttonTextStyle, props.buttonTextStyle]}
        />

    );
};


export const MyTextButtonSimple = (props) => {
    return (
        <View>
            <TouchableOpacity style={[props.buttonContainerStyle]}
                              onPress={props.onPress}>
                <AppText style={[props.buttonTextStyle]}>
                    {props.buttonText}
                </AppText>
            </TouchableOpacity>
        </View>
    );
};

export default MyTextButton;

export const MyGradientView = (props) => {
    return (
        <View source={props.image} style={[{height: deviceWidth}]}>
            <MyImage
                source={props.image}
                imageContainerStyle={{
                    flex: 1,
                    width: deviceWidth,
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                    resizeMode: 'cover',
                    opacity: 0.8,
                    // backgroundColor:'red'
                }}
            />

            <MyImage
                source={require('../Asset/gradient.png')}
                tintColor={Colors.app_background_color}
                imageContainerStyle={{
                    flex: 1,
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                    resizeMode: 'cover',
                }}
            />
            {/*<View style={{backgroundColor: Colors.gradient_bottom_color, height: 50}}/>*/}
            <View style={[{
                position: 'absolute',
                bottom: 0,
                top: 0,
                left: 0,
                right: 0,
                // justifyContent: 'flex-end',
                // alignItems: 'center',
                // padding: dimen.app_padding,
                // paddingBottom: 0,
            }, props.style]}>
                {props.children}
            </View>

        </View>
    );
};
