import {Sae} from 'react-native-textinput-effects';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import Colors from '../Styles/Colors';
import {StyleSheet, View} from 'react-native';
import React from 'react';
import dimen from '../Styles/Dimen';


export const MyInputField = (props) => {


    return (
        <Sae
            label={props.label}
            iconClass={FontAwesomeIcon}
            iconName={'pencil'}
            inputPadding={16}
            labelHeight={24}
            iconSize={0}
            onFocus={() => props.onFocus(true)}
            onBlur={() => props.onBlur(false)}
            labelStyle={!props.isFocus ? styles.labelStyle : styles.labelFocusedStyle}
            iconColor={Colors.app_background_color}
            borderHeight={1}
            autoCapitalize={'none'}
            autoCorrect={false}
            style={[styles.inputStyle, {marginTop: 0}]}
            value={props.value}
            onChangeText={(text) => props.setText(text, 'first_name')}
        />
    );
};


const styles = StyleSheet.create({

    loginText: {
        color: Colors.button_text_color,
        fontSize: 26,
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: dimen.app_padding,
    },
    loginDescText: {
        color: Colors.button_text_color,
        fontSize: 18,
        alignSelf: 'center',
        textAlign: 'center',
    },
    termsText: {
        color: Colors.button_text_color,
        fontSize: 15,
        alignSelf: 'flex-start',
        // textAlign: 'center',
        marginTop: 0,
        marginLeft: 10,
    },
    inputContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        overflow: 'hidden',
        backgroundColor: Colors.app_background_color,
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
        paddingTop: dimen.app_padding,

    },
    buttonContainer: {
        // flex:1,
        borderRadius: 25,
        marginTop: dimen.app_padding,
        justifyContent: 'center',
        alignItems: 'center',
        padding: dimen.app_padding,
        backgroundColor: Colors.login_button_bg_color,

    },

    quickConnectText: {
        fontSize: 14,
        marginRight: 10,
        marginLeft: 10,
        color: Colors.button_text_color,
        fontWeight: 'bold',

    },
    line: {
        height: 1,
        width: 30,
        flexGrow: 1,
        alignSelf: 'center',
        backgroundColor: Colors.border_color,

    },
    quickConnectContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: dimen.app_padding,

    },
    inputStyle: {
        margin: dimen.app_padding,
        // marginBottom: 5,
        paddingBottom: 0,
        borderBottomColor: Colors.button_text_color,
        borderBottomWidth: 1,
        marginBottom: 5,

    },
    labelStyle: {
        color: Colors.button_text_color,
        fontWeight: 'normal',
        marginBottom: 4,
    },
    labelFocusedStyle: {
        color: Colors.primary_color,
        fontWeight: 'normal',
        marginBottom: 4,
    },

    showPasswordIcon: {
        width: 20,
        height: 20,
    },

    showPasswordContainer: {
        position: 'absolute',
        right: 20,
        bottom: 20,
    },
    countryDownIcon: {
        width: 15,
        height: 15,
    },
    countryDownContainer: {
        position: 'absolute',
        right: 20,
        bottom: 10,
    },
    termsCheckBoxStyle: {
        backgroundColor: Colors.app_background_color,
        borderWidth: 1,
        borderColor: Colors.button_text_color,
    },

});
