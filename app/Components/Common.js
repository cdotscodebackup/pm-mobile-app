import dimen, {deviceWidth} from '../Styles/Dimen';
import {StyleSheet, TouchableNativeFeedback, TouchableWithoutFeedback, View} from 'react-native';
import React from 'react';
import AppText from './AppText';
import Colors from '../Styles/Colors';
import MyImageButton, {MyImage} from './MyImageButton';
import {TextInput} from 'react-native-gesture-handler';
import {console_log} from '../Classes/auth';


export const HeaderTitle = (props) => {

    return (
        <View style={{justifyContent: 'flex-start', alignItems: 'center', fontWeight: 'bold'}}>
            <AppText style={{fontSize: 15, fontWeight: 'bold', color: Colors.button_text_color}}>
                {props.title}
            </AppText>
        </View>
    );
};
export const HeaderSkip = (props) => {

    return (
        <TouchableNativeFeedback onPress={() => props.onPress()}>
            <View style={{
                position: 'absolute',
                right: 15,
                top: 15,
                justifyContent: 'flex-start',
                alignItems: 'flex-end',
            }}>
                <AppText style={{fontSize: 16, color: Colors.primary_color, fontWeight: 'bold'}}>
                    SKIP
                </AppText>
            </View>
        </TouchableNativeFeedback>
    );
};


export const HeaderReset = (props) => {


    return(
        <TouchableNativeFeedback onPress={props.resetPress}>
            <View style={{
                position: 'absolute',
                right: 20,
                top: 15,
                justifyContent: 'flex-start',
                alignItems: 'flex-end',
            }}>
                <AppText style={{fontSize: 16, color: Colors.normal_text_color, fontWeight: 'bold'}}>
                    RESET
                </AppText>
            </View>
        </TouchableNativeFeedback>
    )
}


export const EditSave = (props) => {

    return (
        <TouchableNativeFeedback onPress={() => props.onPress(props.isEdit)}>
            <View style={{
                position: 'absolute',
                right: 15,
                top: 15,
                justifyContent: 'flex-start',
                alignItems: 'flex-end',
            }}>
                <AppText style={{fontSize: 16, color: Colors.primary_color, fontWeight: 'bold'}}>
                    {props.isEdit ? 'SAVE' : 'EDIT'}
                </AppText>
            </View>
        </TouchableNativeFeedback>
    );
};


export const HeaderContainer = (props) => {
    return (
        <View style={{
            justifyContent: 'flex-start',
            alignItems: 'center',
            padding: dimen.app_padding,
        }}>
            {props.children}
        </View>
    );
};

export const FilterList = (props) => {
    const onPress = (item) => {
        props.setSelected(item);
    };

    let filterList = props.filterList.map((item, index) => {
        return (
            <TouchableWithoutFeedback key={index} onPress={() => onPress(item)}>
                <View
                    style={[{
                        // flex:1,
                        // width: deviceWidth / 4,
                        marginRight: 15,
                        marginTop: 15,
                        padding: 5,
                        paddingRight: 10,
                        paddingLeft: 10,
                        borderWidth: 1,
                        borderColor: Colors.normal_text_color,
                        marginLeft: 0,
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                    },
                        item.status ? {backgroundColor: Colors.button_text_color} : {},

                    ]}>
                    {!item.status &&
                    <AppText style={{
                        fontSize: 14,
                        color: Colors.normal_text_color,
                        // backgroundColor: 'red',
                        textAlignVertical:'center',
                        textAlign:'center',alignSelf:'center',
                        marginTop:3
                    }}>
                        {item.title !== undefined ? item.title : 'OFFERS'}
                    </AppText>
                    }
                    {item.status &&
                    <AppText style={{
                        fontSize: 14,
                        color: Colors.black,
                        textAlignVertical:'center',
                        textAlign:'center',alignSelf:'center',
                        marginTop:3
                    }}>
                        {item.title !== undefined ? item.title : 'OFFERS'}
                    </AppText>
                    }
                </View>
            </TouchableWithoutFeedback>
        );

    });
    // return filterList;
    if (filterList.length > 0) {
        return (
            <View>
                <FilterHeader title={'FILTER'}/>
                <View style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                }}>
                    {filterList}
                </View>
            </View>
        );
    } else {
        return <View/>;
    }
};
export const FilterHeader = (props) => {
    return (
        <AppText style={{fontSize: 16, color: Colors.normal_text_color, marginTop: dimen.app_padding}}>
            {props.title}
        </AppText>
    );
};

export const SearchComponent = (props) => {

    return (
        <View style={styles.inputContainer}>
            <MyImage source={require('../Asset/search.png')}
                     imageContainerStyle={{width: 15, height: 15}}/>
            <TextInput onChangeText={(text) => props.setText(text)}
                       value={props.text}
                       placeholder={'Search'}
                       placeholderTextColor={Colors.search_place_holder_color}
                       autoCapitalize='none'
                       keyboardType={'default'}
                       style={[styles.inputField, props.textStyle]}/>
            {props.text.length !== 0 &&
            <MyImageButton
                onPress={() => props.setText('')}
                source={require('../Asset/close.png')}
                tintColor={Colors.gray}
                imageContainerStyle={{width: 15, height: 15, marginRight: 10}}/>}
        </View>
    );
};


export const BrandListHeader = (props) => {

    return (
        <AppText style={{fontSize: 14, color: Colors.categories_heading_color}}>
            {props.title}
        </AppText>
    );
};

const styles = StyleSheet.create({
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: Colors.search_bg_color,
        padding: 0,
        paddingLeft: 10,
    },
    inputField: {
        padding: 3,
        paddingLeft: 10,
        color: Colors.normal_text_color,
        flex: 1,
        fontSize:18
    },


});
