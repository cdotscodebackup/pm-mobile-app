import * as React from 'react';
import {
    Image,
    StyleSheet,
    TouchableOpacity,
    TouchableNativeFeedback,
    TouchableWithoutFeedback,
    View,
} from 'react-native';
import Colors from '../Styles/Colors';
import AppText from './AppText';


const Tab = (props) => {
    return (
        <View style={{flex: 1,}}>
            <TouchableWithoutFeedback
                onPress={props.onPress}>
                <View>
                    {(props.id == 0) &&
                    <View style={[styles.tabContainer, props.focused ?styles.focusedBackground:styles.notFocusedBackground]}>
                        <View style={styles.tabIconContainer}>
                            <Image
                                source={require('../Asset/home.png')}
                                tintColor={props.focused ? Colors.primary_color : Colors.non_active_icon_color}
                                style={[styles.tabIconStyle,
                                    props.focused ? {tintColor: Colors.primary_color} : {tintColor: Colors.non_active_icon_color}]}/>
                            {props.focused &&
                            <AppText style={styles.focusedStyle}>
                                HOME
                            </AppText>}
                            {!props.focused &&
                            <AppText style={styles.notFocusedStyle}>
                                HOME
                            </AppText>}
                        </View>
                    </View>}

                    {(props.id == 1) &&
                    <View style={[styles.tabContainer, props.focused ?styles.focusedBackground:styles.notFocusedBackground]}>
                        <View style={styles.tabIconContainer}>

                            <Image
                                source={require('../Asset/map.png')}
                                tintColor={props.focused ? Colors.primary_color : Colors.non_active_icon_color}
                                style={[styles.tabIconStyle, props.focused ? {tintColor: Colors.primary_color} : {tintColor: Colors.non_active_icon_color}]}/>
                            {props.focused &&
                            <AppText style={styles.focusedStyle}>
                                MAP
                            </AppText>}
                            {!props.focused &&
                            <AppText style={styles.notFocusedStyle}>
                                MAP
                            </AppText>}
                        </View>
                    </View>}

                    {(props.id == 2) &&
                    <View style={[styles.tabContainer, props.focused ?styles.focusedBackground:styles.notFocusedBackground]}>
                        <View style={styles.tabIconContainer}>
                            <Image
                                source={require('../Asset/parking.png')}
                                tintColor={props.focused ? Colors.primary_color : Colors.non_active_icon_color}
                                style={[styles.tabIconStyle, props.focused ? {tintColor: Colors.primary_color} : {tintColor: Colors.non_active_icon_color}]}/>
                            {props.focused &&
                            <AppText style={styles.focusedStyle}>
                                PARKING
                            </AppText>}
                            {!props.focused &&
                            <AppText style={styles.notFocusedStyle}>
                                PARKING
                            </AppText>}
                        </View>

                    </View>}

                    {(props.id == 3) &&
                    <View style={[styles.tabContainer, props.focused ?styles.focusedBackground:styles.notFocusedBackground]}>
                        <View style={styles.tabIconContainer}>
                            <Image
                                source={require('../Asset/services.png')}
                                tintColor={props.focused ? Colors.primary_color : Colors.non_active_icon_color}
                                style={[styles.tabIconStyle, props.focused ? {tintColor: Colors.primary_color} : {tintColor: Colors.non_active_icon_color}]}/>
                            {props.focused &&
                            <AppText style={styles.focusedStyle}>
                                SERVICES
                            </AppText>}
                            {!props.focused &&
                            <AppText style={styles.notFocusedStyle}>
                                SERVICES
                            </AppText>}
                        </View>

                    </View>}

                    {(props.id == 4) &&
                    <View style={[styles.tabContainer, props.focused ?styles.focusedBackground:styles.notFocusedBackground]}>
                        <View style={styles.tabIconContainer}>
                            <Image
                                source={require('../Asset/more.png')}
                                tintColor={props.focused ? Colors.primary_color : Colors.non_active_icon_color}
                                style={[styles.tabIconStyle, props.focused ? {tintColor: Colors.primary_color} : {tintColor: Colors.non_active_icon_color}]}/>
                            {props.focused &&
                            <AppText style={styles.focusedStyle}>
                                MORE
                            </AppText>}
                            {!props.focused &&
                            <AppText style={styles.notFocusedStyle}>
                                MORE
                            </AppText>}
                        </View>

                    </View>}

                </View>
            </TouchableWithoutFeedback>
        </View>

    );
};
export default Tab;

const styles = StyleSheet.create({
    tabIconStyle: {
        width: 22,
        height: 22,
        padding: 2,
        // flex: 1,flexGrow:1,
        resizeMode: 'contain',
    },
    tabIconContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
    },

    tabContainer: {
        justifyContent: 'center', alignItems: 'center', flexGrow: 1,

    },
    focusedStyle: {color: Colors.primary_color, marginTop: 2,fontSize:9},
    notFocusedStyle: {color: Colors.non_active_icon_color, marginTop: 2,fontSize:9},

    focusedBackground: {},
    notFocusedBackground: {},

});


