import React, {useState, useRef, useEffect} from 'react';

import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import {TouchableWithoutFeedback, View} from 'react-native';
import AppText from './AppText';
import {MyImage} from './MyImageButton';
import {FlatList} from 'react-native-gesture-handler';


export const CallComponent = (props) => {
    const [categories, setCategories] = useState([
        {id: 1, title: 'Phone', value: 'UAN: 111-MY-MALL (111-696-255)'},
        {id: 2, title: 'Information Desk', value: 'Level 1: Ext. 1141'},
        {id: 3, title: 'Information Desk', value: 'Level 2: Ext. 1142'},
        {id: 4, title: 'Cinepax', value: '(042) 111 146 372'},
        {id: 5, title: 'Carrefour', value: '(042) 323 053 01'},
    ]);


    const showPress = () => {
    };

    return (
        <View style={{flex: 1, backgroundColor: Colors.bottom_up_bg_color}}>
            <AppText style={{
                color: Colors.normal_text_color,
                fontSize: 18,
                margin: dimen.small_padding,
                marginLeft: dimen.app_padding,
            }}>
                Contacts
            </AppText>
            <FlatList
                style={{}}
                data={categories}
                keyExtractor={(item, index) => item.id.toString()}
                numColumns={1}
                bounces={false}
                renderItem={({item, index}) => (
                    <ContactItem data={item}/>
                )}
                ListHeaderComponent={<View style={{height: 10,}}/>}
                ListFooterComponent={<View style={{height: 20}}/>}
            />
        </View>

    );


};


export const ContactItem = (props) => {
    const onPress = () => {
        // alert('jasldfkj')
    };
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={{marginLeft: dimen.app_padding, marginRight: dimen.app_padding, marginTop: 10}}>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    // padding: dimen.small_padding,
                    // paddingLeft: 10,
                }}>
                    <View style={{marginLeft: 5}}>
                        <AppText
                            style={{fontSize: 15, color: Colors.button_text_color}}>
                            {props.data.title}
                        </AppText>
                        <AppText
                            style={{
                                fontSize: 12,
                                color: Colors.button_text_color,
                                marginTop: 5,
                            }}>
                            {props.data.value}
                        </AppText>
                    </View>
                    <MyImage
                        source={require('../Asset/right_arrow.png')}
                        tintColor={Colors.call_arrow_color}
                        imageContainerStyle={{width: 13, height: 13, marginRight: 5}}
                    />
                </View>
                <View style={{
                    height: 1,
                    backgroundColor: Colors.call_arrow_color,
                    marginTop: 10,
                    // marginBottom:5,
                    // marginRight: dimen.small_padding,
                    // marginLeft: dimen.small_padding,
                }}/>
            </View>
        </TouchableWithoutFeedback>
    );
};


export const BookRideComponent = (props) => {
    const [categories, setCategories] = useState([]);


    const showPress = () => {
    };

    return (
        <View style={{flex: 1, backgroundColor: Colors.bottom_up_bg_color}}>

            <AppText heading={true} style={{
                color: Colors.normal_text_color,
                fontSize: 24,
                margin: dimen.small_padding,
                marginLeft: dimen.app_padding,
            }}>
                Book a Ride
            </AppText>

            <TouchableWithoutFeedback onPress={() => props.onPress('uber')}>
                <AppText style={{
                    color: Colors.normal_text_color,
                    fontSize: 18,
                    margin: dimen.small_padding,
                    marginLeft: dimen.app_padding,
                }}>
                    Uber
                </AppText>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback onPress={() => props.onPress('careem')}>

                <AppText style={{
                    color: Colors.normal_text_color,
                    fontSize: 18,
                    margin: dimen.small_padding,
                    marginLeft: dimen.app_padding,
                }}>
                    Careem
                </AppText>
            </TouchableWithoutFeedback>

        </View>

    );


};
