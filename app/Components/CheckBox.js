import {ActivityIndicator, Image, View, StyleSheet, TouchableWithoutFeedback, TouchableOpacity} from 'react-native';
import React, {useState, useEffect} from 'react';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import AppText from './AppText';
import {MyImage} from './MyImageButton';


export const CheckBox = (props) => {
    return (
        <View>
            {!props.status &&
            <TouchableOpacity onPress={props.setStatus}>
                <View style={[styles.checboxContainer, props.style !== null ? props.style : {}]}>
                </View>
            </TouchableOpacity>
            }
            {props.status &&
            <TouchableOpacity onPress={props.setStatus}>
                <View style={[styles.checkboxContainerSelected, props.style !== null ? props.style : {}]}>
                    <MyImage source={require('../Asset/tick.png')}
                           tintColor={Colors.button_text_color}
                           imageContainerStyle={styles.tickImageStyle}/>
                </View>
            </TouchableOpacity>
            }
        </View>
    );
};


const styles = StyleSheet.create({

    firstBox: {flex: 0.2, margin: 10, marginRight: 0},
    secondBox: {flex: 1.8, flexDirection: 'row', margin: 10},
    thirdBox: {flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', marginRight: 10},


    boxContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-evenly',
    },
    statusBoxActive: {
        width: 30,
        height: 10,
        backgroundColor: Colors.primary_color,
        marginLeft: 10,
        borderRadius: 3,
    },
    statusBoxNonActive: {
        width: 30,
        height: 10,
        backgroundColor: Colors.signup_input_text_background_color,
        marginLeft: 10,
        borderRadius: 3,
    },


    checboxContainer: {
        width: 20,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 3,
        borderColor: Colors.primary_color,
    },
    checkboxContainerSelected: {
        width: 20,
        height: 20,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,
    },
    tickImageStyle: {width: 9, height: 9, alignSelf: 'center', resizeMode: 'contain'},
    checkBoxMainContainer: {marginRight: 5, justifyContent: 'center', alignItems: 'center'},

});

