import {View} from 'react-native';
import dimen from '../Styles/Dimen';
import {MyImage} from './MyImageButton';
import Colors from '../Styles/Colors';
import AppText from './AppText';
import React from 'react';
import {isNetConnected} from '../Classes/auth';



export const checkNetwork=(setIsNetwork)=>{
    // return new Promise((resolve,reject )=>{
        isNetConnected()
            .then(res => setIsNetwork(true))
            .catch(err => setIsNetwork(false));

    // })

}

export const NoInternetView = (props) => {


    if (props.status) {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                margin: dimen.app_padding,
            }}>
                <MyImage source={require('../Asset/no_internet.png')}
                         tintColor={Colors.button_text_color}
                         imageContainerStyle={{width: 50, height: 50}}/>
                <AppText style={{
                    fontSize: 18, fontFamily: 'crimson-roman',
                    marginTop: 22,
                    color: Colors.normal_text_color,
                }}>
                    No Network
                </AppText>
            </View>
        );
    } else {
        return <View/>
    }
}
