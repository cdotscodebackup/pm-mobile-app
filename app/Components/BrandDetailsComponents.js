import dimen, {deviceWidth} from '../Styles/Dimen';
import {ScrollView, StyleSheet, TouchableWithoutFeedback, View,} from 'react-native';
import React, {useState, useCallback} from 'react';
import AppText from './AppText';
import Colors from '../Styles/Colors';
import {MyImage} from './MyImageButton';
import {TextInput} from "react-native-gesture-handler";
import {exp} from 'react-native-reanimated';

export const BrandDetailsDivider = (props) => {
    return (
        <View style={[{
            backgroundColor: Colors.brand_store_border_color,
            height: 1,
            // width: '100%',
            marginRight: dimen.app_padding,
            marginLeft: dimen.app_padding,
        }, props.style]}/>
    );
};

export const BrandDescription = (props) => {
    const [showDesc, setShowDesc] = useState(false);
    const [lines, setLines] = useState(2);
    const showMoreDesc = (e) => {
        setShowDesc(!showDesc);
    }

    const onTextLayout = useCallback((e) => {
        console.log('lines', e.nativeEvent.lines.length)
        setLines(e.nativeEvent.lines.length)
    })

    return (
        <View>

            <AppText style={{
                fontSize: 18,
                color: Colors.normal_text_color,
                marginLeft: dimen.app_padding,
                marginTop: dimen.app_padding
            }}>
                Description
            </AppText>
            <View style={{padding: dimen.app_padding}}>
                <AppText style={{fontSize: 14, color: Colors.normal_text_color}}
                         onTextLayout={onTextLayout}
                         numberOfLines={showDesc ? 0 : 2}>
                    {props.description}
                </AppText>
                {lines > 2 &&
                <TouchableWithoutFeedback onPress={showMoreDesc}>
                    <View>
                        {!showDesc &&
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'flex-end',
                            marginTop: 10,
                        }}>
                            <AppText style={{
                                fontSize: 12,
                                color: Colors.search_place_holder_color,
                                alignSelf: 'flex-end',
                                marginTop: 1,
                                fontWeight: 'bold',
                                borderBottomWidth: 1, borderBottomColor: Colors.search_place_holder_color
                            }}>
                                READ MORE
                            </AppText>
                            <MyImage source={require('../Asset/down.png')}
                                     tintColor={Colors.search_place_holder_color}
                                     imageContainerStyle={{width: 11, height: 11, marginLeft: 5}}/>
                        </View>
                        }
                        {showDesc &&
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'flex-end',
                            marginTop: 10,
                        }}>
                            <AppText style={{
                                fontSize: 12,
                                color: Colors.search_place_holder_color,
                                alignSelf: 'flex-end',
                                marginTop: 1,
                                fontWeight: 'bold',
                                borderBottomWidth: 1, borderBottomColor: Colors.search_place_holder_color

                            }}>
                                READ LESS
                            </AppText>
                            <MyImage source={require('../Asset/up.png')}
                                     tintColor={Colors.search_place_holder_color}
                                     imageContainerStyle={{width: 11, height: 11, marginLeft: 5}}/>
                        </View>
                        }
                    </View>
                </TouchableWithoutFeedback>
                }
            </View>
        </View>
    );
};


export const SimilarBrandHeading = (props) => {
    return (
        <AppText style={{fontSize: 24, color: Colors.similar_brands_text_color}} heading={true}>
            {props.title}
        </AppText>

    )
}

export const SimilarBrandCard = (props) => {

    return (
        <TouchableWithoutFeedback onPress={() => props.onPress(props.data)}>
            <View style={{
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: Colors.normal_text_color,
                padding: dimen.app_padding,
                marginRight: dimen.app_padding,
                width: deviceWidth / 2.5,
                height: deviceWidth / 4.5,
            }}>
                <AppText style={{fontSize: 14, color: Colors.app_background_color}}>
                    {props.data.title.rendered}
                </AppText>
                <AppText style={{
                    position: 'absolute',
                    bottom: 2,
                    fontSize: 9,
                    color: Colors.similar_brands_text_color,
                }}>
                    {props.data.level}
                </AppText>
            </View>
        </TouchableWithoutFeedback>
    )
}


export const NearestParkingComponent = () => {

    return (
        <View>
            <View style={{padding: dimen.app_padding}}>
                <AppText style={{fontSize: 24, color: Colors.normal_text_color}} heading={true}>
                    Nearest Parking (G)
                </AppText>
                {/*<AppText style={{fontSize: 14, color: Colors.primary_color}}>*/}
                {/*    TAKE ME THERE*/}
                {/*</AppText>*/}
            </View>
            <BrandDetailsDivider/>
        </View>
    )
}


const styles = StyleSheet.create({});
