import React from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    StyleSheet,
    Text,
    Platform,
    TouchableOpacity, ScrollView, Dimensions,
    View,
} from 'react-native';

import Carousel, {Pagination} from 'react-native-snap-carousel';
import AppText from './AppText';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import {MyImage} from './MyImageButton';
import {console_log} from '../Classes/auth';

const {width: viewportWidth, height: viewportHeight} = Dimensions.get('window');

export default class MyCarousel extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            entries: [
                {'id': '1', 'title': require('../Asset/intro1.png')},
                {'id': '2', 'title': require('../Asset/intro2.png')},
                {'id': '3', 'title': require('../Asset/intro3.png')},
            ],
            activeSlide: 0,
        });
        this._carousel = null;
        this.width = dimen.carouselContainerWidth;
    }

    _renderItem({item, index}) {
        return (
            <View style={styles.slide}>
                <MyImage source={item.title}
                         imageContainerStyle={styles.bannerImageStyle}/>
            </View>
        );
    }

    get pagination() {
        const {entries, activeSlide} = this.state;
        return (
            <Pagination
                dotsLength={entries.length}
                activeDotIndex={activeSlide}
                containerStyle={{paddingVertical: 10}}
                dotStyle={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    // marginHorizontal: 8,
                    backgroundColor: Colors.primary_color,
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                }}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
            />
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <Carousel
                    ref={(c) => {
                        this._carousel = c;
                    }}
                    data={this.state.entries}
                    renderItem={this._renderItem}
                    sliderWidth={viewportWidth}
                    itemWidth={viewportWidth}
                    slideStyle={{width: viewportWidth}}
                    onSnapToItem={(index) => this.setState({activeSlide: index})}
                />
                {this.pagination}
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    slide: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
        padding: dimen.app_padding,
        // backgroundColor: 'red',
    },
    title: {
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        fontSize: 50,
        backgroundColor: 'yellow',

        // flex:1,
    },
});
