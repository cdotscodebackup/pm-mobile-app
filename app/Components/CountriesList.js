import React, {useState, useRef, useEffect} from 'react';

import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';
import {TouchableWithoutFeedback, View} from 'react-native';
import AppText from './AppText';
import {MyImage} from './MyImageButton';
import {FlatList} from 'react-native-gesture-handler';
import {console_log} from '../Classes/auth';
import {AllCountriesList} from '../Classes/AllCountriesData';


export const CountriesList = (props) => {
    const [categories, setCategories] = useState(AllCountriesList);


    const countryPress = (item) => {
        props.onCountrySelect(item);
    };

    return (
        <View style={{flex: 1, backgroundColor: Colors.bottom_up_bg_color}}>
            <FlatList
                style={{}}
                data={categories}
                keyExtractor={(item, index) => item.name}
                numColumns={1}
                bounces={false}
                renderItem={({item, index}) => (
                    <CountryItem data={item} onPress={countryPress}/>
                )}
                // ListHeaderComponent={<View style={{height: 10}}/>}
                ListFooterComponent={<View style={{height: 20}}/>}
            />
        </View>

    );


};


export const CountryItem = (props) => {
    const onPress = () => {
        // console_log(props.data);
        props.onPress(props.data);
    };
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={{marginLeft: dimen.app_padding, marginRight: dimen.app_padding, marginTop: 10}}>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    // padding: dimen.small_padding,
                    // paddingLeft: 10,
                }}>
                    <View style={{marginLeft: 5}}>
                        <AppText
                            style={{fontSize: 16, color: Colors.button_text_color}}>
                            {props.data.name}
                        </AppText>
                    </View>
                    {/*<MyImage*/}
                    {/*    source={require('../Asset/right_arrow.png')}*/}
                    {/*    tintColor={Colors.call_arrow_color}*/}
                    {/*    imageContainerStyle={{width: 13, height: 13, marginRight: 5}}*/}
                    {/*/>*/}
                </View>
                <View style={{
                    height: 1,
                    backgroundColor: Colors.call_arrow_color,
                    marginTop: 10,
                }}/>
            </View>
        </TouchableWithoutFeedback>
    );
};

