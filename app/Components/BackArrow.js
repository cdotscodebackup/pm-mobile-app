import {TouchableNativeFeedback, View} from 'react-native';
import {MyImage} from './MyImageButton';
import React from 'react';
import Colors from '../Styles/Colors';


export const BackArrow = (props) => {
    const onPress = () => {
        props.onPress();
    };
    return (
        <TouchableNativeFeedback onPress={onPress}>
            <View
                style={{
                    position: 'absolute',
                    left: 0,
                    padding: 15,
                    top: 5,
                    justifyContent: 'flex-start',
                    alignItems: 'flex-end'
                }}>
                <MyImage
                    source={require('../Asset/back_arrow_android.png')}
                    tintColor={Colors.normal_text_color}
                    imageContainerStyle={{width: 15, height: 15}}
                />
            </View>
        </TouchableNativeFeedback>
    );
};
