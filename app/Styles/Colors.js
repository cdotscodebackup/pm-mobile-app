const Colors = {

    primary_color: '#1574c4',
    primary_light_color: '#bf5a5e',
    primary_dark_color: '#144d77',
    app_background_color: '#11100e',
    bottom_up_bg_color: '#141414',
    accent_color: '#f39200',
    non_active_icon_color: '#ffffff',

    login_field_background_color: '#F5F5F5',
    login_field_border_color: '#DAD9E1',

    gradient_top_color: '#ffffff',
    gradient_mid_color: '#717171',
    gradient_bottom_color: '#11100e',

    brand_store_border_color: '#272727',
    read_more_text_color: '#272727',
    similar_brands_text_color: '#6d6c6b',



    sub_text_color: '#052B0D',
    dashboard_bg_color: '#effaf2',
    categories_heading_color: '#717171',
    categories_count_bg_color: '#888888',
    search_bg_color: '#1c1c1c',
    search_place_holder_color: '#484848',


    current_appoint_color: '#911014',
    current_appoint_text_color: '#F5F5F5',
    upcoming_appoint_color: '#F5F5F5',
    completed_appoint_color: '#9B9B9B',


    login_button_bg_color: '#144d77',


    panic_container_bg_color: '#F5F5F5',
    panic_button_bg_color: '#FF0303',
    panic_enable_text_color: '#9B9B9B',
    panic_switch_bg_color: '#34C759',


    search_background_color: '#DAD9E1',
    vertical_line_color: '#9B9B9B',

    calender_bg_color:'#F5F5F5',

    otp_field_bg_color:'#F5F5F5',




    normal_text_color: '#ffffff',
    highlight_text_color: '#ff0000',
    heading_text_color: '#231f20',
    // button_text_color: '#ffffff',
    button_text_color: 'white',
    tab_text_color: '#144d88',
    signup_input_text_background_color: '#e4e4e4',
    image_background_color: '#7B92BF',


    vendor_credit_border_color: '#777777',
    call_arrow_color: '#2d2d2e',
    border_color: '#d6d6d6',
    order_border_color: '#d6d6d6',
    bottom_menu_border: '#d0d0d0',
    bottom_menu_text_color: '#707070',

    light_text_login: '#8f8f8f',
    dark_text_login: '#8f8f8f',
    homelighbackground: '#eceff8',
    filter_keys_background: '#f5f5f5',


    home_widget_background_color: '#ffffff',
    home_background_color: '#ffffff',
    bottom_bar_background_color: '#F5F5F5',

    header_text_color: '#ffffff',
    header_start_text: '#35bfae',
    light_background_color: '#e6e8e8',
    light_border_color: '#e5e5e5',
    text_prominent_color: '#25437d',
    fbcolor: '#25437d',
    gmailcolor: '#963D32',
    tabbar_background_color: '#333333',

    black: '#000000',
    night: '#333333',
    charcoal: '#474747',
    gray: '#7D7D7D',
    lightishgray: '#9D9D9D',
    lightgray: '#D6D6D6',
    smoke: '#EEEEEE',
    white: '#FFFFFF',
    ypsDark: '#47546E',
    yps: '#637599',
    ypsLight: '#7B92BF',
    cosmic: '#963D32',

};
export default Colors;
